#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  1 15:08:51 2023

@author: casper
"""

import numpy as np
from ase.data import covalent_radii
from gpatom.beacon.str_gen import RandomStructure, RandomBox, RandomCell
from scipy.optimize import minimize, Bounds
from gpatom.fractional_beacon.icebeacon import UnitCellHandler
from gpatom.hyperspatial_beacon.hyperspacebeacon_alternative import HSParamsHandler

class HighDimRandomStructure(RandomStructure):

    def __init__(self, atoms, rng=np.random, dims=3, world_center=None):
        
        RandomStructure.__init__(self, atoms, rng)
        self.ndof=dims
        self.world_center=world_center
        self.n_atoms=len(atoms)


class HighDimOptimizer:

    def __init__(self, dims=3, world_center=None, constr=None, derivative_modulation=1):
        
        self.ndof=dims
        self.constr=constr
        self.world_center=world_center
        self.derivative_modulation=derivative_modulation
        

    def constrain_atoms(self, atoms, lb, ub):
        
        n = max(self.ndof, 3)
        
        if len(self.constr.cindex)!=0:
            for atom in atoms:
                if atom.index in self.constr.cindex:
                    
                    idx=atom.index*n
                    for i in range( n ):
                    
                        if i<3:
                            lb[idx+i]=np.array( atom.position[i] )
                            ub[idx+i]=lb[idx+i]
                        else:
                            lb[idx+i]=self.world_center[i] 
                            ub[idx+i]=lb[idx+i]
        return lb, ub  


    def prior_vals(self, params, *args):
        
        atoms=args[0]
        prior=args[1]
       
        positions, extra_coords =  HSParamsHandler.unpack_atomic_params(len(atoms), self.ndof, params)   
        
        atoms.positions=positions
        atoms.extra_coords=extra_coords
        atoms.dims=self.ndof
        
        prior.calculate(atoms)
        
        energy = prior.results['energy']
        derivatives = prior.results['forces']

        return (energy*self.derivative_modulation, 
                -np.array(derivatives.flatten())*self.derivative_modulation )  
    
    
    
    def prior_vals_with_stress(self, params, *args):
        
        
        atoms=args[0]
        prior=args[1]
        original_cell=args[2]
        cell_factor=args[3]
        
                         
        natoms=len(atoms)
        
        deformed_positions, extra_coords, deformation_tensor =  HSParamsHandler.unpack_params(natoms, self.ndof, params)  
                       
        atoms=UnitCellHandler.atoms_deformed_to_real(atoms, deformation_tensor, deformed_positions, original_cell, cell_factor)
        

        atoms.extra_coords=extra_coords
        atoms.dims=self.ndof
        
        prior.calculate(atoms)
        
        energy = prior.results['energy']
        stress=prior.results['stress']
        forces=prior.results['forces']
        
        d3_forces, extra_forces = HSParamsHandler.get_real_and_extra(len(atoms), self.ndof, forces)
        
        deformed_forces, deformed_virial = UnitCellHandler.forces_real_to_deformed(atoms, d3_forces, stress, original_cell, cell_factor)
        
        deformed_gradients=-deformed_forces
        
        deformed_virial = UnitCellHandler.apply_cell_mask(deformed_virial, self.opt_cell_mask)   
         
        
        derivatives=HSParamsHandler.pack_params(natoms, self.ndof, deformed_gradients, extra_forces, deformed_virial)
       

        return (energy*self.derivative_modulation, 
                np.array(derivatives.flatten())*self.derivative_modulation )  

    

class HighDimAtomsRelaxer(HighDimOptimizer):

    def __init__(self, calculator, dims=3, constr=None, world_center=None,
                 fmax=0.05, steps=200):
        
        HighDimOptimizer.__init__(self, dims=dims, constr=constr, 
                                  world_center=world_center)
        
        self.calculator=calculator
        self.fmax=fmax
        self.steps=steps

    def run(self, atoms, extra_coords):
        """ Relax a structure in a simple repulsive potential """
                
            
        params=HSParamsHandler.pack_atomic_params(len(atoms), self.ndof, atoms.positions, extra_coords)

        lb, ub = self.get_bounds(atoms)
        
        result = minimize(self.prior_vals,   
                          params,
                          args=(atoms, self.calculator),
                          method='L-BFGS-B',
                          bounds=Bounds(lb, ub, keep_feasible=False),
                          jac=True,
                          options={'ftol':0, 'gtol':self.fmax, 'maxiter': self.steps, 'maxls':20})
          
        opt_coords = result['x']
        
        positions, extra_coords = HSParamsHandler.unpack_atomic_params(len(atoms), self.ndof, opt_coords)
        atoms.positions=positions
        
        return atoms, extra_coords


    def get_bounds(self, atoms):
        n_atoms=len(atoms)
        lb = [-np.inf for i in range(self.ndof)]*n_atoms
        ub = [np.inf for i in range(self.ndof)]*n_atoms
        lb, ub = self.constrain_atoms(atoms, lb, ub)    
        return lb, ub
    


class HighDimAtomsAndStressRelaxer(HighDimOptimizer):

    def __init__(self, calculator, dims=3, constr=None, world_center=None,
                 fmax=0.05, steps=200, fixed_cell_params=None):
        
        HighDimOptimizer.__init__(self, dims=dims, constr=constr, 
                                  world_center=world_center)
        
        self.calculator=calculator
        self.fmax=fmax
        self.steps=steps

        if fixed_cell_params is None:
            fixed_cell_params = [False]*6    
        self.opt_cell_mask = np.array([not elem for elem in fixed_cell_params], dtype=int)        


    def run(self, atoms, extra_coords):    
        natoms=len(atoms)            
        original_cell=atoms.get_cell()
        cell_factor=float(natoms)

        deformation_tensor, deformed_positions = UnitCellHandler.atoms_real_to_deformed(atoms, original_cell, cell_factor)

        params=HSParamsHandler.pack_params(natoms, self.ndof, deformed_positions, extra_coords, deformation_tensor)

        lb, ub = self.get_bounds(atoms) 

        
        result = minimize(self.prior_vals_with_stress,   
                          params,
                          args=(atoms, self.calculator, original_cell, cell_factor),
                          method='L-BFGS-B',
                          bounds=Bounds(lb, ub, keep_feasible=False),
                          jac=True,
                          options={'ftol':0, 'gtol':self.fmax, 'maxiter': self.steps, 'maxls':20})
          

        opt_coords = result['x']
        
        deformed_positions, extra_coords, deformation_tensor = HSParamsHandler.unpack_params(natoms, self.ndof, opt_coords)  
    
        atoms=UnitCellHandler.atoms_deformed_to_real(atoms, deformation_tensor, deformed_positions, original_cell, cell_factor)
        
        
        return atoms, extra_coords


    def get_bounds(self, atoms):
        n_atoms=len(atoms)
        n_forces=max(self.ndof,3)
        
        lb = [-np.inf for i in range(n_forces)]*n_atoms +[-np.inf]*9
        ub = [np.inf for i in range(n_forces)]*n_atoms +[np.inf]*9
        lb, ub = self.constrain_atoms(atoms, lb, ub)    
        return lb, ub
    

    
class HighDimAtomsInsideBoxRelaxer(HighDimOptimizer):

    def __init__(self, calculator, box, dims=3, constr=None, world_center=None,
                 fmax=0.05, steps=200, covrad_inside=None, derivative_modulation=1):
        
        HighDimOptimizer.__init__(self, dims=dims, constr=constr, 
                                  world_center=world_center, 
                                  derivative_modulation=derivative_modulation)
        
        self.calculator=calculator
        self.box=box
        self.fmax=fmax*derivative_modulation
        self.steps=steps
        
        if covrad_inside is None:
            covrad_inside=[True]*max(dims,3)
        
        self.covrad_inside=covrad_inside

    def run(self, atoms, extra_coords):

        params=HSParamsHandler.pack_atomic_params(len(atoms), self.ndof, atoms.positions, extra_coords)
        
        
        lb, ub = self.get_bounds(atoms, self.box, self.covrad_inside)        
                     
        result = minimize(self.prior_vals,   
                          params,
                          args=(atoms, self.calculator),
                          method='SLSQP',
                          bounds=Bounds(lb, ub, keep_feasible=False),
                          jac=True,
                          options={'ftol': self.fmax, 'maxiter': self.steps})
        
        opt_coords = result['x']
        positions, extra_coords = HSParamsHandler.unpack_atomic_params(len(atoms), self.ndof, opt_coords)
        atoms.positions=positions
        
        return atoms, extra_coords


    def get_bounds(self, atoms, box,  covrad_inside):
        lb , ub = self.setup_limits(atoms, box, covrad_inside)
        lb , ub = self.constrain_atoms(atoms, lb, ub)
        
        return lb, ub
    
    
    def setup_limits(self, atoms, box, covrad_inside):
        
        covrad_inside=np.array(covrad_inside,dtype=int)
            
        atomic_radii=[  covalent_radii[atoms[i].number]*covrad_inside  for i in range(len(atoms))   ]
        
        n_atoms=len(atoms)
        lb = [box[i][0] for i in range(self.ndof)]*n_atoms
        ub = [box[i][1] for i in range(self.ndof)]*n_atoms
        
        lb=np.array(lb) + np.array(atomic_radii).flatten()
        ub=np.array(ub) - np.array(atomic_radii).flatten()    
        
        return lb, ub             
      
    
    
     
    
    
    
class HighDimRandomBox(HighDimRandomStructure, RandomBox):

        
    def __init__(self, atoms, box=[(0., 1.), (0., 1.), (0., 1.), (0.,1.)],
                 covrad_inside=None, world_center=None, **kwargs):        
            
        if world_center is None:
            world_center=[atoms.cell.lengths()[0]/2]*len(box) 
            
        self.world_center=world_center

        
        assert(len(self.world_center)==len(box))
        
        HighDimRandomStructure.__init__(self, atoms.copy(), dims=len(box),
                                        world_center=self.world_center, **kwargs)
    
        self.atoms=atoms
        self.box=box 
        self.dims=len(box)
        self.relaxer=None
    
        if covrad_inside is None:    
            self.covrad_inside=[True]*max(len(box),3)
        else:
            self.covrad_inside=covrad_inside
        
        
        self.extra_dims=len(box)-3
    
    
    def get_extra_coords(self, atoms):
        
        extra_coords=np.zeros( (len(self.atoms), self.extra_dims) )
        
        for atom in atoms:            
            if atom.index not in self.constr.cindex:
                rc=self.get_rc(atom)
                for dim_idx in range(self.extra_dims):
                    extra_coords[atom.index, dim_idx]=self.get_coord(rc, dim_idx+3)
            else:
                for dim_idx in range(self.extra_dims):
                    extra_coords[atom.index, dim_idx]=self.world_center[dim_idx+3]
                    
        return extra_coords

    
    def get_rc(self, atom):
        rc=np.zeros(len(self.box))
        rc[self.covrad_inside]=covalent_radii[atom.number]
        
        return rc
    
    
    def get(self):
        
        newatoms=self.atoms.copy()
        
        newatoms=self.get_positions(newatoms)
        
        if self.extra_dims>0:
            extra_coords = self.get_extra_coords(newatoms)
        else:
            extra_coords=None
       
        
        if self.relaxer is not None:
            newatoms, extra_coords  = self.relaxer.run(newatoms, extra_coords)
        
        newatoms.extra_coords=extra_coords
        
        newatoms.wrap()
        newatoms.calc = None
     
        return newatoms


class HighDimRandomCell(HighDimRandomStructure, RandomCell):
    
    def __init__(self, atoms, fixed_cell_params=None, world_center=None, extra_size=None,  **kwargs):        
        
        
        '''
        fixed_cell_params : list of 6 booleans
            Whether to keep the cell parameters fixed, respectively in Voigt form
            If None, all cell parameters are given by random (if randomcell==True)
        '''

        
        
        if world_center is None:
            world_center=[] 
            for i in range(3):
                cell=atoms.get_cell() 
                world_center.append(cell[i][i])
                
            if extra_size is not None:
                for i in range(len(extra_size)):
                    world_center.append(extra_size[i]/2)
            
        ndims=len(world_center)    

        self.world_center=world_center
        self.extra_dims= ndims - 3
        self.extra_size=extra_size
        
        assert(len(self.world_center)==ndims)

        HighDimRandomStructure.__init__(self, atoms.copy(), dims=ndims,
                                        world_center=self.world_center, **kwargs)
     
        if  fixed_cell_params is None:
            fixed_cell_params = [False]*6 
            
        self.fixed_cell_3x3 = RandomCell.get_fixed_cell_params(fixed_cell_params)
        
        self.relaxer=None

    def get(self):
        cell = RandomCell.get_random_cellparams(self.atoms, self.rng,
                                                fixed_cell_3x3=self.fixed_cell_3x3)

        newatoms = self.atoms.copy()
        coords=self.get_new_positions(cell)
        
        newatoms.positions = coords
        newatoms.cell = cell
        
        
        if self.extra_dims>0:
            extra_coords=self.get_extra_coords(self.extra_size)
        else:
            extra_coords=None
        
        if self.relaxer is not None:
            newatoms, extra_coords = self.relaxer.run(newatoms, extra_coords)
        
        newatoms.extra_coords=extra_coords
        
        newatoms.wrap()
        newatoms.calc = None
        
        return newatoms
    
    def get_extra_coords(self, extra_size):
        
        extra_coords=np.zeros( (len(self.atoms), self.extra_dims) )
        
        for atom in self.atoms:

            if atom.index in self.constr.cindex:
                for dim_idx in range(self.extra_dims):
                    extra_coords[atom.index,dim_idx]=self.world_center[3+dim_idx]
            
            else:
                for dim_idx in range(self.extra_dims):
                    extra_coords[atom.index,dim_idx]=extra_size[dim_idx]*self.rng.random()
        
        return extra_coords
        
    
class HighDimBoxConstructor:
    
    @staticmethod
    def get_box(atoms, dims=4, volume_fraction=0.2, free_space=None, cell=None):
        
        box_length=HighDimBoxConstructor.get_box_length(atoms, volume_fraction, dims)

        if cell is None:
            cell=HighDimBoxConstructor.get_unitcell(box_length, free_space)
        
        box = HighDimBoxConstructor.construct_box(cell, box_length, dims)
        
        return box, cell


    @staticmethod
    def get_box_length(atoms, volume_fraction, dims):
        
        rc =np.array( [    covalent_radii[atoms[i].number]  for i in range(len(atoms))   ]  )
        
        if dims==1:
            V_atoms=sum(2*rc)
        elif dims==2:
            V_atoms=sum(np.pi*(rc**2) )
        elif dims==3:
            V_atoms=sum(  (4/3)*np.pi*(rc**3)  )
        elif dims==4:
            V_atoms=sum(  (1/2)*(np.pi**2)*(rc**4)  )
        elif dims==5:
            V_atoms=sum(  (8/15)*(np.pi**2)*(rc**5)  )
        
        box_length=(V_atoms/volume_fraction)**(1/dims)
        
        return box_length
    
    @staticmethod
    def construct_box(cell, box_length, dims): 
    
        box_start=cell[0]/2-box_length/2
        box_end=cell[0]/2+box_length/2
                
        box=[(box_start, box_end)]*dims
        
        if len(box)==2:
            box.append(  (cell[0]/2, cell[0]/2)  )
        elif len(box)==1: 
            box.append(  (cell[0]/2, cell[0]/2) )
            box.append(  (cell[0]/2, cell[0]/2) )
        
        return box
    
    @staticmethod
    def get_unitcell(box_length, free_space):
            
        if free_space is None:
            cell_L=box_length
        else:
            cell_L=box_length+2*free_space
       
        cell=[cell_L, cell_L, cell_L]
        
        return cell
    