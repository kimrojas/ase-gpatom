#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 13:24:03 2023

@author: casper
"""


import numpy as np
from gpatom.gpfp.prior import ConstantPrior, CalculatorPrior, RepulsivePotential, RepulsivePotentialWithStress

from ase.calculators.calculator import Calculator, all_changes
from ase.data import covalent_radii
from ase.neighborlist import NeighborList

class HighDimConstantPrior(ConstantPrior):

    def potential(self, x):
        d = len(x.atoms) * x.dims  
        output = np.zeros(d + 1)
        output[0] = self.constant
        return output


class HighDimCalculatorPrior(CalculatorPrior):    

    def __init__(self, calculator, constant, **kwargs):
        CalculatorPrior.__init__(calculator, constant, **kwargs)

        self.calculator = calculator
        self.constant = constant
        
    def get_atoms(self,x):    
        atoms=x.atoms.copy()
    
        extra_coords=x.extra_coords
        atoms.extra_coords=extra_coords
        
        dims=x.dims
        atoms.dims=dims
        
        return atoms


    def setup_output(self, atoms):
        d = len(atoms) * atoms.dims
        output = np.zeros(d + 1)   
        return output
        
    
class HighDimRepulsivePotential(RepulsivePotential):
    
    implemented_properties = ['energy', 'forces']

    default_parameters = {'prefactor': 0.7,'rc': None, 'potential_type': 'LJ'}    
    
    nolabel = True
  
    def setup_energy_forces(self, atoms):
        energy = 0.0
        forces = np.zeros((len(atoms),atoms.dims))  
        return energy, forces    


    def get_distance_info(self, atoms, atom_idx, neighbor_list):
        
            positions = atoms.positions
            
            cell = atoms.cell
        
            neighbors, offsets = neighbor_list.get_neighbors(atom_idx)
            
            cells = np.dot(offsets, cell)

            d = positions[neighbors] + cells - positions[atom_idx]
                
            if atoms.extra_coords is not None:
                extra_coords=atoms.extra_coords
                d_ext=extra_coords[neighbors]-extra_coords[atom_idx]
                d=np.hstack((d,d_ext))
    
            return neighbors, d





class HighDimPotentialWithStress(RepulsivePotentialWithStress):
    implemented_properties = ['energy', 'forces', 'stress']

    default_parameters = {'prefactor': 0.7,'rc': None, 
                          'potential_type': 'LJ', 'extrapotential': None}    
    
    nolabel = True

    def setup_energy_forces(self, atoms):
        energy = 0.0
        
        forces = np.zeros((len(atoms),   max(atoms.dims, 3)))  
        return energy, forces    


    def get_distance_info(self, atoms, atom_idx, neighbor_list):
        
            positions = atoms.positions
            
            cell = atoms.cell
        
            neighbors, offsets = neighbor_list.get_neighbors(atom_idx)
            
            cells = np.dot(offsets, cell)

            d = positions[neighbors] + cells - positions[atom_idx]

            if atoms.extra_coords is not None:
                extra_coords=atoms.extra_coords
                d_ext=extra_coords[neighbors]-extra_coords[atom_idx]
                d=np.hstack((d,d_ext))
    
            return neighbors, d
        
        
    def update_stress(self, stress, derivative, d):

        stress -= np.dot(derivative[:,:3].T, d[:,:3])  

        return stress        



class SB_HighDimRepulsivePotential(HighDimRepulsivePotential):    
    
    ''' Repulsive potential of the form
    sum_ij (0.7 * (Ri + Rj) / rij)**12

    where Ri and Rj are the covalent radii of atoms
    '''

    implemented_properties = ['energy', 'forces']

    default_parameters = {'prefactor_hD': 0.7,'rc_hD': None, 
                          'prefactor_lD': 0.7,'rc_lD': None, 
                          'projection_dims': 3,
                          'potential_type':'LJ'}    
    
    nolabel = True



    def calculate(self, atoms=None,
                  properties=['energy', 'forces'],
                  system_changes=all_changes):
        
        
        Calculator.calculate(self, atoms, properties, system_changes)
                 
        rc_hD, rc_lD, prefactor_hD, prefactor_lD = self.get_parameters(atoms)
        
        neighbor_list_hD = NeighborList(np.array(rc_hD), self_interaction=False)   
        neighbor_list_hD.update(atoms)
        energy_hD, forces_hD =self.get_energy_and_forces(atoms, rc_hD, prefactor_hD, neighbor_list_hD)


        neighbor_list_lD = NeighborList(np.array(rc_lD), self_interaction=False)   
        neighbor_list_lD.update(atoms)        
        energy_lD, forces_lD = self.get_projected_energy_and_forces(atoms, rc_lD, prefactor_lD, neighbor_list_lD)

        # set energy and forces
        self.results['energy'] = energy_hD + energy_lD
        self.results['forces'] = forces_hD + forces_lD
    
        

    def get_projected_energy_and_forces(self, atoms, rc, prefactor, neighbor_list_lD):
        expanded_forces=np.zeros(   (len(atoms), atoms.dims)     )
        
        projected_atoms=self.project_atoms(atoms)
        
        energy, forces =self.get_energy_and_forces(projected_atoms, rc, prefactor, neighbor_list_lD)
             
        expanded_forces[:,0:3]=forces
        
        return energy, expanded_forces    
    
    
    def get_energy_and_forces(self, atoms, rc, prefactor, neighbor_list):

        energy, forces = self.setup_energy_forces(atoms)   
        
        for a1 in range(len(atoms)):
            
            neighbors, d = self.get_distance_info(atoms, a1, neighbor_list)
            
            if len(neighbors)>0:     
                
                potetial, derivative = self.get_potential(a1, rc, prefactor, d, neighbors)   
                
                energy = self.update_energy(energy, potetial)
                
                forces = self.update_forces(forces, derivative, a1, neighbors) 
                
        return energy, forces
    
    
    
    
    def get_parameters(self, atoms):
      
        prefactor_hD = self.parameters.prefactor_hD
        prefactor_lD = self.parameters.prefactor_lD
        
        
        if self.parameters.rc_hD is None:
            rc_hD = [ covalent_radii[self.atoms[i].number]  for i in range(len(self.atoms))   ]
            rc_hD=np.array(rc_hD)
        else:   
            if type(self.parameters.rc_hD)==list:
                rc_hD=self.parameters.rc_hD
            else:
                rc_hD=np.ones(len(atoms))*self.parameters.rc_hD
                
                
        if self.parameters.rc_lD is None:
            rc_lD=0.1*rc_hD
        else:
            if type(self.parameters.rc_lD)==list:
                rc_lD=self.parameters.rc_lD
            else:
                rc_lD=np.ones(len(atoms))*self.parameters.rc_lD
    
        return np.array(rc_hD), np.array(rc_lD), prefactor_hD, prefactor_lD
    
    
    
    def project_atoms(self, atoms): 
        
        projected_atoms=atoms.copy()
        
        coords=projected_atoms.get_positions()
        
        if self.parameters.projection_dims==2:
          coords[:,2:3]=np.zeros( (len(atoms),1 ))
        elif self.parameters.projection_dims==1:
          coords[:,1:3]=np.zeros( (len(atoms),2 ))
              
        projected_atoms.positions=coords
        projected_atoms.dims=3
        projected_atoms.extra_coords=None
        
        return projected_atoms
    
    