import numpy as np
from gpatom.gpfp.fingerprint import (RadialFP, RadialFPCalculator,
                                     RadialFPGradientCalculator,
                                     RadialAngularFPGradientCalculator,
                                     RadialAngularFP, RadialAngularFPCalculator,
                                     AtomPairs, AtomTriples,
                                     FPElements, FPElementsForTriples,
                                     RadialFPStrainCalculator,
                                     RadialAngularFPStrainCalculator, FPTools)



class HighDimFingerPrint:
    
    def __init__(self, fp_args={}, angular=True, 
                 calc_gradients=True, calc_strain=False ):

        self.fp_args=fp_args
        self.calc_gradients=calc_gradients
        self.calc_strain=calc_strain
        
        if angular:
            self.fp_class=HighDimRadialAngularFP
        else:
            self.fp_class=HighDimRadialFP
        
    def get(self, atoms, extra_coords=None):
        
            return self.fp_class(atoms, extra_coords,
                                 calc_gradients=self.calc_gradients,
                                 calc_strain=self.calc_strain,
                                 **self.fp_args)
        
        
class HighDimRadialFP(RadialFP):

    def __init__(self, atoms, extra_coords=None, calc_gradients=True, 
                 calc_strain=False, **kwargs):
        ''' Parameters:

        r_cutoff: float
            Threshold for radial fingerprint (Angstroms)

        r_delta: float
            Width of Gaussian broadening in radial fingerprint
            (Angstroms)

        r_nbins: int
            Number of bins in radial fingerprint

        calc_gradients: bool
            Whether gradients are calculated
        '''

        
        self.dims = CoordExtender.get_dimension(extra_coords)
        self.extra_coords=extra_coords

        default_parameters = {'r_cutoff': 8.0,
                              'r_delta': 0.4,
                              'r_nbins': 200}

        self.params = default_parameters.copy()
        self.params.update(kwargs)

        self.calc_gradients = calc_gradients

        # parameters in this class for constructing the fingerprint:
        self.param_names = ['r_cutoff', 'r_delta', 'r_nbins']


        self.atoms = atoms.copy()
        self.atoms.wrap()

        self.pairs = HighDimAtomPairs(self.atoms,
                                      extra_coords,
                                      self.params['r_cutoff'])

        fpparams = dict(pairs=self.pairs,
                        cutoff=self.params['r_cutoff'],
                        width=self.params['r_delta'],
                        nbins=self.params['r_nbins'])

        self.rho_R = RadialFPCalculator.calculate(**fpparams)
        self.vector = self.rho_R.flatten()


        self.gradients = (HighDimRadialFPGradientCalculator.
                          calculate(natoms=self.natoms, dimension=self.dims, 
                                    **fpparams)  
                          if self.calc_gradients else None)

        self.strain = (HighDimRadialFPStrainCalculator.calculate(**fpparams)
                       if calc_strain else None)
        
    def reduce_coord_gradients(self):
        '''
        Reshape gradients by flattening the element-to-element
        contributions.
        '''
        return self.gradients.reshape(self.natoms, -1, self.dims) 
    
    
    
    
    
    
class HighDimRadialAngularFP(HighDimRadialFP, RadialAngularFP):
    def __init__(self, atoms, extra_coords=None, calc_gradients=True, 
                 calc_strain=False, **kwargs):
        ''' Parameters:

        a_cutoff: float
                Threshold for angular fingerprint (Angstroms)

        a_delta: float
                Width of Gaussian broadening in angular fingerprint
               (Radians)

        a_nbins: int
            Number of bins in angular fingerprint

        aweight: float
            Scaling factor for the angular fingerprint; the angular
            fingerprint is multiplied by this number

        '''
        
        HighDimRadialFP.__init__(self, atoms, extra_coords=extra_coords, 
                                 calc_gradients=calc_gradients, 
                                 calc_strain=calc_strain, **kwargs)

        self.dims = CoordExtender.get_dimension(extra_coords)
        self.extra_coords=extra_coords

        default_parameters = {'r_cutoff': 8.0,
                              'r_delta': 0.4,
                              'r_nbins': 200,
                              'a_cutoff': 4.0,
                              'a_delta': 0.4,
                              'a_nbins': 100,
                              'gamma': 0.5,
                              'aweight': 1.0}

        self.params = default_parameters.copy()
        self.params.update(kwargs)
        
        
        # parameters in this class for constructing the fingerprint:
        self.param_names = ['r_cutoff', 'r_delta', 'r_nbins',
                            'a_cutoff', 'a_delta', 'a_nbins',
                            'aweight']

        assert self.params['r_cutoff'] >= self.params['a_cutoff']

        self.triples = HighDimAtomTriples(self.atoms,          
                                          extra_coords,
                                          cutoff=self.params['r_cutoff'],                     
                                          cutoff2=self.params['a_cutoff'])
                                                                                        
                                                                                        
        fpparams = dict(triples=self.triples,                                           
                        cutoff=self.params['a_cutoff'],
                        width=self.params['a_delta'],
                        nbins=self.params['a_nbins'],
                        aweight=self.params['aweight'],
                        gamma=self.params['gamma'])

        self.rho_a = RadialAngularFPCalculator.calculate(**fpparams)
        self.vector = np.concatenate((self.rho_R.flatten(),
                                      self.rho_a.flatten()), axis=None)
           

        calc_gradients = (self.calc_gradients if calc_gradients is None
                          else calc_gradients)

        self.anglegradients = (HighDimRadialAngularFPGradientCalculator.
                               calculate(natoms=self.natoms, dimension=self.dims,
                                         **fpparams)
                               if calc_gradients else None)
        
        self.anglestrain = (HighDimRadialAngularFPStrainCalculator.
                            calculate(**fpparams)
                            if calc_strain else None)
        
        
    def reduce_coord_gradients(self):
        '''
        Reshape gradients by flattening the element-to-element
        contributions and all angles, and concatenate those arrays.
        '''
        
        return np.concatenate((self.gradients.reshape(self.natoms, -1, self.dims),
                               self.anglegradients.reshape(self.natoms, -1, self.dims)),
                              axis=1)
    


class HighDimRadialFPGradientCalculator(RadialFPGradientCalculator):

    @classmethod
    def calculate(self, natoms, pairs, cutoff,
                  width, nbins, dimension):

        gradients = np.zeros([natoms, pairs.elem.nelem,
                              pairs.elem.nelem, nbins, dimension])
        
        if pairs.empty:
            return gradients

        results=self.get_results(pairs, cutoff, nbins, width)
    
        gradients=self.calculate_gradients(pairs, results, gradients, natoms)
           
        return gradients


class HighDimRadialAngularFPGradientCalculator(RadialAngularFPGradientCalculator):

    @classmethod
    def calculate(self, natoms, triples, cutoff, width, nbins, aweight,
                  gamma, dimension):

        gradients = np.zeros([natoms, triples.elem.nelem, triples.elem.nelem,
                              triples.elem.nelem, nbins, dimension])

        if triples.empty:
            return gradients

        firsts, seconds, thirds = self.do_anglegradient_math(triples, cutoff,
                                                             width, nbins,
                                                             aweight, gamma)

        gradients=self.calculate_gradients(triples, firsts, seconds, 
                                           thirds, gradients, natoms)

        return np.array(gradients)




class HighDimAtomPairs(AtomPairs):

  
    def __init__(self, atoms, extra_coords, cutoff):
        
        primary_atoms, extended_atoms = self.extend_atoms(atoms, cutoff)
        
        ap=CoordExtender.get_extended_coords(primary_atoms, extra_coords)
        ep=CoordExtender.get_extended_coords(extended_atoms, extra_coords)
                
        self.indices = self.get_pair_index_list(ap, ep, cutoff)

        self.get_vectors_and_distances(ap, ep, self.indices)        

        self.elem = FPElements(atoms1=primary_atoms,
                               atoms2=extended_atoms,
                               indices=self.indices) 
    


class HighDimAtomTriples(AtomTriples):

    def __init__(self, atoms, extra_coords, cutoff, cutoff2):

        primary_atoms, extended_atoms= AtomTriples.extend_atoms(atoms, cutoff, cutoff2)

        ap=CoordExtender.get_extended_coords(primary_atoms, extra_coords)
        ep=CoordExtender.get_extended_coords(extended_atoms, extra_coords)
        
        self.indices = self.get_angle_index_list(ap, ep, cutoff2)

        self.get_vectors_and_angles(ap, ep, self.indices)

        self.elem = FPElementsForTriples(atoms1=primary_atoms,
                                         atoms2=extended_atoms,
                                         indices=self.indices) 
    
    
class CoordExtender:
            
    @staticmethod 
    def get_dimension(extra_coords):
        
        
        if extra_coords is None:
            dimension=3
        else:
            dimension = np.shape(extra_coords)[1]+3  # this 3 is the 3 normal dimensions
            
        return dimension
    
    
    @staticmethod
    def get_extended_coords(atoms, extra_coords):
        
        old_coords=atoms.positions
        
        if extra_coords is None:
            return old_coords
        
        n_coords=len(extra_coords)
                
        n_coords=np.shape(extra_coords)[0]
        
        n_extradims=np.shape(extra_coords)[1]
        
        n_reps=int(len(atoms)/n_coords)
        
        extra_coords=np.tile(extra_coords.reshape(n_extradims,n_coords) , n_reps)
        
        extended_coords=np.hstack((old_coords , extra_coords.reshape(n_coords*n_reps,n_extradims)))
        
        return extended_coords
   



class HighDimRadialFPStrainCalculator(RadialFPStrainCalculator):

    @classmethod
    def calculate(cls, pairs, cutoff, width, nbins):

        ''' Derivative of fingerprint w.r.t. cell parameters '''
        result = np.zeros([pairs.elem.nelem, pairs.elem.nelem, nbins, 3, 3])

        if pairs.empty:
            return result
        
        gradients = cls.get_gradients(pairs, cutoff, nbins, width)
        
        gradients_3d=gradients[:,:,:3,:3]
        # gradients_3d=gradients
        result = FPTools.sum_in_groups(values=gradients_3d,
                                       indexlist=pairs.elem.indices,
                                       nelem=pairs.elem.nelem)
    
        return result
    
    
    
class HighDimRadialAngularFPStrainCalculator(RadialAngularFPStrainCalculator):

    @classmethod
    def calculate(cls, triples, cutoff, width, nbins, aweight, gamma):
        '''
        Derivative of the fingerprint vector w.r.t. the cell
        parameters.

        This method has not been optimized in speed.
        '''
        result = np.zeros([triples.elem.nelem, triples.elem.nelem,
                           triples.elem.nelem, nbins, 3, 3])

        if triples.empty:
            return result

        results=cls.get_results(triples, cutoff, width, nbins, aweight, gamma)
        
        results_3d=results[:,:,:3,:3]
        #results_3d=results        
       
        result = FPTools.sum_in_groups(values=results_3d,
                                       indexlist=triples.elem.indices,
                                       nelem=triples.elem.nelem)
        
        return result
    
    