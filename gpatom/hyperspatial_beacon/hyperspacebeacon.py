#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  6 13:28:13 2023

@author: casper
"""

from gpatom.beacon.beacon import SurrogateOptimizer
import numpy as np
from scipy.optimize import minimize, Bounds
import warnings
from ase.calculators.singlepoint import SinglePointCalculator
from ase.io import write
from ase.neighborlist import NeighborList
from ase.constraints import FixAtoms

        
class HighDimSurrogateOptimizer(SurrogateOptimizer):
    '''
    Small optimizer class to handle local optimizations in BEACON.
    '''

    def __init__(self, fmax=0.05, relax_steps=100, after_steps=50, cycles=1,
                 write_surropt_trajs=False, world_center=None, world_size=None,
                 from_dims=4, to_dims=3 ,width=None, rate=0.5,
                 rattle=True, rattle_strength=0.05, potential_type=0, penalty_type=0,
                 rattle_rng=np.random): 
        
        
        assert(from_dims>=to_dims)
        assert(to_dims<4)
        
        self.fmax = fmax
        self.cycles=cycles
        self.relax_steps = relax_steps
        self.after_steps=after_steps
        self.write_surropt_trajs = write_surropt_trajs        
        self.world_center=world_center
        self.rattle_rng=rattle_rng
        
        self.penalty_type=penalty_type
        self.potential_type=potential_type
        
        self.from_dims=from_dims
        
        self.to_dims=to_dims
        
        self.rate=rate
        self.rattle=rattle
        self.rattle_strength=rattle_strength

   

        if width is None:
            if penalty_type==0:
                self.width=np.arccosh(1/0.01)/(world_size/2)
            elif penalty_type==1:
                self.width=1/(world_size/2)**2
                             
        else: 
            self.width=width
        


        if world_center is None:
            self.world_center=[None]*from_dims
            for i in range(to_dims, from_dims):
                self.world_center[i]=(world_size/2)
        else: 
            self.world_center=world_center

        
    def _calculate_properties(self, params, *args):
        '''
        Function to be minimized. Returns the predicted energy and
        its gradients w.r.t. both coordinates and fractions.
        '''        
        writer = args[0]
        atoms = args[1]
        model=args[2]
        dims=args[3]
        penalize=args[4]
        

        if dims>3:       
            all_coords=params.reshape(len(atoms), dims)
            coords = all_coords[:, :3] 
            h_coords = all_coords[:, 3:]    
            penalty_coords=all_coords[:, self.to_dims:]  
        else:
            coords = params.reshape(len(atoms),3)
            h_coords=None
            penalty_coords=coords[:, self.to_dims:] 


        atoms = atoms.copy()   
        
    
        atoms.positions = coords
    
        
        fp=model.fp.get(atoms=atoms, extra_coords=h_coords) 
        
        
        results, variance = model.gp.predict(fp, get_variance=False) 
        
        
        energy = results[0]
        
        energy_print=energy
        
        derivatives = results[1:]      

        if penalize:
            
            
            steps_taken=writer.step_count
            strength=self.get_strength(steps_taken) 
            
            HS_energy_minus, HS_energy_plus, HS_derivatives = self.HS_penalty(penalty_coords, strength) 
            
            energy+=HS_energy_minus
            energy_print+=HS_energy_plus

            derivatives+=HS_derivatives.flatten()
            
        writer.set_atoms(atoms)   
        
        writer.set_properties(dims=dims, energy=energy_print, gradients=derivatives)

         
        return (energy , np.array(derivatives))        
        
        
    def get_strength(self, steps_taken):
        if self.potential_type==0:
            strength=self.rate*steps_taken
        elif self.potential_type==1:
            strength=0.01*(self.rate**steps_taken)

        return strength


    



    def HS_penalty(self, penalty_coords, strength):      
        
        
        penalty_coords_mod=penalty_coords-self.world_center[self.to_dims:self.from_dims]
        
        HS_energy_minus=0
        HS_energy_plus=0
        HS_derivatives=np.zeros((len(penalty_coords) , self.from_dims)) 
         
        for idx, x in enumerate(penalty_coords_mod):

            if self.penalty_type==0:

                u=np.sign(x)*np.minimum(abs(self.width*x),10)
                eng_x=-strength/np.cosh(u)
                HS_energy_minus += np.sum(eng_x) 
                HS_energy_plus += np.sum(eng_x + strength )  
                HS_derivatives[idx,self.to_dims:] = strength*self.width*np.sinh(u)/(np.cosh(u)**2)  
            elif self.penalty_type==1:
                eng_x=strength * self.width * np.linalg.norm(x,axis=0)**2
                HS_energy_minus +=  eng_x - strength
                HS_energy_plus +=  eng_x 
                HS_derivatives[idx,self.to_dims:] = strength*self.width *2*x
        
        return HS_energy_minus, HS_energy_plus, HS_derivatives



    
    def get_params(self, atoms, h_coords):
        
     
    
        if h_coords is not None:
            h_coords.reshape(len(atoms), self.from_dims-3)
            params = np.concatenate((atoms.positions, h_coords), axis=1).flatten()        
        else:
            params=atoms.positions.flatten()
           
        return params
    
    

    
    def constrain_and_minimize(self, atoms, model, writer, dims, h_coords=None, steps=50, penalize=True):
        

        params=self.get_params(atoms, h_coords)
      
        
        lb, ub = self.constrain_atoms(atoms, dims)
        
    
        
        result = minimize(self._calculate_properties,   
                              params,
                              args=(writer, atoms, model, dims, penalize),
                              method='L-BFGS-B',
                              bounds=Bounds(lb, ub, keep_feasible=False),
                              jac=True,
                              options={'ftol':0, 'gtol':0., 'eps':1,  'maxiter': steps, 'maxls':20},
                              callback=writer.write_atoms)
                             
        success = result['success']
        
        opt_array = result['x'].reshape( len(atoms), max(dims,3) )
      
        atoms.positions=opt_array[:, :3].reshape(len(atoms), 3)
        
        
        
        
        if dims>3:
            extra_coords=opt_array[:,3:]
        else:
            extra_coords=None
        
        
        return success, atoms, extra_coords              
        
        
        
        
    def constrain_atoms(self, atoms, dims):
      
    
        n=max(dims,3) 
            
        constrained_atoms_indices=self.get_constrained_atoms_indices(atoms)
      
        lb=-np.inf*np.ones(len(atoms)*n)
        ub=np.inf*np.ones(len(atoms)*n)
        
        if len(constrained_atoms_indices)>0:
            for atom in atoms:
                if atom.index in constrained_atoms_indices:
                    idx=atom.index*n
                    for i in range( n ):
                       
                        if i<3:
                            lb[idx+i]=np.array( atom.position[i] )
                            ub[idx+i]=lb[idx+i]
                        else:
                            lb[idx+i]=self.world_center[i]
                            ub[idx+i]=lb[idx+i]

                        
        return lb, ub      
        


    def get_constrained_atoms_indices(self, atoms):    
        constrained_atoms_indices = []
        for C in atoms.constraints:
            if isinstance(C, FixAtoms):
                constrained_atoms_indices = C.index
        
        return constrained_atoms_indices

        
    def initiate_writer(self, atoms, model, h_coords, file_identifier):
              
        params=self.get_params(atoms, h_coords)
        
        
        if self.from_dims>3:    
            coord_center=self.world_center[3:self.from_dims]
        else:
            coord_center=None
 
    
            
        writer = OptimizationWriter(atoms=atoms, coord_center=coord_center,
                                    file_identifier=file_identifier,
                                    write_surropt_trajs=self.write_surropt_trajs)
    
    
        e, f = self._calculate_properties(params, writer, atoms, 
                                          model, self.from_dims, True)
        
        writer.write_atoms(params)
        
        return writer


        
        
    def relax(self, atoms, model, file_identifier=''):           
        
        # make something that checks for atoms.extra_coords here and
        # give an error message if not present. directing to the 
        # spacebeacon str_gen package.
        
        # if AGOX compatible, the 4D coordinates should be made inside of 
        # specabeacon relaxer, but thats too involved at this point. 
        # was attempted with an internal sgen/hgen, hence ignoring the 
        # beacon sgen. but I dont wanna do things double. 
        # untill better solution is made. lets just rely on atoms.extra_coords
        
        
        h_coords=atoms.extra_coords
        
        writer=self.initiate_writer(atoms, model, h_coords, file_identifier)
        
        for i in range(self.cycles):
        
            success, atoms, h_coords = self.constrain_and_minimize(atoms, model, 
                                                                   writer, self.from_dims,
                                                                   h_coords=h_coords,
                                                                   steps=self.relax_steps, 
                                                                   penalize=True)       
            
    
    
        if self.to_dims==3:
            projected_atoms=atoms
        else:
            projected_atoms=self.project_atoms(atoms)
        
        
        writer.hyperspatial_squeeze_complete=True

        if self.rattle:
            projected_atoms=self.rattle_close_atoms(projected_atoms)
       
        if hasattr(model.gp.prior, "calculator"):
            model.gp.prior.calculator.reset()  #necessary or prior wont calculate again and thus wont reset forces to the right size format
        
        success, opt_atoms, h_coords = self.constrain_and_minimize(projected_atoms, model, 
                                                                   writer, self.to_dims,
                                                                   h_coords=None, 
                                                                   steps=self.after_steps, 
                                                                   penalize=False)
       
        if writer.converged_forces:
            success=True
        
        
        return opt_atoms, success
        
     
                
        
    def project_atoms(self,atoms): 
        
        coords=atoms.get_positions()
                
        if self.to_dims==2:
            coords[:,2:3]=self.world_center[2]*np.ones( (len(atoms),1 ))
        elif self.to_dims==1:
            coords[:,1:3]=self.world_center[1:2]*np.ones( (len(atoms),2 ))
        
        atoms.positions=coords
        
        return atoms
      
        
       
    def rattle_close_atoms(self, atoms):  
        neighbor_list = NeighborList(0.1*np.ones(len(atoms)), self_interaction=False)     
        
        close_idx=self.get_close_atoms(atoms, neighbor_list)
        
        while len(close_idx)>0:
            atoms=self.rattle_atoms(atoms, close_idx) 
            close_idx=self.get_close_atoms(atoms, neighbor_list)
            
        return atoms
        
       
        
        
    def get_close_atoms(self,atoms, neighbor_list):  
        
        close_idx=[]
        
        neighbor_list.update(atoms)
        
        for idx in range(len(atoms)):
            neighbors, offsets = neighbor_list.get_neighbors(idx)
        
            if len(neighbors)>0:
                close_idx.append(idx)
                close_idx.extend(neighbors)
        
        close_idx=np.unique(close_idx)
        return close_idx
        
        
    
    def rattle_atoms(self,atoms,indices=None):

        if indices is None:
            indices=np.arange(len(atoms))

        
        pos=atoms.positions
            
        perturb=np.zeros(np.shape(pos))

        
        perturb[indices,0:self.to_dims:1]=self.rattle_rng.normal(loc=0.0, scale=self.rattle_strength, 
                                                                 size=(len(indices),self.to_dims))
        
        
        atoms.set_positions(pos + perturb)
                
        return atoms        
        
        
        
          

class OptimizationWriter:   
    '''
    Handles output of trajectories and atom fractions.
    '''

    def __init__(self, atoms, file_identifier, coord_center=None, write_surropt_trajs=False):
        
        
        self.atoms = atoms
        self.natoms = len(atoms)
        self.write_surropt_trajs = write_surropt_trajs
        self.step_count=0  
        self.coord_center=coord_center
        self.hyperspatial_squeeze_complete=False
        self.converged_forces=False

        if self.write_surropt_trajs:
            self.optfilename = 'opt_'+file_identifier+'.xyz'

            # format:
            f = open(self.optfilename, 'w')
            f.close()
            
            
        

    def set_properties(self, dims, energy, gradients):
        
        self.dims=dims
        self.energy = energy
        self.gradients = gradients.reshape(self.natoms, max(dims,3)  )[:, :3]
        
    def set_atoms(self, atoms):
        self.atoms = atoms
        self.natoms = len(atoms)


    def get_hyperdim_color(self, atoms, params):

        if self.dims>3:

            h_coords=self.get_h_coords(params)
                 
            if self.dims==4:
                atoms.set_initial_charges(charges=h_coords[:,0]) 
            elif self.dims==5:
                atoms.set_initial_charges(charges=h_coords[:,0])
                atoms.set_initial_magnetic_moments(magmoms=h_coords[:,1])

        return atoms


    def get_h_coords(self, params):

        h_coords = params.reshape(self.natoms, self.dims)[:, 3:]-self.coord_center
        return h_coords

    def write_atoms(self, params):
       
        self.step_count+=1
        
        if self.write_surropt_trajs:
            
            atoms = self.atoms.copy()
         
            
            coords = params.reshape(self.natoms, max(self.dims,3))[:, :3]


            atoms.positions = coords
            
     
            results = dict(energy=self.energy,
                           forces=-self.gradients)
            atoms.calc = SinglePointCalculator(atoms, **results)
            
            
            
            atoms=self.get_hyperdim_color(atoms, params)
            
            
            with warnings.catch_warnings():

                # with EMT, a warning is triggered while writing the
                # results in ase/io/extxyz.py. Lets filter that out:
                warnings.filterwarnings('ignore', category=UserWarning)

                write(self.optfilename,
                      atoms,
                      append=True,
                      parallel=False)     
                   
                
        if not self.hyperspatial_squeeze_complete:
            self.terminate_dimensional_squeezing(params)
        else:
            self.terminate_converged_forces(params)
        
        
    def terminate_dimensional_squeezing(self, params):
        h_coords=self.get_h_coords(params)
        h_coords_norms=np.linalg.norm(h_coords, axis=1)
        if all(h_coords_norms<0.01):
            print('squeeze complete')
            self.hyperspatial_squeeze_complete=True
            raise StopIteration



    def terminate_converged_forces(self, params):
        forces=-self.gradients
        force_norms=np.linalg.norm(forces, axis=1)
        
        if max(force_norms)<0.05:
            self.converged_forces=True
            print('force convergence complete')
            raise StopIteration
        

        
        