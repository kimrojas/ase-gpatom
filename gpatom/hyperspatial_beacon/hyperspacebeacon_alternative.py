#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  6 13:28:13 2023

@author: casper
"""

from gpatom.beacon.beacon import SurrogateOptimizer
import numpy as np
from scipy.optimize import minimize, Bounds
import warnings
from ase.calculators.singlepoint import SinglePointCalculator
from ase.io import write
from ase.neighborlist import NeighborList
from ase.constraints import FixAtoms

from gpatom.fractional_beacon.icebeacon import UnitCellHandler

        
class HighDimSurrogateOptimizer(SurrogateOptimizer):
    '''
    Small optimizer class to handle local optimizations in BEACON.
    '''

    def __init__(self, fmax=0.05, relax_steps=100, after_steps=50, cycles=1,
                 write_surropt_trajs=False, world_center=None, world_size=None,
                 from_dims=4, to_dims=3 ,width=None, rate=0.5, with_unit_cell=False, 
                 fixed_cell_params=None, rattle=True, rattle_strength=0.05, 
                 potential_type=0, penalty_type=0, rattle_rng=np.random): 
        
        assert(from_dims>=to_dims)
        assert(to_dims<4)
        
        self.fmax = fmax
        self.cycles=cycles
        self.relax_steps = relax_steps
        self.after_steps=after_steps
        self.write_surropt_trajs = write_surropt_trajs        
        self.world_center=world_center
        self.rattle_rng=rattle_rng
        
        self.penalty_type=penalty_type
        self.potential_type=potential_type
        
        self.from_dims=from_dims
        
        self.to_dims=to_dims
        
        self.rate=rate
        self.rattle=rattle
        self.rattle_strength=rattle_strength
   

        if width is None:
            if penalty_type==0:
                self.width=np.arccosh(1/0.01)/(world_size/2)
            elif penalty_type==1:
                self.width=1/(world_size/2)**2
                             
        else: 
            self.width=width
        


        if world_center is None:
            self.world_center=[None]*from_dims
            for i in range(to_dims, from_dims):
                self.world_center[i]=(world_size/2)
        else: 
            self.world_center=world_center


        self.with_unit_cell=with_unit_cell
                
        

        if self.with_unit_cell:
            self.relax_method=self.constrain_and_minimize_unitcell
        else:
            self.relax_method=self.constrain_and_minimize
            
        if self.with_unit_cell:
            if fixed_cell_params is None:
                fixed_cell_params = [False]*6    
            self.opt_cell_mask = np.array([not elem for elem in fixed_cell_params], dtype=int)

    
 
    def _calculate_properties(self, params, *args):
        '''
        Function to be minimized. Returns the predicted energy and
        its gradients w.r.t. both coordinates and fractions.
        '''        
        writer = args[0]
        atoms = args[1]
        natoms=len(atoms)
        
        model=args[2]
        dims=args[3]
        penalize=args[4]
        
        positions, extra_coords =  HSParamsHandler.unpack_atomic_params(natoms, dims, params)  
        atoms.positions=positions
        
        
        penalty_coords = HSParamsHandler.get_penalty_params(natoms, dims, self.to_dims, 
                                                            atoms.positions, extra_coords)

     
        fp=model.fp.get(atoms=atoms, extra_coords=extra_coords) 
        
        results, variance = model.gp.predict(fp, get_variance=False) 

        energy, atoms_forces = model.gp.translate_predictions(results, dims=dims)
        
        if penalize:
            
            strength=self.get_strength(self.steps_taken) 

            HS_energy_minus, HS_energy_plus, HS_forces = self.HS_penalty(penalty_coords, strength)

            energy+=HS_energy_plus

            atoms_forces+=HS_forces 
          

        d3_forces, extra_forces=HSParamsHandler.get_real_and_extra(natoms, dims, atoms_forces)
        writer.set_atoms(atoms=atoms, extra_coords=extra_coords, dims=dims, energy=energy, gradients=atoms_forces) 
         
        derivatives=HSParamsHandler.pack_atomic_params(natoms, self.from_dims, d3_forces, extra_forces)

        return (energy , np.array(derivatives))   
 
    

        
    def _calculate_properties_unitcell(self, params, *args):
        '''
        Function to be minimized. Returns the predicted energy and
        its gradients w.r.t. both coordinates and fractions.
        '''        
        writer = args[0]
        atoms = args[1]
        natoms=len(atoms)
        
        model=args[2]
        dims=args[3]
        penalize=args[4]
        
        original_cell=args[5]
        cell_factor=args[6]
      
        deformed_positions, extra_coords, deformation_tensor =  HSParamsHandler.unpack_params(natoms, dims, params)  
        
        atoms=UnitCellHandler.atoms_deformed_to_real(atoms, deformation_tensor, deformed_positions, original_cell, cell_factor)
        
        
        penalty_coords = HSParamsHandler.get_penalty_params(natoms, dims, self.to_dims, 
                                                            atoms.positions, extra_coords)

        fp=model.fp.get(atoms=atoms, extra_coords=extra_coords) 
    
        results, variance = model.gp.predict(fp, get_variance=False) 

        energy, atoms_forces = model.gp.translate_predictions(results, dims=dims)
        
        stress = model.gp.hs_predict_stress(fp)

        if penalize:
            
            strength=self.get_strength(self.steps_taken) 

            HS_energy_minus, HS_energy_plus, HS_forces = self.HS_penalty(penalty_coords, strength)
                        
            energy+=HS_energy_plus

            atoms_forces+=HS_forces

        d3_forces, extra_forces=HSParamsHandler.get_real_and_extra(natoms, dims, atoms_forces)
        
        writer.set_atoms(atoms=atoms, extra_coords=extra_coords, dims=dims, energy=energy, gradients=atoms_forces) 

        deformed_forces, deformed_virial = UnitCellHandler.forces_real_to_deformed(atoms, d3_forces, stress, original_cell, cell_factor)
        
        deformed_virial = UnitCellHandler.apply_cell_mask(deformed_virial, self.opt_cell_mask)   
         
        derivatives=HSParamsHandler.pack_params(natoms, self.from_dims, deformed_forces, extra_forces, deformed_virial)

        return (energy , np.array(derivatives))        
    

            
        
    def get_strength(self, steps_taken):
        if self.potential_type==0:
            strength=self.rate*steps_taken
        elif self.potential_type==1:
            strength=0.01*(self.rate**steps_taken)

        return strength


    



    def HS_penalty(self, penalty_coords, strength):      
        
        
        penalty_coords_mod=penalty_coords-self.world_center[self.to_dims:self.from_dims]
        
        HS_energy_minus=0
        HS_energy_plus=0
        HS_derivatives=np.zeros((len(penalty_coords) , self.from_dims)) 
         
        for idx, x in enumerate(penalty_coords_mod):

            if self.penalty_type==0:

                u=np.sign(x)*np.minimum(abs(self.width*x),10)
                eng_x=-strength/np.cosh(u)
                HS_energy_minus += np.sum(eng_x) 
                HS_energy_plus += np.sum(eng_x + strength )  
                HS_derivatives[idx,self.to_dims:] = strength*self.width*np.sinh(u)/(np.cosh(u)**2)  
            elif self.penalty_type==1:
                eng_x=strength * self.width * np.linalg.norm(x,axis=0)**2
                HS_energy_minus +=  eng_x - strength
                HS_energy_plus +=  eng_x 
                HS_derivatives[idx,self.to_dims:] = strength*self.width *2*x
        
        return HS_energy_minus, HS_energy_plus, HS_derivatives


    def constrain_and_minimize(self, atoms, model, writer, dims, h_coords=None, steps=50, penalize=True):
        
        natoms=len(atoms)

        params=HSParamsHandler.pack_atomic_params(natoms, self.from_dims, atoms.positions, h_coords)
      
        lb, ub = self.constrain_atoms(atoms, dims)
        
        result = minimize(self._calculate_properties,   
                              params,
                              args=(writer, atoms, model, dims, penalize),
                              method='L-BFGS-B',
                              bounds=Bounds(lb, ub, keep_feasible=False),
                              jac=True,
                              options={'ftol':0, 'gtol':0., 'eps':1,  'maxiter': steps, 'maxls':20},
                              callback=writer.write_atoms)
                             
        success = result['success']
        
        opt_array = result['x']
     
        positions, extra_coords = HSParamsHandler.unpack_atomic_params(natoms, dims, opt_array)  
        atoms.positions=positions
        
        return success, atoms, extra_coords   


    
    def constrain_and_minimize_unitcell(self, atoms, model, writer, dims, h_coords=None, steps=50, penalize=True):
        
        natoms=len(atoms)
        original_cell=atoms.get_cell()
        cell_factor=float(natoms)  


        deformation_tensor, deformed_positions = UnitCellHandler.atoms_real_to_deformed(atoms, original_cell, cell_factor)

        params=HSParamsHandler.pack_params(natoms, self.from_dims, deformed_positions, h_coords, deformation_tensor)
      
        lb, ub = self.constrain_atoms(atoms, dims)
        
        result = minimize(self._calculate_properties_unitcell,   
                              params,
                              args=(writer, atoms, model, dims, penalize, original_cell, cell_factor),
                              method='L-BFGS-B',
                              bounds=Bounds(lb, ub, keep_feasible=False),
                              jac=True,
                              options={'ftol':0, 'gtol':0., 'eps':1,  'maxiter': steps, 'maxls':20},
                              callback=writer.write_atoms)
                             
        success = result['success']
        
        opt_array = result['x']
     
        deformed_positions, extra_coords, deformation_tensor = HSParamsHandler.unpack_params(natoms, dims, opt_array)  
    
        atoms=UnitCellHandler.atoms_deformed_to_real(atoms, deformation_tensor, deformed_positions, original_cell, cell_factor)
        
        return success, atoms, extra_coords              
        


    def constrain_atoms(self, atoms, dims):
      
    
        n=max(dims,3) 
            
        constrained_atoms_indices=self.get_constrained_atoms_indices(atoms)
      
        if self.with_unit_cell:
            lb=-np.inf*np.ones(len(atoms)*n + 9)
            ub=np.inf*np.ones(len(atoms)*n + 9)
        else:
            lb=-np.inf*np.ones(len(atoms)*n)
            ub=np.inf*np.ones(len(atoms)*n)
            

        if len(constrained_atoms_indices)>0:
            for atom in atoms:
                if atom.index in constrained_atoms_indices:
                    idx=atom.index*n
                    for i in range( n ):
                       
                        if i<3:
                            lb[idx+i]=np.array( atom.position[i] )
                            ub[idx+i]=lb[idx+i]
                        else:
                            lb[idx+i]=self.world_center[i]
                            ub[idx+i]=lb[idx+i]

                        
        return lb, ub      
        


    def get_constrained_atoms_indices(self, atoms):    
        constrained_atoms_indices = []
        for C in atoms.constraints:
            if isinstance(C, FixAtoms):
                constrained_atoms_indices = C.index
        
        return constrained_atoms_indices

        
    def initiate_writer(self, atoms, model, h_coords, file_identifier):
        
        coord_center=self.world_center[self.to_dims:self.from_dims]
            
        writer = OptimizationWriter(atoms=atoms, from_dims=self.from_dims, 
                                    to_dims=self.to_dims,
                                    coord_center=coord_center,
                                    file_identifier=file_identifier,
                                    write_surropt_trajs=self.write_surropt_trajs)
    
        fp=model.fp.get(atoms=atoms, extra_coords=h_coords) 
        
        results, variance = model.gp.predict(fp, get_variance=False) 
        
        energy, atoms_forces = model.gp.translate_predictions(results, dims=self.from_dims)
        
        d3_forces, extra_forces=HSParamsHandler.get_real_and_extra(len(atoms), self.from_dims, atoms_forces)
        
        params=HSParamsHandler.pack_params(len(atoms), self.from_dims, atoms.positions, h_coords, atoms.cell)

        writer.set_atoms(atoms=atoms, extra_coords=h_coords, dims=self.from_dims, energy=energy, gradients=atoms_forces) 
        
        writer.write_atoms(params)
        
        return writer


        
        
    def relax(self, atoms, model, file_identifier=''):           
        
        # make something that checks for atoms.extra_coords here and
        # give an error message if not present. directing to the 
        # spacebeacon str_gen package.
        
        # if AGOX compatible, the 4D coordinates should be made inside of 
        # specabeacon relaxer, but thats too involved at this point. 
        # was attempted with an internal sgen/hgen, hence ignoring the 
        # beacon sgen. but I dont wanna do things double. 
        # untill better solution is made. lets just rely on atoms.extra_coords
        self.steps_taken=0
        
        
        if self.from_dims>3:
            h_coords=atoms.extra_coords
        else: 
            h_coords=None
        
        writer=self.initiate_writer(atoms, model, h_coords, file_identifier)
        
        self.steps_taken=0
        for i in range(self.cycles):
            if writer.hyperspatial_squeeze_complete:
                break
            success, atoms, h_coords = self.relax_method(atoms, model, 
                                                         writer, self.from_dims,
                                                         h_coords=h_coords,
                                                         steps=self.relax_steps, 
                                                         penalize=True)   
 
            self.steps_taken+=1

        if self.to_dims==3:
            projected_atoms=atoms
        else:
            projected_atoms=self.project_atoms(atoms)
        
        writer.hyperspatial_squeeze_complete=True

        if self.rattle:
            projected_atoms=self.rattle_close_atoms(projected_atoms)
       
        success, opt_atoms, h_coords = self.relax_method(projected_atoms, model, 
                                                         writer, self.to_dims,
                                                         h_coords=None, 
                                                         steps=self.after_steps, 
                                                         penalize=False)
       
        if writer.converged_forces:
            success=True
        
        return opt_atoms, success
                
        
    def project_atoms(self,atoms): 
        
        coords=atoms.get_positions()
                
        if self.to_dims==2:
            coords[:,2:3]=self.world_center[2]*np.ones( (len(atoms),1 ))
        elif self.to_dims==1:
            coords[:,1:3]=self.world_center[1:2]*np.ones( (len(atoms),2 ))
        
        atoms.positions=coords
        
        return atoms
      
        
    def rattle_close_atoms(self, atoms):  
        neighbor_list = NeighborList(0.1*np.ones(len(atoms)), self_interaction=False)     
        
        close_idx=self.get_close_atoms(atoms, neighbor_list)
        
        while len(close_idx)>0:
            atoms=self.rattle_atoms(atoms, close_idx) 
            close_idx=self.get_close_atoms(atoms, neighbor_list)
            
        return atoms
        
       
    def get_close_atoms(self,atoms, neighbor_list):  
        
        close_idx=[]
        
        neighbor_list.update(atoms)
        
        for idx in range(len(atoms)):
            neighbors, offsets = neighbor_list.get_neighbors(idx)
        
            if len(neighbors)>0:
                close_idx.append(idx)
                close_idx.extend(neighbors)
        
        close_idx=np.unique(close_idx)
        return close_idx
        
        
    def rattle_atoms(self,atoms,indices=None):

        if indices is None:
            indices=np.arange(len(atoms))

        pos=atoms.positions
            
        perturb=np.zeros(np.shape(pos))

        perturb[indices,0:self.to_dims:1]=self.rattle_rng.normal(loc=0.0, scale=self.rattle_strength, 
                                                                 size=(len(indices),self.to_dims))
        
        atoms.set_positions(pos + perturb)
                
        return atoms        
        

class HSParamsHandler:
    
    @staticmethod
    def get_real_and_extra(natoms, dims, atomic_params):
        
        assert np.shape(atomic_params)==(natoms,  max(dims,3))
        
        d3_params=atomic_params[:,:3]
        
        if dims>3:
            extra_params=atomic_params[:,3:]
        else:
            extra_params=None
    
        return d3_params, extra_params

    @staticmethod    
    def unpack_params(natoms, dims, params):
        
        atomic_params=params[0:-9] 
        cell_params=params[-9::].reshape(3,3)

        position_params, extra_position_params = HSParamsHandler.unpack_atomic_params(natoms, dims, atomic_params)
        return position_params, extra_position_params, cell_params
    
    @staticmethod 
    def unpack_atomic_params(natoms, dims, params):
        atomic_params=params.reshape(natoms, max(dims,3))
        if dims>3:       
            position_params = atomic_params[:, :3] 
            extra_position_params = atomic_params[:, 3:]    

        else:
            position_params = atomic_params  
            extra_position_params=None
            
        return position_params, extra_position_params
    
    @staticmethod 
    def get_penalty_params(natoms, dims, to_dims, position_params, extra_position_params):
        
        atomic_params = HSParamsHandler.pack_atomic_params(natoms, dims, position_params, extra_position_params)
        
        atomic_params=atomic_params.reshape(natoms,max(dims,3))
        
        penalty_position_params=atomic_params[:, to_dims:] 
        
        return penalty_position_params
    
    
    @staticmethod
    def pack_params(natoms, dims, position_params, extra_position_params, cell_params):
        
        assert np.shape(position_params)==(natoms,3)
        assert np.shape(cell_params)==(3,3)
    
        atomic_params=HSParamsHandler.pack_atomic_params(natoms, dims, position_params, extra_position_params)
            
        params= np.concatenate((atomic_params, cell_params.flatten()), axis=0)
           
        return params
    
    
    @staticmethod 
    def pack_atomic_params(natoms, dims, position_params, extra_position_params):
        if extra_position_params is not None:
            extra_position_params.reshape(natoms, dims-3)
            atomic_params = np.concatenate((position_params, extra_position_params), axis=1).flatten()        
        else:
            atomic_params=position_params.flatten()
        
        return atomic_params
    


class OptimizationWriter:   
    '''
    Handles output of trajectories and atom fractions.
    '''

    def __init__(self, atoms, from_dims, to_dims, file_identifier, coord_center=None, write_surropt_trajs=False):
        
        
        self.atoms = atoms
        self.from_dims=from_dims
        self.to_dims=to_dims
        self.natoms = len(atoms)
        self.write_surropt_trajs = write_surropt_trajs
        self.coord_center=coord_center
        
        if self.to_dims==self.from_dims:
            self.hyperspatial_squeeze_complete=True
        else:
            self.hyperspatial_squeeze_complete=False
            
        self.converged_forces=False

        if self.write_surropt_trajs:
            self.optfilename = 'opt_'+file_identifier+'.xyz'

            # format:
            f = open(self.optfilename, 'w')
            f.close()
            
            
        

    def set_atoms(self, atoms, extra_coords, dims, energy, gradients):
        self.atoms = atoms
        self.natoms = len(atoms)  
        self.extra_coords=extra_coords
        self.dims=dims
        self.energy = energy
        self.all_gradients = gradients  
        self.d3_gradients=gradients[:,:3]


    def get_hyperdim_color(self, atoms, extra_coords):

        if self.dims>3:

            h_coords=extra_coords-self.coord_center
                 
            if self.dims==4: 
                atoms.set_initial_charges(charges=h_coords[:,0]) 
            elif self.dims==5:
                atoms.set_initial_charges(charges=h_coords[:,0])
                atoms.set_initial_magnetic_moments(magmoms=h_coords[:,1])

        return atoms


    def write_atoms(self, params):

        if self.write_surropt_trajs:
            
            atoms = self.atoms.copy()
            
            penalty_coords = HSParamsHandler.get_penalty_params(self.natoms, self.dims, self.to_dims, 
                                                                self.atoms.positions, self.extra_coords)

     
            results = dict(energy=self.energy,
                           forces=-self.d3_gradients)
            atoms.calc = SinglePointCalculator(atoms, **results)
            
            
            atoms=self.get_hyperdim_color(atoms, self.extra_coords)
            
            with warnings.catch_warnings():

                # with EMT, a warning is triggered while writing the
                # results in ase/io/extxyz.py. Lets filter that out:
                warnings.filterwarnings('ignore', category=UserWarning)

                write(self.optfilename,
                      atoms,
                      append=True,
                      parallel=False)     
                   
        if not self.hyperspatial_squeeze_complete:
            terminate=self.terminate_dimensional_squeezing(penalty_coords)
            terminate=self.terminate_converged_forces()
        else:
            terminate=self.terminate_converged_forces()
            self.converged_forces=True

        if terminate:
            raise StopIteration

    def terminate_dimensional_squeezing(self, penalty_coords):
        h_coords=penalty_coords-self.coord_center
        h_coords_norms=np.linalg.norm(h_coords, axis=1)
        if all(h_coords_norms<0.01):
            self.hyperspatial_squeeze_complete=True
            print('squeeze complete')
            return True
            
    def terminate_converged_forces(self):
        forces=-self.all_gradients
        force_norms=np.linalg.norm(forces, axis=1)
        if max(force_norms)<0.05:
            print('forces converged')
            return True

        

        
        