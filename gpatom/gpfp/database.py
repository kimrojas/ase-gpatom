from gpatom.gpfp.kerneltypes import EuclideanDistance


class Database:

    def __init__(self, fingerprints=tuple(), energies=tuple(),
                 forces=tuple()):

        if not (len(fingerprints) == len(energies) == len(forces)):
            raise ValueError('Length of all input data do not match.')

        self.fingerprintlist = list(fingerprints)
        self.energylist = list(energies)
        self.forceslist = list(forces)

    def __eq__(self, db):
        if len(self) != len(db):
            return False

        # Compare distances of fingerprints:
        for i in range(len(self)):
            for j in range(len(self)):
                fp1 = self.fingerprintlist[i]
                fp2 = db.fingerprintlist[i]
                if EuclideanDistance(fp1, fp2) < 1e-4:
                    return False

        return True

    def __len__(self):
        '''
        Return number of data points in the database.
        '''
        return len(self.fingerprintlist)

    def copy(self):
        return Database(self.fingerprintlist, self.energylist,
                        self.forceslist)

    def add(self, fingerprint, energy, forces):
        '''
        Add data to database.
        '''
        self.fingerprintlist.append(fingerprint)
        self.energylist.append(energy)
        self.forceslist.append(forces)

    def get_energyforce_array(self, index, negative_forces=True):
        '''
        Return a list where the first element is energy, and
        the last element is forces for the given index.
        '''
        factor = -1 if negative_forces else 1
        return ([self.energylist[index]] +
                list(factor * self.forceslist[index].flatten()))

    def get_all_fingerprints(self):
        return self.fingerprintlist

    def get_all_energyforces(self, negative_forces=True):
        '''
        Return all energies and forces, ordered as
        [e1, f11x, f11y, f11z, f12x, f12y, ...f1Nz, e2, f21x, f21y,
        f21z, f22x, f22y, ...]
        '''
        listofall = []
        for i in range(len(self.energylist)):
            listofall += self.get_energyforce_array(i,
                        negative_forces=negative_forces)
        return listofall



        
        