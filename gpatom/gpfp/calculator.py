import warnings
from copy import copy

import numpy as np
from ase.calculators.calculator import Calculator, all_changes
from ase.calculators.singlepoint import SinglePointCalculator



def copy_image(atoms):
    '''
    Copy an image, so that it is suitable as a training set point.
    It returns a copy of the atoms object with the single point
    calculator attached
    '''
    # Check if the input already has the desired format
    if atoms.calc.__class__.__name__ == 'SinglePointCalculator':
        # Return a copy of the atoms object
        calc = copy(atoms.calc)
        atoms0 = atoms.copy()

    else:
        # Check if the atoms object has energy and forces calculated for
        # this position
        # If not, compute them
        atoms.get_forces()

        # Initialize a SinglePointCalculator to store this results
        calc = SinglePointCalculator(atoms, **atoms.calc.results)

    atoms0 = atoms.copy()
    atoms0.calc = calc
    return atoms0





class GPCalculator(Calculator):

    implemented_properties = ['energy', 'forces', 'stress', 'uncertainty', 'free_energy']
    nolabel = True

    def __init__(self, model, calculate_uncertainty=True, calculate_stress=False):

        Calculator.__init__(self)

        self.model = model
        self.calculate_uncertainty = calculate_uncertainty
        self.calculate_stress=calculate_stress

    def calculate(self, atoms=None,
                  properties=['energy', 'forces',  'stress', 'uncertainty'],
                  system_changes=all_changes):
        '''
        Calculate the energy, forces and uncertainty on the energies for a
        given Atoms structure. Predicted energies can be obtained by
        *atoms.get_potential_energy()*, predicted forces using
        *atoms.get_forces()* and uncertainties using
        *atoms.calc.results['uncertainty'].
        '''
        # Atoms object.
        Calculator.calculate(self, atoms, properties, system_changes)


        x=self.model.new_fingerprint(atoms)
        
        f, V = self.model.gp.predict(x, get_variance=self.calculate_uncertainty)
      
        # Obtain energy and forces for the given geometry.
        energy = f[0]
        forces = -f[1:].reshape(-1, 3)

        # Get uncertainty for the given geometry.
        if self.calculate_uncertainty:
            if self.model.gp.use_forces:   
                uncertainty = V[0][0]
            else:
                uncertainty = V[0]
            if uncertainty < 0.0:
                uncertainty = 0.0
                warning = ('Imaginary uncertainty has been set to zero')
                warnings.warn(warning)
            uncertainty = np.sqrt(uncertainty)
        else:
            uncertainty = None

        # Results:
        self.results['energy'] = energy
        self.results['forces'] = forces
        self.results['uncertainty'] = uncertainty
        self.results['free_energy']=energy
        
        if self.calculate_stress:
            stress = self.model.gp.predict_stress(x)
            self.results['stress'] = stress
            
            
            
class PriorCalculator(Calculator):
    implemented_properties = ['energy', 'forces', 'stress', 'uncertainty', 'free_energy']
    nolabel = True

    def __init__(self, prior, calculate_stress=False):

        Calculator.__init__(self)

        self.prior=prior
        self.calculate_stress=calculate_stress
    
    def calculate(self, atoms=None,
                  properties=['energy', 'forces',  'stress', 'uncertainty'],
                  system_changes=all_changes):
        
        # Atoms object.
        Calculator.calculate(self, atoms, properties, system_changes)
        
        self.prior.calculate(atoms)
        
        self.results['energy'] = self.prior.results['energy']
        self.results['forces'] = self.prior.results['forces']
        self.results['free_energy']=self.prior.results['energy']
        
        if self.calculate_stress:
            self.results['stress'] = self.prior.results['stress']
        
        
