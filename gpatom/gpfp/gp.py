import numpy as np
from scipy.linalg import solve_triangular, cho_factor, cho_solve

from gpatom.gpfp.kernel import FPKernel, FPKernelParallel, FPKernelNoforces
from gpatom.gpfp.prior import ConstantPrior


class GaussianProcess:

    '''Gaussian Process Regression
    It is recomended to be used with other Priors and Kernels
    from ase.optimize.gpmin

    Parameters:

    prior: Prior class, as in ase.optimize.gpmin.prior
        Defaults to ConstantPrior with zero as constant

    kernel: Kernel function for the regression, as
       in ase.optimize.gpmin.kernel
        Defaults to the Squared Exponential kernel with derivatives '''

    def __init__(self, hp=None, prior=None, kernel=None, kerneltype='sqexp',
                 use_forces=True, parallelkernel=True):

        
        self.kernel_name=kerneltype  

        self.use_forces = use_forces
     
        
        if kernel is not None:
            self.kernel=kernel
            
        else:
        
            if self.use_forces:
                if parallelkernel:
                    self.kernel = FPKernelParallel(kerneltype)
                else:
                    self.kernel = FPKernel(kerneltype)
            else:
                self.kernel = FPKernelNoforces(kerneltype)
                

        default_params = dict(scale=self.kernel.kerneltype.scale,
                              weight=self.kernel.kerneltype.weight,
                              noise=1e-3,
                              noisefactor=0.5)
        default_params.update({'ratio': default_params['noise'] /
                               default_params['weight']})

        self.hp = default_params  

        if prior is None:
            prior = ConstantPrior(0.0)
        self.prior = prior



        self.set_hyperparams(hp, check_noise=True)

    def set_hyperparams(self, new_params, check_noise=False):

        self.hp.update(new_params)

        if 'ratio' not in self.hp:
            ratio = self.hp['noise'] / self.hp['weight']
            self.set_ratio(ratio)

        # Keep noise-weight ratio as constant:
        if check_noise: 
            # now the model can do optimization without ratio
            self.hp['noise'] = self.hp['ratio'] * self.hp['weight']   

        if 'prior' in self.hp.keys():
            self.prior.set_constant(self.hp['prior'])

        self.kernel.set_params(self.hp)

        return self.hp

    def train(self, X, Y):
        
        X = np.array(X)        
        Y = np.array(Y)
        
        K = self.kernel.kernel_matrix(X)      
        
 
        n_data=len(K)
        K[range(n_data),range(n_data)]=K[range(n_data),range(n_data)] + np.sum(np.diag(K))*(1/(1/(4.0*np.finfo(float).eps)-1)) 

        self.X = X
        self.Y = Y
        

        Natoms = int((len(self.Y.flatten()) / len(self.X) - 1) / 3)
        self.K = self.add_regularization(K, Ntrain=len(self.X), Natoms=Natoms)   
        
        self.model_vector = self.calculate_model_vector(K)                        
        
        return

    def add_regularization(self, matrix, Ntrain, Natoms):


        if self.use_forces:
            D = Natoms * 3  # number of derivatives
            regularization = np.array(Ntrain * ([self.hp['noise'] *
                                                 self.hp['noisefactor']] +
                                                D * [self.hp['noise']]))

        else:
            regularization = np.array(Ntrain * ([self.hp['noise']]))            

        matrix += np.diag(regularization**2)
        return matrix

    def calculate_model_vector(self, matrix):


        # factorize K-matrix:
        self.L, self.lower = cho_factor(matrix,                  
                                        lower=True,
                                        check_finite=True)
        
        self.prior_array = self.calculate_prior_array(self.X, get_forces=self.use_forces)

        model_vector = self.Y.flatten() - self.prior_array
        
        
        # Overwrite model vector so that it becomes C^-1 * (Y - prior):
        cho_solve((self.L, self.lower), model_vector,                  
                  overwrite_b=True, check_finite=True)
        
        return model_vector



    def calculate_prior_array(self, list_of_fingerprints, get_forces=True):
        
        if get_forces:
            return list(np.hstack([self.prior.potential(x)
                                   for x in list_of_fingerprints]))
        else:
            return list(np.hstack([self.prior.potential(x)[0]
                                   for x in list_of_fingerprints]))


    def predict(self, x, get_variance=False):
        '''
        If get_variance==False, then variance
        is returned as None
        '''
        
        k = self.kernel.kernel_vector(x, self.X)

        f = np.dot(k, self.model_vector)

        if not self.use_forces:
            dk_dxi = (np.array([self.kernel.kerneltype.kernel_gradient(x, x2)
                                for x2 in self.X]))

            forces = np.einsum('ijk,i->jk', dk_dxi,
                               self.model_vector).flatten()
            f = list([f]) + list(forces)
            
        prior_array = self.calculate_prior_array([x], get_forces=True)
        
        f = np.array(f) + np.array(prior_array)

        V = self.calculate_variance(get_variance, k, x)

        return f, V

    def calculate_variance(self, get_variance, k, x):
        V = None
        if get_variance:
            k_transpose = k.T.copy()
            k_transpose = solve_triangular(self.L,
                                           k_transpose,
                                           lower=True,
                                           check_finite=False)

            variance = self.kernel.kernel(x, x)
            covariance = np.tensordot(k_transpose, k_transpose,
                                      axes=(0, 0))

            V = variance - covariance
  
        return V




    def predict_stress(self, x):

        if hasattr(self.prior, 'calculator'):
            self.prior.calculator.calculate(x.atoms)
            priorstress = self.prior.calculator.results['stress']
            x.atoms.calc = None
        else:
            priorstress = [0., 0., 0., 0., 0., 0.]

        if not self.use_forces:
            
            dk_dc = np.array([self.kernel.kerneltype.dkernel_dc(x, x2)
                              for x2 in self.X])
            
        else:

            d_dc = [np.concatenate((self.kernel.kerneltype.dkernel_dc(x, x2)[np.newaxis, :, :],
                                    self.kernel.kerneltype.dkernelgradient_dc(x2, x).reshape(-1, 3, 3)),
                                   axis=0)
                    for x2 in self.X]
            dk_dc = np.array(d_dc)
            dk_dc = dk_dc.reshape(-1, 3, 3)

        strain = np.einsum('ijk,i->jk', dk_dc, self.model_vector)

        # XXX In future ASE, voigt form is obtained from
        # ase.stress methods
        strain = strain.flat[[0, 4, 8, 5, 2, 1]]

        stress = strain / x.atoms.get_volume()
        
        stress += priorstress

        return stress



    def get_properties(self, x):
        f, V = self.predict(x, get_variance=True)
                
        energy=f[0]
        forces=f[1:].reshape(-1, 3)
        
        if self.use_forces:
            uncertainty_squared = V[0, 0]
        else:
            uncertainty_squared = V[0]
        
        return energy, forces, uncertainty_squared

