import numpy as np
from gpatom.gpfp.calculator import copy_image

from gpatom.gpfp.fingerprint import FingerPrint
from gpatom.gpfp.gp import GaussianProcess
from gpatom.gpfp.database import Database

from gpatom.gpfp.kerneltypes import EuclideanDistance



class Model:
    '''
    GP model parameters
    -------------------
    
    gp :  gaussianprocess object
     
    fp : fingerprint creator object
        Fingerprint class to be used in the model

    params: dict
        Dictionary to include all the hyperparameters for the kernel and
        for the fingerprint

    use_forces: bool
        Whether to train the GP on forces

    '''
   

    def __init__(self, gp=GaussianProcess, fp=FingerPrint):  

        self.data=Database()
        self.gp=gp
        self.fp=fp
        

           
    
    def new_fingerprint(self, atoms, *args, **kwargs):    
        '''
        Compute a fingerprint of 'atoms' with given parameters.
        '''
        
        return self.fp.get(atoms=atoms, *args, **kwargs)



    def add_data_points(self, train_images):       
        '''
        Calculate fingerprints and add features and corresponding targets
        to the database of 'data' and retrain.
        '''
        
        if type(train_images)!=list:
            train_images=[train_images]
                
        for im in train_images:
            image = copy_image(im)
            fp = self.new_fingerprint(image)
            self.data.add(fp, image.get_potential_energy(apply_constraint=False),
                     image.get_forces(apply_constraint=False))        
            
        self.train_model()





    def train_model(self):       
        '''
        Train a Gaussian process with given data.

        Parameters
        ----------
        gp : GaussianProcess instance
        data : Database instance

        Return a trained GaussianProcess instance
        '''

        if self.gp.use_forces:
            targets = self.data.get_all_energyforces()
        else:
            targets = self.data.energylist
        features = self.data.get_all_fingerprints()
        
        self.gp.train(features, targets)

        


    def calculate(self, atoms, get_variance=True):  

        '''
        Calculate energy, forces and uncertainty for a set of atoms
        using the given fingerprint. If get_variance==False, variance is returned
        as None.
        '''
    
        fp=self.new_fingerprint(atoms)
        
        return self.gp.predict(fp, get_variance=get_variance)
    

    def get_predicted_properties(self, atoms):
        
        fp=self.new_fingerprint(atoms)
        
        energy, forces, uncertainty_squared = self.gp.get_properties(fp)
        
        uncertainty = np.sqrt(uncertainty_squared)
        
        return energy, forces, uncertainty


    def get_predicted_stress(self, atoms):
        
        fp=self.new_fingerprint(atoms)
            
        stress=self.gp.predict_stress(fp)
        return stress


    def calculate_distances(self, atoms):              
        '''
        Calculate the distances between the training set and
        given atoms.
        '''
        fp0 = self.new_fingerprint(atoms)

        distances = np.zeros(len(self.data), dtype=float)

        for i, x in enumerate(self.data.fingerprintlist):
            distances[i] = EuclideanDistance.distance(fp0, x)

        return distances


