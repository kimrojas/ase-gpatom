import numpy as np
from scipy.linalg import cho_solve
from ase.calculators.calculator import Calculator, all_changes
from ase.neighborlist import NeighborList
from ase.data import covalent_radii


# each prior must implement a method called potential taking a fingerprint, x 
# with an atoms object attached x.atoms

class ConstantPrior:
    '''Constant prior, with energy = constant and zero forces

    Parameters:

    constant: energy value for the constant.
    '''

    def __init__(self, constant, **kwargs):
        self.constant = constant

    def potential(self, x):
        d = len(x.atoms) * 3  # number of forces
        output = np.zeros(d + 1)
        output[0] = self.constant
        return output

    def set_constant(self, constant):
        self.constant = constant

    def get_contribution_vector(self, X):
        self.set_constant(1.)
        return np.hstack([self.potential(x) for x in X])

    def update(self, X, Y, L, use_forces):
        """Update the constant to maximize the marginal likelihood.

        The optimization problem:
        m = argmax [-1/2 (y-m).T K^-1(y-m)]

        can be turned into an algebraic problem
        m = [ u.T K^-1 y]/[u.T K^-1 u]

        where u is the constant prior with energy 1 (eV).

        parameters:
        ------------
        X: training data
        Y: training targets
        L: Cholesky factor of the kernel """


        if use_forces:
            u=self.get_contribution_vector(X)
        else:
            u=np.ones(len(X))
            
        w = cho_solve((L, True), u, check_finite=False)
  
        m = np.dot(w, np.array(Y).flatten()) / np.dot(w, u)   
        
        self.set_constant(m)

       


class CalculatorPrior:

    '''CalculatorPrior object, allows the user to
    use another calculator as prior function instead of the
    default constant.

    The form of prior is

    E_p(x) = m*u + E_c(x)

    where m is constant (energy), u is array with 1's for energy
    and 0 for force components, E_c(x) is the calculator
    potential.

    Parameters:

    calculator: one of ASE's calculators
    **kwargs: arguments passed to ASE parent calculator
    '''

    def __init__(self, calculator, constant=0, **kwargs):

        self.calculator = calculator
        self.constant = constant 


    def potential(self, x):

        atoms=self.get_atoms(x)
                
        output=self.setup_output(atoms)
    
        self.calculator.results.clear()
    
        atoms.calc = self.calculator

        output[0] = atoms.get_potential_energy() + self.constant

        output[1:] = -atoms.get_forces().reshape(-1)

        return output
    
    def set_constant(self, constant):
        self.constant = constant    
    
    
    def get_atoms(self,x):
        atoms=x.atoms.copy()
        return atoms


    def setup_output(self, atoms):
        d = len(atoms) * 3
        output = np.zeros(d + 1)
           
        return output
   

    def get_contribution_vector(self, X):
        self.set_constant(1.)
        return np.hstack([ConstantPrior.potential(self, x) for x in X])


    def update(self, X, Y, L, use_forces):
        """Update the constant to maximize the marginal likelihood.

        The optimization problem:
        m = argmax [-1/2 (y-m-Ec).T K^-1(y-m-Ec)]

        can be turned into an algebraic problem
        m = [ u.T K^-1 (y - Ec)]/[u.T K^-1 u]

        where u is the constant prior with energy 1 (eV),
        and Ec is the calculator prior.

        parameters:
        ------------
        X: training parameters
        Y: training targets
        L: Cholesky factor of the kernel """

        if use_forces:
            u=self.get_contribution_vector(X)
            pv=np.hstack([self.potential(x) for x in X])
        else:
            u=np.ones(len(X))
            pv=np.hstack([self.potential(x)[0] for x in X])
                    
        E_calcprior = (pv - u).flatten()
        
        # w = K\u
        w = cho_solve((L, True), u, check_finite=False)   

        # Set constant
        m = np.dot(w, (np.array(Y).flatten() - E_calcprior)) / np.dot(w, u)
        
        self.set_constant(m)

   
class RepulsivePotential(Calculator):
    ''' Repulsive potential of the form
    sum_ij (0.7 * (Ri + Rj) / rij)**12

    where Ri and Rj are the covalent radii of atoms
    '''

    implemented_properties = ['energy', 'forces']

    default_parameters = {'prefactor': 0.7,'rc': None, 'potential_type': 'LJ'}    
    
    nolabel = True



    def calculate(self, atoms=None,
                  properties=['energy', 'forces'],
                  system_changes=all_changes):
        
        
        Calculator.calculate(self, atoms, properties, system_changes)
        
        # get parameters
        rc, prefactor = self.get_parameters(atoms)

        neighbor_list = NeighborList(np.array(rc), self_interaction=False)   
        neighbor_list.update(atoms)
                
        energy, forces = self.get_energy_and_forces(atoms, rc, prefactor, neighbor_list)
        
        # set energy and forces
        self.results['energy'] = energy
        self.results['forces'] = forces  



    def get_parameters(self, atoms):
        
        prefactor = self.parameters.prefactor

        if self.parameters.rc is None:
            rc = [    covalent_radii[self.atoms[i].number]  for i in range(len(self.atoms))   ]
        else:
            
            if type(self.parameters.rc)==list:
                rc=self.parameters.rc
            else:
                rc=np.ones(len(atoms))*self.parameters.rc
        
        return rc, prefactor



    def get_energy_and_forces(self, atoms, rc, prefactor, neighbor_list):

        energy, forces = self.setup_energy_forces(atoms)   
        
        for a1 in range(len(atoms)):
            
            neighbors, d = self.get_distance_info(atoms, a1, neighbor_list)
            
            if len(neighbors)>0:     
                
                potetial, derivative = self.get_potential(a1, rc, prefactor, d, neighbors)   
                
                energy = self.update_energy(energy, potetial)
                
                forces = self.update_forces(forces, derivative, a1, neighbors) 
                
        return energy, forces
    
    
    
    def setup_energy_forces(self, atoms):
        energy = 0.0
        forces = np.zeros((len(atoms), 3)) 
        return energy, forces



    def get_distance_info(self, atoms, atom_idx, neighbor_list):
        
        positions = atoms.positions
            
        cell = atoms.cell
        
        neighbors, offsets = neighbor_list.get_neighbors(atom_idx)
            
        cells = np.dot(offsets, cell)
            
        d = positions[neighbors] + cells - positions[atom_idx]
        
        return neighbors, d  
        

    def get_potential_function_LJ(self, prefactor, x):
        
        # Potential function with zero value and zero derivative at x=1.
        if x<=1:
            value = prefactor*(1/x**12-1) - 12*prefactor*(1-x)
            deriv = 12*prefactor*(1-1/x**13)
        else:
            value = 0
            deriv = 0
        return value , deriv


    def get_potential_function_parabola(self, prefactor, x):
        
        # Potential function with zero value and zero derivative at x=1.
        
        if x<=1:
            value = prefactor*((x-1)**2)
            deriv = 2*prefactor*(x-1)
        else:
            value = 0
            deriv = 0
        return value , deriv



    def get_potential(self, atom_index, rc, prefactor, d, neighbors):        
        r2 = (d**2).sum(1)
        r=np.sqrt(r2)
        crs=rc[atom_index]+np.array( [   rc[n] for n in neighbors   ]  )
        x = r/crs   
    
        potential = np.zeros(len(x))
        derivative=np.zeros(len(x))
        
        
        if self.parameters.potential_type=='LJ':
            for i, xx in enumerate(x):
                potential[i], derivative[i] = self.get_potential_function_LJ(prefactor,xx)
        elif self.parameters.potential_type=='parabola':
            for i, xx in enumerate(x):
                potential[i], derivative[i] = self.get_potential_function_parabola(prefactor,xx)
        else:
            raise RuntimeError('potential_type={:s} not known.'
                               .format(self.parameters.potential_type))

        derivative *= 1/crs
            
        r_block=np.tile(r,(len(d[0,:]),1)).T
        
        polar_to_cart=d/r_block
        derivative=derivative[:,np.newaxis]*polar_to_cart
        return potential, -derivative

    
    def update_energy(self, energy, potential):
        energy += potential.sum()
        return energy
    
    
    def update_forces(self, forces, derivative, atom_index, neighbors):
    
        forces[atom_index] -= derivative.sum(axis=0)
    
        for a2, f2 in zip(neighbors, derivative):
            forces[a2] += f2
        return forces

    
class RepulsivePotentialWithStress(RepulsivePotential):
    implemented_properties = ['energy', 'forces', 'stress']

    default_parameters = {'prefactor': 0.7,'rc': None, 
                          'potential_type': 'LJ', 'extrapotential': None}    
    
    nolabel = True


    def calculate(self, atoms=None,
                  properties=['energy', 'forces', 'stress'],
                  system_changes=all_changes):
        
        Calculator.calculate(self, atoms, properties, system_changes)
        
        # get parameters
        rc, prefactor = self.get_parameters(atoms)

        neighbor_list = NeighborList(np.array(rc), self_interaction=False)     
        neighbor_list.update(atoms)

        energy, forces, stress = self.get_energy_and_derivatives(atoms, rc, prefactor, neighbor_list)
        
        # set energy and forces
        self.results['energy'] = energy   
        self.results['forces'] = forces    
        
        assert(atoms.cell.rank==3)
        self.results['stress'] = stress.flat[[0, 4, 8, 5, 2, 1]] / atoms.get_volume()

        if self.parameters.extrapotential is not None:
            
            self.results['energy']  +=  self.parameters.extrapotential.potential(atoms)
            self.results['forces']  +=  self.parameters.extrapotential.forces(atoms)
            self.results['stress']  +=  self.parameters.extrapotential.stress(atoms)


    def get_energy_and_derivatives(self, atoms, rc, prefactor, neighbor_list):

        energy, forces = self.setup_energy_forces(atoms)   
        
        stress = np.zeros((3, 3))

        for a1 in range(len(atoms)):
            
            neighbors, d = self.get_distance_info(atoms, a1, neighbor_list)
            
            if len(neighbors)>0:     
                
                potential, derivative = self.get_potential(a1, rc, prefactor, d, neighbors)   
                
                energy = self.update_energy(energy, potential)
                
                forces = self.update_forces(forces, derivative, a1, neighbors) 
                
                stress=self.update_stress(stress, derivative, d)
                #stress -= np.dot(derivative.T, d)
                
        return energy, forces, stress


    def update_stress(self, stress, derivative, d):
        stress -= np.dot(derivative.T, d)
        return stress

    
class TallStructurePunisher:

    '''
    NOTICE: DOESNT WORK PROPERLY
    '''    

    def __init__(self, zlow=0.0, zhigh=5.0, strength=2.0):
        self.zlow = zlow
        self.zhigh = zhigh
        self.strength = strength

    def potential(self, atoms):
        
        result = 0.0
        
        for i in range(len(atoms)):
            z = atoms.positions[i, 2]
            if z >= self.zhigh:
                result += self.strength * (z - self.zhigh)**2
            elif z <= self.zlow:
                result += self.strength * (z - self.zlow)**2
        return result
        
    def forces(self, atoms):
        result = np.zeros((len(atoms), 3))
        for i in range(len(atoms)):
            z = atoms.positions[i, 2]
            if z >= self.zhigh:
                result[i, 2] += self.strength * 2 * (z - self.zhigh)
            elif z <= self.zlow:
                result[i, 2] += self.strength * 2 * (z - self.zlow)
        return -result

    def stress(self, atoms):
       
        results=np.zeros(6)
        
        forces=self.forces(atoms)
    
    
        stress_zz=0
        stress_zy=0
        stress_zx=0
        
        for i in range(len(atoms)):
            
            
            force_z=forces[i,2]
            
            stress_zz += force_z * atoms.positions[i,2] / atoms.get_volume()
            stress_zy += 0.5*force_z * atoms.positions[i,1] / atoms.get_volume()
            stress_zx += 0.5*force_z * atoms.positions[i,0] / atoms.get_volume()
    
       
        results[2]=stress_zz
        results[3]=stress_zy
        results[4]=stress_zx
    
        return -results  
        
 
class SmallAreaPunisher:
    
    '''
    NOTICE: DOESNT WORK PROPERLY    
    '''
    def __init__(self, start=20.0, strength=10.0):
        '''
        Punish too small unit cells in directions x and y.
        Potential shape:
        
        E = strength * (start - A)**2  if A < start
        E = 0                          if A > start

        Here A is the area in xy plane.

        Parameters
        ----------
        start : float (Angstroms**2)
            Where the potential starts
        strength : float
            Strength k of the harmonic potential E = kA**2
        '''

        self.start = start
        self.strength = strength

    def potential(self, atoms):
        
        '''
        Compute potential.
        '''

        cell = atoms.cell
        A = self.area(cell)
        
     

        result = 0.0
        if A <= self.start:
            result += self.strength * (A - self.start)**2
        return result

    @staticmethod
    def area(cell):
        '''
        XXX In future ASE, area will be directly available for ase.Cell
        '''
        return abs(np.cross(cell[0, :2], cell[1, :2]))

    def forces(self, atoms):
        '''
        Potential does not affect forces on atoms.
        '''
        return np.zeros((len(atoms), 3))

    def stress(self, atoms):
        
       
        results=np.zeros(6)     
        cell = atoms.cell
        A = self.area(cell)
        
        if A>self.start:
            return results
            
        else:
            stress= 2*self.strength * (A - self.start)*A  / atoms.get_volume()
            results[0]=stress
            results[1]=stress
            results[3]=0.5*stress
            results[4]=0.5*stress
            return results



