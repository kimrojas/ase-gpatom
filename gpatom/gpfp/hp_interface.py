
import numpy as np
from scipy.spatial import distance_matrix

from gpatom.gpfp.hpfitter import HyperparameterFitter, GPPrefactorFitter, PriorFitter, ScaleSetter
from gpatom.gpfp.hpfitter_GSS import HPfit_GoldenSearch, Posterior_max
from gpatom.gpfp.gp import GaussianProcess
import copy



class GSSHpFitInterface:
    def __init__(self, E_train=False, bounds=None, param_bounds=None, 
                 scale_prior=None, noise_prior=None, fixed_noise=False, prior_constant_correction=True,
                 set_large_scale=False, large_scale_factor=None):
        
        self.E_train=E_train
        self.bounds=bounds
        self.param_bounds=param_bounds
        self.scale_prior=scale_prior
        self.noise_prior=noise_prior
        self.fixed_noise=fixed_noise
        self.prior_constant_correction=prior_constant_correction
        

        self.set_large_scale=set_large_scale
        self.large_scale_factor=large_scale_factor
        
        
        if self.bounds is not None:
            self.update_bounds=False
        else:
            self.update_bounds=True
            
            
        if self.param_bounds is None:
            if self.fixed_noise:
                self.param_bounds=[-3, 10]
            else:
                self.param_bounds=[0.1, 10]

    def set_scale_bounds(self, distances, dist_nn_avg):
            
        if self.update_bounds:
            
            self.bounds=np.array([[dist_nn_avg,np.max(distances)*100]])
            

    def get_energies(self, gp):
        
        if gp.use_forces:
            prior_const=gp.prior.constant
            P=gp.prior.get_contribution_vector(gp.X).reshape(1,-1)
            Y_eng=gp.Y.flatten()[P.flatten().astype(bool)].reshape(-1,1)
            gp.prior.set_constant(prior_const)
        else:
            Y_eng=gp.Y.flatten().reshape(-1,1)
        
        return Y_eng


    def construct_gp_energy(self, gp, Y_eng):
        eng_prior=copy.copy(gp.prior)
        eng_prior.use_forces=False
        gp_args_eng = dict(prior=eng_prior, hp=gp.hp, kerneltype=gp.kernel_name,  use_forces=False)
        gp_eng = GaussianProcess(**gp_args_eng)
        gp_eng.train(gp.X, Y_eng)
        return gp_eng
    
    
    
    def update_original_gp(self,gp, gp_original):
        gp_original.prior.constant=gp.prior.constant
        gp_original.set_hyperparams(gp.hp)
        return gp_original
        

    def initialize_parameters(self, gp):
        
        if self.set_large_scale:
            
            scale=ScaleSetter.get_large_scale(gp, self.large_scale_factor)
            
            gp.set_hyperparams({'scale': scale})
    
            gp.train(gp.X, gp.Y)     
    


    def get_prior_energies(self, gp):
        
        
        if gp.use_forces:
            prior_const=gp.prior.constant
            P=gp.prior.get_contribution_vector(gp.X).reshape(1,-1)
            # Y_prior must be done here. otherwise the prior potential will have a wrong value. and the optimization
            # will not be indifferent to the prior.constant when starting the optimization
            Y_prior = (np.hstack([gp.prior.potential(x) for x in gp.X]) - P).reshape(-1,1)  
            gp.prior.set_constant(prior_const)    # necessary for now because get_contribution vector sets prior to 1. 
                                                  # but I dont dare to change it as it is coupled to other methods 
                                                  
        else:
            P=np.ones(len(gp.X)).reshape(1,-1)
            Y_prior = (np.hstack([gp.prior.potential(x)[0] for x in gp.X]) - P).reshape(-1,1)
                                                  
        return Y_prior, P



                 
    def fit(self, gp, fit_scale=False, fit_weight=False, fit_prior=False):        

        Y_eng=self.get_energies(gp)
        
        if self.E_train:
            gp_original=gp
            gp=self.construct_gp_energy(gp, Y_eng) 
        

        if fit_scale:
            distances, dist_nn_avg = ScaleSetter.calculate_all_distances(gp)
            self.set_scale_bounds(distances, dist_nn_avg)
            ScaleSetter.update_scale_prior_distribution(self.scale_prior, distances)
          
        
        gp,ll=self.GSS_fit(gp, log=True, E_train=self.E_train, fit_scale=fit_scale, 
                           fit_weight=fit_weight, fit_prior=fit_prior, Y_eng=Y_eng)
        
        
        if self.E_train:
            gp=self.update_original_gp(gp, gp_original)
        
        gp.train(gp.X, gp.Y)
        return gp, ll




    def GSS_fit(self, gp, log=True, E_train=False, fit_scale=False, 
                fit_weight=False, fit_prior=False, Y_eng=None):
                
        '''
        if gp.fixed_noise:
            param_bounds=[-3, 10] # bounds set as 10^(2*param_bounds) 
            
            if not fit_weight: 
                #param_bounds=[gp.hp['weight']]
                param_bounds=[np.log10(gp.hp['weight'])]
            
        else: 
            param_bounds=[0.1, 10] # multipliers put on np.log10 of eigenvalues to eigendecomposition og K
            if not fit_weight: 
                  param_bounds=[gp.hp['ratio']]
        '''        
         
        param_bounds=self.param_bounds.copy()
        if not fit_weight: 
            if self.fixed_noise:
                param_bounds=[np.log10(gp.hp['weight'])]
            else:
                param_bounds=[gp.hp['ratio']]
                    
        
        func=Posterior_max(fun='lp_pn',log=log, optimize=True, fixed_noise=self.fixed_noise, 
                           noise=gp.hp['noise'], bounds=param_bounds, 
                           scale_prior=self.scale_prior, noise_prior=self.noise_prior,
                           correct_mu=self.prior_constant_correction)
        
            

        Y_prior, P = self.get_prior_energies(gp)
                                               
        if fit_prior: 
            mu=None
        else: 
            mu=gp.prior.constant
        
        
        Y=gp.Y.reshape(-1,1) 
        args=[gp.kernel, gp.X, Y, Y_prior, Y_eng, mu, P]
        assert(np.shape(P)==(1,np.size(P)))
        assert(np.shape(Y)==(np.size(Y),1))
        assert(np.shape(Y_prior)==(np.size(Y_prior),1))
            
        
        if not fit_scale:
            x0=gp.hp['scale']
            if log:
                x0=np.log(x0) 
            func.fun(x0,*args)
        
        else:
            
            
            if log:
                bounds=np.log(self.bounds)      
        
        
            HPfit_GoldenSearch.line_search_scale(func.fun,np.array([gp.hp['scale']]),
                                                 bounds,maxiter=5000,log=log,args=args,
                                                 spacing=1,optimize=True,tol=1e-3)
       
        
            
        
        gp.prior.set_constant(func.sol['hp']['mu'])
        
        gp.set_hyperparams({'scale': float(func.sol['hp']['scale']), 'weight': float(func.sol['hp']['weight']), 
                            'noise': float(func.sol['hp']['noise']),  'ratio': float(func.sol['hp']['ratio'])})
        

        return gp, func.sol['fun']




















class GSSHpFitInterfaceSmall(GSSHpFitInterface):

    def __init__(self, E_train=False, bounds=None, 
                 scale_prior=None, noise_prior=None, prior_constant_correction=True,
                 set_large_scale=False, large_scale_factor=None):
        
        self.E_train=E_train
        self.bounds=bounds
        self.scale_prior=scale_prior
        self.noise_prior=noise_prior
        
        self.prior_constant_correction=prior_constant_correction
    
        self.set_large_scale=set_large_scale
        self.large_scale_factor=large_scale_factor
        
        if self.bounds is not None:
            self.update_bounds=False
        else:
            self.update_bounds=True
            
            
            
    def fit(self, gp, fit_scale=False, fit_weight=False, fit_prior=False):        

        Y_eng=self.get_energies(gp)
        
        if self.E_train:
            gp_original=gp
            gp=self.construct_gp_energy(gp, Y_eng) 
        

        if fit_scale:
            distances, dist_nn_avg = ScaleSetter.calculate_all_distances(gp)
            self.set_scale_bounds(distances, dist_nn_avg)
            ScaleSetter.update_scale_prior_distribution(self.scale_prior, distances)
          
        
        gp,ll=self.GSS_fit(gp, fit_scale=fit_scale, fit_prior=True, Y_eng=Y_eng)
        
        
        if self.E_train:
            gp=self.update_original_gp(gp, gp_original)
        
        gp.train(gp.X, gp.Y)
        return gp, ll            
            
    
    

    def GSS_fit(self, gp, fit_scale=False, 
                fit_prior=False, Y_eng=None):
                
        
        func=Posterior_max(fun='lp_pn',log=True, optimize=True, fixed_noise=False, 
                           noise=gp.hp['noise'], bounds=[gp.hp['ratio']], 
                           scale_prior=self.scale_prior, noise_prior=self.noise_prior,
                           correct_mu=self.prior_constant_correction)
        
        Y_prior, P = self.get_prior_energies(gp)
       
        if fit_prior: 
            mu=None
        else: 
            mu=gp.prior.constant
        
        
        Y=gp.Y.reshape(-1,1) 
        args=[gp.kernel, gp.X, Y, Y_prior, Y_eng, mu, P]
        assert(np.shape(P)==(1,np.size(P)))
        assert(np.shape(Y)==(np.size(Y),1))
        assert(np.shape(Y_prior)==(np.size(Y_prior),1))
            
        
        if not fit_scale:
            x0=gp.hp['scale']
            x0=np.log(x0) 
            func.fun(x0,*args)
        else:
            bounds=np.log(self.bounds)      
        
        
            HPfit_GoldenSearch.line_search_scale(func.fun,np.array([gp.hp['scale']]),
                                                 bounds,maxiter=5000,log=True,args=args,
                                                 spacing=1,optimize=True,tol=1e-3)
       
        
        gp.prior.set_constant(func.sol['hp']['mu'])
        
        gp.set_hyperparams({'scale': float(func.sol['hp']['scale']), 'weight': float(func.sol['hp']['weight']), 
                            'noise': float(func.sol['hp']['noise']),  'ratio': float(func.sol['hp']['ratio'])})
        

        return gp, func.sol['fun']




















