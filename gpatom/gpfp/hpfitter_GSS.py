#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  9 17:16:05 2022

@author: casper
"""

import numpy as np
from scipy.linalg import eigh
from scipy.optimize import OptimizeResult
import copy
#from scipy.special import expit
#import warnings 
        
class HPfit_GoldenSearch:  

    @staticmethod
    def line_search_scale(fun,x0,bounds,maxiter=5000,log=True,args=[],spacing=0.1,optimize=True,tol=1e-3,**kwargs):
        " Do a scaled line search in 1D (made for the 1D length scale) "
        dim=len(x0)

        # Stationary arguments
        #sol={'success':False,'x':x0,'nfev':1,'fun':fun(x0,*args)}
        sol={'success':False,'x':x0,'nfev':1,'fun':np.inf}
        # Initial and final line path
        theta_i=np.array([bounds[d][0] for d in range(dim)])
        theta_f=np.array([bounds[d][-1] for d in range(dim)])
        direc=theta_f-theta_i
        direc_size=np.linalg.norm(direc)
        direc=direc/direc_size
        scalings=np.arange(0,direc_size,spacing)
        #if log:
        #    scalings=np.append(scalings,direc_size+np.log(1e8))
        #else:
        #    scalings=np.append(scalings,direc_size*1e8)
        if len(scalings)>maxiter:
            scalings=scalings[::int(len(scalings)/maxiter)]
        scalings=scalings[::-1]
        # Calculate all points on line
        
        f_list=np.array([fun(theta_i+s*direc,*args) for s in scalings])
        sol['nfev']+=len(scalings)  
                
        if not optimize:
            # Find the optimal value
            i_min=np.argmin(f_list)
            if f_list[i_min]<=sol['fun']:
                sol['fun'],sol['x'],sol['success']=f_list[i_min],theta_i+scalings[i_min]*direc,True
        else:
            # Optimize all possible candidates
            sol_o=HPfit_GoldenSearch.run_golden(fun,theta_i,direc,scalings,f_list,maxiter,tol=tol,args=args)
            sol=copy.deepcopy(sol_o)
            sol['nfev']+=len(scalings)
        return OptimizeResult(**sol)
    
    
    @staticmethod
    def run_golden(fun,theta0,direc,scalings,f_list,maxiter,tol=1e-3,args=()):
        " Perform a golden section search for all candidates to optima as part of line_search_scale "
        # Sign changes
        fs=np.sign(f_list[1:]-f_list[:-1])
        # check from zero and whenever it goes from a lower to a higher value.
        i_mins=np.append(np.append([0],np.where((fs[1:]-fs[:-1])<0)[0]+1),[len(f_list)])
        i_minimas=[i_mins[i-1]+np.argmin(f_list[i_mins[i-1]:i_mins[i]]) for i in range(1,len(i_mins))]
        
        # Do multiple golden section search if necessary
        sol_b={'fun':np.inf,'x':None,'nfev':0}
        niter=0
        for i_min in i_minimas:
            x1=i_min-1 if i_min-1>=0 else i_min
            f1=f_list[x1]
            x1=scalings[x1]
            x4=i_min+1 if i_min+1<=len(scalings)-1 else i_min
            f4=f_list[x4]
            x4=scalings[x4]
            sol_o=HPfit_GoldenSearch.golden_search(fun,[x1,x4],fbrack=[f1,f4],maxiter=maxiter,tol=tol,args=args,vec0=theta0,direc=direc)
            niter+=sol_o['nfev']
            if sol_o['fun']<=sol_b['fun']:
                sol_b=copy.deepcopy(sol_o)
        sol_b['nfev']=niter
        return sol_b
    
    
    @staticmethod
    def golden_search(fun,brack,maxiter=200,tol=0.001,args=(),fbrack=None,vec0=np.array([0]),direc=np.array([1]),**kwargs):
        'Perform a golden section search for integers'
        #Golden ratio
        r=(np.sqrt(5)-1)/2
        c=1-r
        sol={'success':False,'nfev':0}
        # Bracket
        x1,x4=brack
        if fbrack is None:
            f1,f4=fun(vec0+direc*x1,*args),fun(vec0+direc*x4,*args)
            sol['nfev']+=2
        else:
            f1,f4=fbrack
        # Make points in bracket
        x2,x3=r*x1+c*x4,c*x1+r*x4
        if maxiter<1:
            i_min=np.argmin([f4,f1])
            sol['fun']=[f4,f1][i_min]
            sol['x']=vec0+direc*([x4,x1][i_min])
            return OptimizeResult(**sol)
        if abs(x3-x2)==0:
            f2=fun(vec0+direc*x2,*args)
            i_min=np.argmin([f4,f2,f1])
            sol['fun'],sol['success']=[f4,f2,f1][i_min],True
            sol['x']=vec0+direc*([x4,x2,x1][i_min])
            sol['nfev']+=1
            return OptimizeResult(**sol)
        f2=fun(vec0+direc*x2,*args)
        f3=fun(vec0+direc*x3,*args)
        sol['nfev']+=2
        # Perform the line search
        while sol['nfev']<maxiter:
            if f1==f2==f3==f4:
                break
            i_min=np.argmin([f1,f2,f3,f4])
            if i_min<2:
                x4=x3 ; f4=f3
                x3=x2 ; f3=f2
                x2=r*x3+c*x1
                f2=fun(vec0+direc*x2,*args)
            else:
                x1=x2 ; f1=f2
                x2=x3 ; f2=f3
                x3=r*x2+c*x4
                f3=fun(vec0+direc*x3,*args)
            sol['nfev']+=1
            if tol*(x2+x3)>abs(x4-x1):
                sol['success']=True
                break
        i_min=np.argmin([f4,f3,f2,f1])
        sol['fun']=[f4,f3,f2,f1][i_min]
        sol['x']=vec0+direc*([x4,x3,x2,x1][i_min])
        return OptimizeResult(**sol)


class Posterior_max:
    
    def __init__(self,fun='lp_pn',log=True,optimize=True, fixed_noise=False, noise=None, 
                 bounds=[None, None], scale_prior=None, noise_prior=None, correct_mu=True):
        self.log=log
        self.fun_name=fun
        self.fun_choice(self.fun_name,log)
        self.sol=dict(fun=np.inf,hp={},nfev=0)
        self.optimize=optimize
        self.fixed_noise=fixed_noise
        self.noise=noise
        self.bounds=np.array(bounds)
        self.correct_mu=correct_mu
        
        self.scale_prior=scale_prior
        self.noise_prior=noise_prior
        
        if self.scale_prior is None:
            self.scale_prior=NonePrior
        if self.noise_prior is None:
            self.noise_prior=NonePrior   
        
        
    def fun_choice(self,fun,log):
        " Get the object function specified by fun "
        # What object function to use
        fun_f={'lp_pn':self.lp_pn}
        fun_f=fun_f[fun.lower()]
        # A wrapper function is used if hyperparameters are in log space
        if log:
            self.func=fun_f
            fun_f=self.log_wrapper
        self.fun=fun_f
        return self
        
    def update_sol(self,fun,hp):
        self.sol['nfev']+=1
        if self.sol['fun']>=fun:
            self.sol['fun'],self.sol['hp']=fun,hp
        return self

#    def log_wrapper(self,scale,kernel,FPs, Y, mu=None,P=None):        
    def log_wrapper(self,scale,kernel,FPs, Y, Y_prior, Y_eng, mu=None,P=None):
        " A wrapper that calculate the object function within the log-space of the hyperparameters "
                
        scale=np.exp(scale)
                
        values=self.func(scale,kernel,FPs,Y, Y_prior, Y_eng ,mu=mu,P=P)
        return values

#    def lp_pn(self,scale,kernel,FPs,Y, mu=None,P=None):
    def lp_pn(self,scale,kernel,FPs,Y, Y_prior, Y_eng, mu=None,P=None):

        
        hp=dict(weight=1.0,scale=scale)
        kernel.set_params(hp)
        
        
        K=kernel.kernel_matrix(FPs)
        
        
        n_data=len(K)
        K[range(n_data),range(n_data)]=K[range(n_data),range(n_data)] + np.sum(np.diag(K))*(1/(1/(10.0*np.finfo(float).eps)-1))   
              
        D,U=eigh(K)
        Ut=U.T

        D=self.correct_negative_D(D)

        
        R,Uy=None,None
        if mu is None:
            R=np.matmul(P,U)
            Uy=np.matmul(Ut,Y-Y_prior)   # CL I added Y_prior
            
        
        param,ll=self.maximize_param(Ut,Y,Y_eng,D,n_data,scale,mu=mu,P=P,R=R,Uy=Uy)
        
        # Update hyperparameters
        D_n=self.make_D_n(D, param)
        
        if mu is None:
            RDn=R*D_n
            mu=self.optimize_mu(RDn, Uy, R, Y_eng)

        
        if self.fixed_noise:
            hp['mu'],  hp['weight']  =  mu,np.sqrt(param)
            hp['ratio'], hp['noise'] = self.noise/hp['weight'], self.noise
        else:
            Y_new=Y-mu*P.T
            Uy_new=np.matmul(Ut,Y_new)
            prefactor2=np.matmul(Uy_new.T*D_n,Uy_new).item(0)/n_data
            
            hp['mu'], hp['weight'], hp['ratio']  =  mu, np.sqrt(prefactor2), param
            hp['noise'] = hp['ratio']*hp['weight']
            
        self.update_sol(ll,hp)
                
        return ll
        
    def correct_negative_D(self,D):
        if any(D<0):
            D-=(D[0]-100*np.finfo(float).eps)
        return D
    
    
    
    def make_D_n(self, D, param):
        if self.fixed_noise:
            D_n=1/(param*D + self.noise**2 )
        else:
            D_n=1/(D + param**2 )
        return D_n

       
    def maximize_param(self,Ut,Y, Y_eng, D,n_data, scale, mu=None,P=None,R=None,Uy=None):
                
        if self.fixed_noise:
            if len(self.bounds)==1:
                #params=[10**(np.log10(self.bounds[0]**2))]
                params=[10**(2*self.bounds[0])]
            else:
                #params=10**np.linspace(np.log10(self.bounds[0]**2),np.log10(self.bounds[1]**2),100)
                params=10**np.linspace(2*self.bounds[0],2*self.bounds[1],100)                
        else:
            if len(self.bounds)==1:
                #print('hello')
                params=[10**np.log10(self.bounds[0])]
            else:
                params=10**np.linspace(np.log10( np.min(D) * self.bounds[0]) , np.log10( np.max(D) * self.bounds[1]),100)

        args=(Ut,Y, Y_eng, D,n_data, scale, mu,P,R,Uy)
        ll_l=[self.get_eig_ll(n,*args) for n in params]
        i_min=np.argmin(ll_l)
        param=params[i_min]
                
        ll=ll_l[i_min]
        if self.optimize:
            x1=params[i_min-1] if i_min-1>=0 else param
            f1=ll_l[i_min-1] if i_min-1>=0 else ll
            x4=params[i_min+1] if i_min+1<=len(params)-1 else param
            f4=ll_l[i_min+1] if i_min+1<=len(params)-1 else ll

            s=HPfit_GoldenSearch.golden_search(self.get_eig_ll,[x1,x4],fbrack=[f1,f4],maxiter=400,tol=1e-5,args=args)
            if s['fun']<=ll:
                param,ll=s['x'],s['fun']
        return param,ll
        
    def get_eig_ll(self,param,Ut,Y, Y_eng, D,n_data, scale, mu=None,P=None,R=None,Uy=None):
        " Calculate log-likelihood from Eigendecomposition "
        
        D_n=self.make_D_n(D, param)
        
        if mu is None:
            RDn=R*D_n
            mu=self.optimize_mu(RDn, Uy, R, Y_eng)
            
        Y_new=Y-mu*P.T
        Uy_new=np.matmul(Ut,Y_new)
                
        UytCinvUy=np.matmul(Uy_new.T*D_n,Uy_new).item(0)
        
        if self.fixed_noise:       
            nlp= 0.5 * UytCinvUy - 0.5 * np.sum(np.log(D_n)) + 0.5*n_data*np.log(2*np.pi)
            nlp-=self.scale_prior.get(scale)
        else:
            prefactor2=UytCinvUy/n_data
            nlp=0.5*n_data*(np.log(prefactor2)+1+np.log(2*np.pi))-0.5*np.sum(np.log(D_n))  
            
            nlp-=(self.scale_prior.get(scale) + self.noise_prior.get(param*np.sqrt(prefactor2)))
            
        return nlp



    def optimize_mu(self, RDn, Uy, R, Y_eng):
        
        mu=(np.matmul(RDn,Uy)/np.matmul(RDn,R.T))
        
        if mu<min(Y_eng) and self.correct_mu:
            mu=(min(Y_eng)+np.mean(Y_eng))/2
            
        return mu.item(0)
    
    

class NonePrior():
    
    @staticmethod
    def get(x):
        return 0
