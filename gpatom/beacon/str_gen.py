from gpatom.gpfp.prior import RepulsivePotential, RepulsivePotentialWithStress
from ase.constraints import FixAtoms
from ase.optimize import BFGS
from ase.io import read

from scipy.spatial import distance_matrix
import numpy as np
import os

from scipy.optimize import minimize, Bounds

from ase.data import covalent_radii

from ase import Atoms
from ase.io import read, write, Trajectory

from ase.constraints import UnitCellFilter

from ase.calculators.emt import EMT


from ase.io import write

class RandomStructure:

    def __init__(self, atoms, rng=np.random):
        self.atoms = atoms   
        self.rng = rng
        self.constr = ConstraintHandler(atoms)
        
    def sph2cart(self, r, theta, phi):
        x = r * np.sin(theta) * np.cos(phi)
        y = r * np.sin(theta) * np.sin(phi)
        z = r * np.cos(theta)
        return x, y, z

    def attatch_relaxer(self, relaxer):
        self.relaxer=relaxer



class AtomsRelaxer:
    def __init__(self, calculator, fmax=0.05, steps=200):
        self.calculator=calculator
        self.fmax=fmax
        self.steps=steps
        
    def run(self, atoms): 

        atoms.calc = self.calculator

        opt = BFGS(atoms, maxstep=0.1, logfile=os.devnull)
        opt.run(fmax=self.fmax, steps=self.steps)
            
        return atoms


class AtomsAndStressRelaxer:
    def __init__(self, calculator, fixed_cell_params=None, 
                 fmax=0.05, steps=200):
        
        if fixed_cell_params is None:
            fixed_cell_params=[False]*6
        
        self.calculator=calculator
        self.mask=[not elem for elem in fixed_cell_params]
        self.fmax=fmax
        self.steps=steps        
        
    def run(self, atoms):

        atoms.calc = self.calculator   

        ucfilter = UnitCellFilter(atoms, mask=self.mask)
        
        opt = BFGS(ucfilter, maxstep=0.1, logfile=os.devnull)
      
        opt.run(fmax=self.fmax, steps=self.steps)
        return atoms


class AtomsInsideBoxRelaxer:
    
    def __init__(self, calculator, box, covrad_inside=[True, True, True],
                 constr=None, fmax=0.005, derivative_modulation=1, steps=200):
        self.box=box
        self.covrad_inside=covrad_inside
        self.calculator=calculator
        self.fmax=fmax*derivative_modulation
        self.steps=steps
        self.derivative_modulation=derivative_modulation
        self.ndof=3
        self.constr=constr
        
    def run(self, atoms):
        
        lb, ub = self.get_bounds(atoms, self.box, self.covrad_inside)        
                     
        params = atoms.positions.flatten() 
            
        result = minimize(self.prior_vals,   
                          params,
                          args=(atoms, self.calculator),
                          method='SLSQP',
                          bounds=Bounds(lb, ub, keep_feasible=False),
                          jac=True,
                          options={'ftol': self.fmax, 'maxiter': self.steps})


        opt_array = result['x'].reshape(len(atoms), self.ndof)

        opt_coords = opt_array[:, :self.ndof].reshape(-1, self.ndof)
                
        atoms.positions=opt_coords 

        return atoms
        

    def get_bounds(self, atoms, box, covrad_inside):
        lb , ub = self.setup_limits(atoms, box, covrad_inside)
        lb , ub = self.constrain_atoms(atoms, lb, ub)
        
        return lb, ub
    
    
    def setup_limits(self, atoms, box, covrad_inside):
        
        covrad_inside=np.array(covrad_inside,dtype=int)
            
        atomic_radii=[  covalent_radii[atoms[i].number]*covrad_inside  for i in range(len(atoms))   ]
        
        
        n_atoms=len(atoms)
        
        lb = [box[i][0] for i in range(self.ndof)]*n_atoms
        ub = [box[i][1] for i in range(self.ndof)]*n_atoms
        
        lb=np.array(lb) + np.array(atomic_radii).flatten()
        ub=np.array(ub) - np.array(atomic_radii).flatten()    
        
        return lb, ub
    
    
    def constrain_atoms(self, atoms, lb, ub):
            
        if len(self.constr.cindex)!=0:
            for atom in atoms:
                if atom.index in self.constr.cindex:
                    
                    idx=atom.index*self.ndof 
                    for i in range(self.ndof):
                    
                        lb[idx+i]=np.array( atom.position[i] )
                        ub[idx+i]=lb[idx+i]                    

        return lb, ub        
  

    def prior_vals(self, params, *args):
        
        atoms=args[0]
        prior=args[1]
       
        atoms=self.prepare_atoms_for_prior(atoms, params)
        
        prior.calculate(atoms)
        
        energy = prior.results['energy']
        derivatives = prior.results['forces']

        return  (energy*self.derivative_modulation, 
                 -np.array(derivatives.flatten())*self.derivative_modulation)
                    
    
    def prepare_atoms_for_prior(self, atoms, params):
        
        coords = params.reshape(len(atoms), self.ndof)[:, :self.ndof]
        atoms = atoms.copy()
        atoms.positions = coords
        
        return atoms
    
          

class Remake(RandomStructure):
    
    def __init__(self, atoms, rng=np.random):
        RandomStructure.__init__(self, atoms)
                
    def get(self):
        newatoms=self.atoms.copy()
        return newatoms


class RandomBranch(RandomStructure):

    def __init__(self, atoms, llim=1.6, ulim=2.0, **kwargs):

        RandomStructure.__init__(self, atoms, **kwargs)

        assert (llim <= ulim), ('Upper limit (ulim) '
                                'should be larger than '
                                'lower limit (llim).')

        self.llim = llim
        self.ulim = ulim
        self.relaxer=None

    def get(self):
        newatoms = self.atoms.copy()
        cell = newatoms.cell.lengths()
    
        newpos=np.zeros((len(newatoms),3))
        
        idx_occupied=[]
        
        i=len(self.constr.cindex)
        
        
        if i==0:
            newpos[0,:]=newatoms.cell.lengths() / 2
            idx_occupied.append(0)
        else:
            for atom in newatoms:
                if atom.index in self.constr.cindex:
                    newpos[atom.index,:]=atom.position  
                    idx_occupied.append(atom.index)

        for atom in newatoms:
            if atom.index not in idx_occupied:
                new_position=self.connect_new_atom(cell, newpos[idx_occupied])
                newpos[atom.index,:] = new_position
                idx_occupied.append(atom.index)
        
        newatoms.positions=newpos
        
        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)
    
        newatoms.calc = None
        
        return newatoms
    
        
    def connect_new_atom(self, cell, positions):
        
        while True:
            # Sample position for new atom:
            r, theta, phi = self.rng.random([3])
            r = self.llim + r * (self.ulim - self.llim)
            theta *= np.pi
            phi *= 2 * np.pi
            pos = self.sph2cart(r, theta, phi)

            # Add atom next to another one
            neighborindex = self.rng.randint(len(positions)) 
 
            new_position = positions[neighborindex] + pos
            new_position=new_position.reshape((1,3))
          
            dm = distance_matrix(new_position, positions)    

            distances_ok = dm.min() > self.llim
            
            cell_ok = (0.0 < new_position).all() and (new_position < cell).all()

            if distances_ok and cell_ok:
                return new_position


class RandomCell(RandomStructure):
        
    def __init__(self, atoms, fixed_cell_params=None, **kwargs):        
        
        
        '''
        fixed_cell_params : list of 6 booleans
            Whether to keep the cell parameters fixed, respectively in Voigt form
            If None, all cell parameters are given by random (if randomcell==True)
        '''

        RandomStructure.__init__(self, atoms, **kwargs)
     
        if  fixed_cell_params is None:
            fixed_cell_params = [False]*6 
            
        self.fixed_cell_3x3 = self.get_fixed_cell_params(fixed_cell_params)
        
        self.relaxer=None

    def get(self):

        cell = self.get_random_cellparams(self.atoms, self.rng,
                                          fixed_cell_3x3=self.fixed_cell_3x3)

        newatoms = self.atoms.copy()
        newatoms.cell = cell

        newpos=self.get_new_positions(cell)

        newatoms.positions = newpos

        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)
          
        newatoms.wrap()
        newatoms.calc = None
        return newatoms
    
 
    def get_new_positions(self, cell):
        newpos = []

        for atom in self.atoms:

            if atom.index in self.constr.cindex:
                newpos.append(self.atoms.positions[atom.index])
                continue

            r0 = cell[0] * self.rng.random()
            r1 = cell[1] * self.rng.random()
            r2 = cell[2] * self.rng.random()

            pos = r0 + r1 + r2

            newpos.append(pos)

        newpos = np.array(newpos)
        
        return newpos

    @staticmethod
    def get_fixed_cell_params(params):

        if params is None:
            params = np.ones(6, dtype=bool)

        # convert voigt form to 3 x 3 matrix:
        fixed_3x3 = np.zeros((3, 3), dtype=bool)
        c1, c2 = np.unravel_index([0, 4, 8, 5, 2, 1], (3, 3))  # de-voigt
        for i in range(len(c1)):
            fixed_3x3[c1[i], c2[i]] = params[i]
        fixed_3x3[np.tril_indices(3)] = fixed_3x3.T[np.tril_indices(3)]
        
        return fixed_3x3

    @staticmethod
    def get_random_cellparams(atoms, rng, scale=0.3, fixed_cell_3x3=None):

        # Approximately preserve volume of the unit cell
        volume = atoms.cell.volume

        # add random contributions:
        limit = scale * volume**(1/3.)
        addition = rng.uniform(-limit, limit, size=(3, 3))

        if fixed_cell_3x3 is not None:
            addition[fixed_cell_3x3] = 0.0

        newcell = atoms.cell.copy() + addition

        return newcell
    


class RandomBox(RandomStructure):

    def __init__(self, atoms, box=[(0., 1.), (0., 1.), (0., 1.)],
                 covrad_inside=[True,True,True], **kwargs):

        RandomStructure.__init__(self, atoms, **kwargs)

        self.box = box
        self.rc=np.array( [    covalent_radii[atoms[i].number]  for i in range(len(atoms))   ]  )
        self.covrad_inside=covrad_inside
        self.relaxer=None
        
    def get_coord(self, rc, idx):        

        delta_box= (self.box[idx][1]-rc[idx]) - (self.box[idx][0]+rc[idx])
        coord=delta_box*self.rng.random() + (self.box[idx][0]+rc[idx])
        return coord


    def get_rc(self, atom):
        rc=np.zeros(3)
        rc[self.covrad_inside]=covalent_radii[atom.number]
        return rc


    def get_xyz(self, rc):
        x=self.get_coord(rc, 0)
        y=self.get_coord(rc, 1)
        z=self.get_coord(rc, 2)
        return np.array([x, y, z])   


    def get_positions(self, atoms):
        
        for atom in atoms:
            if atom.index not in self.constr.cindex:
                rc=self.get_rc(atom)
                atom.position=self.get_xyz(rc)

        return atoms


    def get(self):
        
        newatoms=self.atoms.copy()

        newatoms=self.get_positions(newatoms)

        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)
            
        newatoms.wrap()
        newatoms.calc = None

        return newatoms



class BoxConstructor:
    
    @staticmethod
    def get_box(atoms, volume_fraction=0.2, free_space=None, cell=None):
        box_length=BoxConstructor.get_box_length(atoms, volume_fraction)
        
        if cell is None:
            cell=BoxConstructor.get_unitcell(box_length, free_space)
        else:
            cell=atoms.cell
    
        box = BoxConstructor.construct_box(cell, box_length)
        
        return box, cell
        
    @staticmethod
    def get_box_length(atoms, volume_fraction):
        rc =np.array( [    covalent_radii[atoms[i].number]  for i in range(len(atoms))   ]  )
        
        V_atoms=sum(  (4/3)*np.pi*rc**3  )
        
        box_length=(V_atoms/volume_fraction)**(1/3)
        
        return box_length
        
    @staticmethod
    def construct_box(cell, box_length): 

        box_start=cell[0]/2-box_length/2
        box_end=cell[0]/2+box_length/2
                
        box=[(box_start, box_end)]*3
    
        return box

    @staticmethod
    def get_unitcell(box_length, free_space):
            
        if free_space is None:
            cell_L=box_length
        else:
            cell_L=box_length+2*free_space
       
        cell=[cell_L, cell_L, cell_L]
        
        return cell
    


class Random2D(RandomStructure):
    '''
    Generator for a 2D-system with possibility of generating random
    cell parameters. The z-coordinate of the cell is kept fixed. The
    atoms are placed randomly in xy-plane and randomly in z-coordinates
    between 'minz' and 'maxz'.
    '''

    def __init__(self, atoms, minz, maxz, fixed_cell_params=None, **kwargs):

        RandomStructure.__init__(self, atoms, **kwargs)

        self.minz, self.maxz = minz, maxz
      #  self.do_minimize_repulsive = do_minimize_repulsive
        self.relaxer=None
        
        if fixed_cell_params is None:
            fixed_cell_params=[False, False, True, True, True, False]
 
        
        self.fixed_cell_3x3 = RandomCell.get_fixed_cell_params(fixed_cell_params)

    def get_xyz(self, cell):
        x = cell[0] * self.rng.random()
        y = cell[1] * self.rng.random()

        # z-coordinate is a vector along the cell[2] axis
        # with random length between minz and maxz:
        zlength = (self.maxz - self.minz) * self.rng.random() + self.minz
        z = zlength * cell[2] / np.linalg.norm(cell[2])

        return x + y + z

    def get(self):
        '''
        Mostly duplication of RandomBox.get()
        '''
        newatoms = self.atoms.copy()
        if not self.fixed_cell_3x3.all():
            cell = RandomCell.get_random_cellparams(self.atoms, self.rng,
                                                    fixed_cell_3x3=self.fixed_cell_3x3)
        else:
            cell = self.atoms.cell

        newatoms.cell = cell

        newpos = []

        for atom in self.atoms:

            if atom.index in self.constr.cindex:
                pos = atom.position
            else:
                pos = self.get_xyz(cell)

            newpos.append(pos)

        newpos = np.array(newpos)

        newatoms.positions = newpos

        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)

        newatoms.wrap()
        newatoms.calc = None

        return newatoms


class Random2DRanges(RandomStructure):
    """
    Generator for a 2D-system with possibility of generating random
    cell parameters for X,Y as well as their angle.
    This generator will ignore the input unit cell, except for the z-direction
    The z-coordinate of the cell is kept fixed.
    The atoms are placed randomly in the generated xy-plane.. 
    and randomly in z-coordinates between 'minz' and 'maxz'. 
    area_range area limits in area, defaults [20,30]
    xy_ratio_rannges are y_to_x_ratio limits, default [1, 1.5]
    anlge_ranges are angle limits in degrees, default [90,135] degrees, so just 90deg.
    """
    def __init__(self, atoms, minz, maxz, area_range=[20, 30],  
                 x_y_ratio_range=[1,1.5], angle_range=[90, 135], **kwargs):
                         
        RandomStructure.__init__(self, atoms, **kwargs)
    
        self.minz, self.maxz = minz, maxz
        self.alpha=np.array(area_range)
        self.beta=np.array(x_y_ratio_range)
        self.theta=np.array(angle_range)
        self.relaxer = None
    
    def get_xyz(self, cell):
        x = cell[0] * self.rng.random()
        y = cell[1] * self.rng.random()

        # z-coordinate is a vector along the cell[2] axis
        # with random length between minz and maxz:
        zlength = (self.maxz - self.minz) * self.rng.random() + self.minz
        z = zlength * cell[2] / np.linalg.norm(cell[2])

        return x + y + z

    def get(self):
        '''
        Mostly duplication of RandomBox.get()
        '''
        newatoms = self.atoms.copy()
        area=self.rng.uniform(self.alpha[0],self.alpha[1]) # *self.area_est
        ratio=self.rng.uniform(self.beta[0],self.beta[1])  #*self.y_to_x_ratio_est
        theta=self.rng.uniform(self.theta[0],self.theta[1])
        
        X=np.array([1,0,0])*(area/(np.sin(theta*np.pi/180)*ratio))**0.5#theta angle btwn y & x vector
        Y=np.array([np.cos(theta*np.pi/180),np.sin(theta*np.pi/180),0])*X[0]*ratio
        Z=np.array([0,0,1])*np.linalg.norm(newatoms.cell[2]) # fixed Z
        newatoms.set_cell(np.array([X,Y,Z]))
        cell=newatoms.cell
        newpos = []

        for atom in self.atoms:

            if atom.index in self.constr.cindex:
                pos = atom.position
            else:
                pos = self.get_xyz(cell)

            newpos.append(pos)

        newpos = np.array(newpos)

        newatoms.positions = newpos

        if self.relaxer is not None:
            newatoms=self.relaxer.run(newatoms)

        newatoms.wrap()
        newatoms.set_constraint(self.constr.constraints)
        newatoms.calc = None
        return newatoms

class ReadFiles():

    def __init__(self, filenames):

        self.frames = [read(fname) for fname in filenames]
        self.count = 0

    def get(self):
        atoms = self.frames[self.count]
        self.count += 1
        return atoms


class Rattler(RandomStructure):                
    
    def __init__(self, atoms, intensity=0.1, **kwargs):

        RandomStructure.__init__(self, atoms, **kwargs)

        self.intensity = intensity

    def get(self):
        atoms = self.atoms.copy()
        atoms.rattle(self.intensity,
                     seed=self.rng.randint(100000))
        atoms.wrap()
        return atoms


class ConstraintHandler:         
    
    '''
    Store constraints that are used in Atoms objects in BEACON.
    if two lits of fix_atoms are set, it only takes the first list
    '''

    def __init__(self, atoms):
        
        self.constraints = atoms.constraints
        self.cindex = self.get_fixed_index()          

    def get_fixed_index(self):
        '''
        Get the list of indices that are constrained with FixAtoms.
        '''
        constr_index = []
        for C in self.constraints:
            if isinstance(C, FixAtoms):
                constr_index = C.index
   
        return np.array(constr_index, dtype=int)

