import numpy as np
from gpatom.fractional_beacon.gpfp.kerneltypes import ICESquaredExp
from gpatom.gpfp.kernel import FPKernelParallel


class ICEKernel(FPKernelParallel):

    def __init__(self, kerneltype='sqexp', params=None):
        '''
        hp: dict
            Hyperparameters for the kernel type
        '''
        kerneltypes = {'sqexp': ICESquaredExp}

        if params is None:
            params = {}

        kerneltype = kerneltypes.get(kerneltype)
        self.kerneltype = kerneltype(**params)
      

class ICEKernelNoforces(ICEKernel):

    def kernel(self, x1, x2):
        return np.atleast_1d(self.kerneltype.kernel(x1, x2))

    def get_size(self, x):
        '''
        Return the size of a kernel matrix
        x: fingerprint
        '''
        return 1
