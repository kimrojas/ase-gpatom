import numpy as np
from gpatom.gpfp.fingerprint import (RadialFP, RadialFPCalculator,
                                     RadialFPGradientCalculator,
                                     RadialAngularFP, RadialAngularFPCalculator,
                                     RadialAngularFPGradientCalculator,
                                     AtomPairs, AtomTriples,
                                     FPElements, RadialFPStrainCalculator,
                                     RadialAngularFPStrainCalculator)





class PartialFingerPrint:
    
    
    def __init__(self, fp_args={}, fractional_elements=None, 
                 Ghost_mode=False, angular=True,
                 calc_coord_gradients=True,
                 calc_frac_gradients=True,
                 calc_strain=False):
        
        self.fp_args=fp_args
        self.Ghost_mode=Ghost_mode
        self.fractional_elements=fractional_elements
        self.calc_coord_gradients=calc_coord_gradients
        self.calc_frac_gradients=calc_frac_gradients    
        self.calc_strain=calc_strain
        
        if angular:
            self.fp_class=PartialRadAngFP
        else:
            self.fp_class=PartialFP
    
    def get(self, atoms, fractions=None):   
        
            fmask = [(symbol in self.fractional_elements)
                     for symbol in atoms.get_chemical_symbols()]
                
            if fractions is None:
                fractions = FPFractions.get_full_fractions(atoms, self.fractional_elements, self.Ghost_mode)

            fp = self.fp_class(atoms=atoms, 
                               fractions=fractions,
                               elements=self.fractional_elements,
                               fmask=fmask,
                               calc_coord_gradients=self.calc_coord_gradients,
                               calc_frac_gradients=self.calc_frac_gradients,
                               calc_strain=self.calc_strain,
                               Ghost_mode=self.Ghost_mode,
                               **self.fp_args)
 
            return fp



class PartialFP(RadialFP):

    def __init__(self, atoms=None, fractions=None,
                 calc_coord_gradients=True,
                 calc_frac_gradients=True,
                 calc_strain=False,
                 fmask=None, Ghost_mode=False,        
                 elements=None, **kwargs):

        '''
        fractions: array(Natoms) fraction for each atom
        fmask:     array(Natoms) boolean for each atom;
                                 if fmask[i] == True, then atom i is fractional
        elements: array(2) elements which are fractionized
        '''

        default_parameters = {'r_cutoff': 8.0,
                              'r_delta': 0.4,
                              'r_nbins': 200}
    
        self.params = default_parameters.copy()
        self.params.update(kwargs)

        self.param_names = ['r_cutoff', 'r_delta', 'r_nbins']


        if elements is None:
            elements = np.sort(list(set(atoms.symbols)))

           
        if (len(elements) !=2  and  (not Ghost_mode)):
            raise RuntimeError('System has more than 2 elements. '
                               'Fractionized elements need to '
                               'be specified.')


        if fmask is None:
            fmask = [(symbol in elements)
                     for symbol in atoms.get_chemical_symbols()]

        if len(atoms) != len(fmask):
            
            raise RuntimeError('Number of fractions must match '
                               'the number of fractioned indices.')

        self.atoms = atoms.copy()
        self.atoms.wrap()

        if fractions is None:
            fractions = FPFractions.get_full_fractions(atoms, elements, Ghost_mode)

        

       # self.fractions=fractions   # to make it possible to import into prior
        self.atoms.fractions=fractions

        pairs = AtomPairs(self.atoms,
                          self.params['r_cutoff'])
        
        
        self.pairs = pairs

        frac = FPFractions(self.atoms, fractions, fmask, elements)
        
    
        if Ghost_mode:
            factor_method = FPFractions.get_ghost_products_for_pairs  
        else:
            factor_method = FPFractions.get_fraction_products_for_pairs                            

        factors=factor_method(pairs,frac, calc_frac_gradients)

        fpparams = dict(pairs=pairs,
                        factors=factors,
                        cutoff=self.params['r_cutoff'],
                        width=self.params['r_delta'],
                        nbins=self.params['r_nbins'])

        self.rho_R = PartialFPCalculator.calculate(**fpparams)
        
        self.vector = self.rho_R.flatten()

        self.gradients = (PartialFPGradientCalculator.
                          calculate(natoms=self.natoms, **fpparams, calc_coord_gradients=calc_coord_gradients))

        self.strain = (PartialRadialFPStrainCalculator.calculate(**fpparams)
                       if calc_strain else None)                  
        
        if Ghost_mode:                
            gradient_calculator=PartialFPFractionGradientCalculator.calculate_ghost
        else: 
            gradient_calculator=PartialFPFractionGradientCalculator.calculate
            
            
        self.frac_gradients = (gradient_calculator(self.natoms, pairs,
                                             frac,
                                             cutoff=self.params['r_cutoff'],
                                             width=self.params['r_delta'],
                                             nbins=self.params['r_nbins'],
                                             calc_frac_gradients=calc_frac_gradients))

    @property
    def natoms(self):
        return len(self.atoms)

    def reduce_frac_gradients(self):
        return self.frac_gradients.reshape(self.natoms, -1, 1)


class PartialFPCalculator(RadialFPCalculator):

    @classmethod
    def calculate(cls, pairs, factors, cutoff, width, nbins):
        '''
        Calculate the Gaussian-broadened fingerprint.
        '''

        if pairs.empty:
            return np.zeros([pairs.elem.nelem, pairs.elem.nelem, nbins])

        # Gaussians with correct heights:
        gs = cls.get_gs(pairs=pairs,
                         width=width,
                         cutoff=cutoff,
                         nbins=nbins)
        gs *= cls.get_peak_heights(pairs=pairs,
                                    cutoff=cutoff,
                                    nbins=nbins)[:, np.newaxis]

        rho_R = np.einsum('hij,hl->ijl', factors, gs, optimize=True)

        return rho_R

class PartialFPGradientCalculator(PartialFPCalculator,
                                  RadialFPGradientCalculator):

    @classmethod
    def calculate(cls, natoms, pairs, factors, cutoff, width, nbins, calc_coord_gradients):                     
        '''
        Calculate fingerprint gradients with respect to atomic positions.
        '''
        gradients = np.zeros([natoms, pairs.elem.nelem,
                              pairs.elem.nelem, nbins, 3])
                
        if not calc_coord_gradients:
            return gradients
                
        if pairs.empty:
            return gradients

        results = np.einsum('ij,ik->ijk',
                            cls.get_gradient_gaussians(pairs, cutoff,
                                                        nbins, width),
                            -pairs.rm,
                            optimize=True)

        for p in range(len(pairs.indices)):
            i, j_ = pairs.indices[p]
            j = j_ % natoms

            gradients[i] += np.einsum('ij,kl->ijkl',
                                       factors[p],
                                       results[p],
                                       optimize=False)
            gradients[j] += -np.einsum('ij,kl->ijkl',
                                         factors[p],
                                         results[p],
                                         optimize=False)

        return gradients


class PartialFPFractionGradientCalculator(PartialFPCalculator):

    @classmethod
    def calculate(cls, natoms, pairs, fractions,
                  cutoff, width, nbins, calc_frac_gradients):

        f_gradients = np.zeros([natoms, pairs.elem.nelem, pairs.elem.nelem,
                              nbins, 1])
        
        if not calc_frac_gradients:  
            return f_gradients         

        if pairs.empty:
            return f_gradients

        gs = (cls.get_peak_heights(pairs, cutoff, nbins)[:, np.newaxis] *
              cls.get_gs(pairs, width, cutoff, nbins))

        fmask = fractions.fmask
        q = fractions.fractions
        C, D = fractions.ei

        for p, (i, j_) in enumerate(pairs.indices):
            j = j_ % natoms

            if not fmask[i] or not fmask[j]:
                A, B = pairs.elem.indices[p]

                if fmask[i]:  # if atom i is fractioned

                    f_gradients[i, C, B, :, 0] += gs[p]
                    f_gradients[i, D, B, :, 0] += - gs[p]
                    f_gradients[i, B, C, :, 0] += gs[p]
                    f_gradients[i, B, D, :, 0] += - gs[p]

            else:  # if both i and j are fractioned
                f_gradients[i, C, C, :, 0] += 2 * q[j] * gs[p]
                f_gradients[i, C, D, :, 0] += (1 - 2 * q[j]) * gs[p]
                f_gradients[i, D, C, :, 0] += (1 - 2 * q[j]) * gs[p]
                f_gradients[i, D, D, :, 0] += - 2 * (1 - q[j]) * gs[p]

        return f_gradients
    
    
    
    
    @classmethod
    def calculate_ghost(cls, natoms, pairs, fractions,
                  cutoff, width, nbins, calc_frac_gradients):
               
        
        f_gradients = np.zeros([natoms, pairs.elem.nelem, pairs.elem.nelem,
                              nbins, 1])
                
        if not calc_frac_gradients:  
            return f_gradients 
                            
        if pairs.empty:
            return f_gradients

        gs = (cls.get_peak_heights(pairs, cutoff, nbins)[:, np.newaxis] *
              cls.get_gs(pairs, width, cutoff, nbins))
        
        fmask = fractions.fmask
        q = fractions.fractions
        
        
        for p, (i, j_) in enumerate(pairs.indices):              
            j = j_ % natoms
            A, B = pairs.elem.indices[p]
            
            if A in fractions.ei:
                C=A

            if B in fractions.ei:
                D=B    


            if not fmask[i] or not fmask[j]:
                A, B = pairs.elem.indices[p]

                if fmask[i]:  # if atom i is fractioned
                    f_gradients[i, C, B, :, 0] += gs[p]
                    
                elif fmask[j]:    
                    f_gradients[j, A, D, :, 0] += gs[p] 

            else:  # if both i and j are fractioned
                f_gradients[i, C, D,:,0] += q[j] * gs[p]  
                f_gradients[j, C, D,:,0] += q[i] * gs[p]
        return f_gradients      




class PartialRadialFPStrainCalculator(RadialFPStrainCalculator):
    
    @classmethod
    def calculate(cls, pairs, factors, cutoff, width, nbins):

        ''' Derivative of fingerprint w.r.t. cell parameters '''
        result = np.zeros([pairs.elem.nelem, pairs.elem.nelem, nbins, 3, 3])

        if pairs.empty:
            return result

        gradients = cls.get_gradients(pairs, cutoff, nbins, width)

        result = np.einsum('hij,hlgs->ijlgs', factors, gradients, optimize=True)

        return result






class PartialRadAngFP(PartialFP, RadialAngularFP):

    def __init__(self, atoms=None, fractions=None,
                 calc_coord_gradients=True,
                 calc_frac_gradients=True,
                 calc_strain=False,
                 fmask=None, Ghost_mode=False,
                 elements=None, **kwargs):
        
        '''
        fractions: array(Natoms) fraction for each atom
        fmask:     array(Natoms) boolean for each atom;
                                 if fmask[i] == True, then atom i is fractional
        elements: array(2) elements which are fractionized
        '''
        
        PartialFP.__init__(self, atoms, fractions,
                           fmask=fmask,
                           Ghost_mode=Ghost_mode,               
                           elements=elements,
                           calc_coord_gradients=calc_coord_gradients,
                           calc_frac_gradients=calc_frac_gradients,
                           calc_strain=calc_strain,
                           **kwargs)

        default_parameters = {'r_cutoff': 8.0,
                             'r_delta': 0.4,
                              'r_nbins': 200,
                              'a_cutoff': 4.0,
                              'a_delta': 0.4,
                              'a_nbins': 100,
                              'gamma': 0.5,
                              'aweight': 1.0}

        

        self.params = default_parameters.copy()
        self.params.update(kwargs)
        

        # parameters in this class for constructing the fingerprint:
        self.param_names = ['r_cutoff', 'r_delta', 'r_nbins',
                            'a_cutoff', 'a_delta', 'a_nbins',
                            'aweight']

        assert self.params['r_cutoff'] >= self.params['a_cutoff']

        triples = AtomTriples(self.atoms,
                              cutoff=self.params['r_cutoff'],
                              cutoff2=self.params['a_cutoff'])
        
        
        self.triples = triples

        if elements is None:
            elements = np.sort(list(set(atoms.symbols)))

        if fractions is None:
            fractions = FPFractions.get_full_fractions(atoms, elements, Ghost_mode)
            
        self.fractions=fractions   # to make it possible to import into prior   

        if fmask is None:
            fmask = [(symbol in elements)
                     for symbol in atoms.get_chemical_symbols()]


        frac = FPFractions(self.atoms, fractions, fmask, elements)
        
        if Ghost_mode:
            factor_method = FPFractions.get_ghost_products_for_triples
        else:
            factor_method = FPFractions.get_fraction_products_for_triples
           

        factors=factor_method(triples,frac, calc_frac_gradients)
                                                         
        
        fpparams = dict(triples=triples,
                        factors=factors,
                        cutoff=self.params['a_cutoff'],
                        width=self.params['a_delta'],
                        nbins=self.params['a_nbins'],
                        aweight=self.params['aweight'],
                        gamma=self.params['gamma'])

        self.rho_a = PartialRadAngFPCalculator.calculate(**fpparams)
        
        
        self.vector = np.concatenate((self.rho_R.flatten(),
                                      self.rho_a.flatten()), axis=None)
        
 

        self.anglegradients = (PartialRadAngFPGradientCalculator.
                               calculate(natoms=self.natoms,
                                         **fpparams,   calc_coord_gradients=calc_coord_gradients))
        
        self.anglestrain = (PartialRadialAngularFPStrainCalculator.calculate(**fpparams)
                            if calc_strain else None)
        

        if Ghost_mode:
            gradient_calculator=PartialRadAngFPFractionGradientCalculator.calculate_ghost
        else:
            gradient_calculator= PartialRadAngFPFractionGradientCalculator.calculate
            
           
        self.frac_gradients_angles = (gradient_calculator(self.natoms,
                                                    triples,
                                                    frac,
                                                    cutoff=self.params['a_cutoff'],
                                                    width=self.params['a_delta'],
                                                    nbins=self.params['a_nbins'],
                                                    aweight=self.params['aweight'],
                                                    gamma=self.params['gamma'],
                                                    calc_frac_gradients=calc_frac_gradients))                                   

    def reduce_frac_gradients(self):

        return np.concatenate((self.frac_gradients.reshape(self.natoms, -1, 1),
                               self.frac_gradients_angles.reshape(self.natoms, -1, 1)),
                              axis=1)



class PartialRadAngFPCalculator(RadialAngularFPCalculator):

    @classmethod
    def calculate(cls, triples, factors, cutoff, width,
                  nbins, aweight, gamma):

        rho_a = np.zeros([triples.elem.nelem, triples.elem.nelem,
                          triples.elem.nelem, nbins])

        if triples.empty:
            return rho_a

        # Gaussians with correct height:   # the reason for not working appear to be because 
        # of the class method label versus the self label in the real main code.  which I changed.
        # will probably explain all the errors.  some clever descision should be made here.
        gs = cls.get_cutoff_ags(triples, cutoff, width, nbins, aweight, gamma)
        rho_a = np.einsum('hijk,hl->ijkl', factors, gs)

        return rho_a


class PartialRadAngFPGradientCalculator(RadialAngularFPGradientCalculator):

    @classmethod
    def calculate(cls, natoms, triples, factors, cutoff,
                  width, nbins, aweight, gamma, calc_coord_gradients):

        gradients = np.zeros([natoms, triples.elem.nelem, triples.elem.nelem,
                              triples.elem.nelem, nbins, 3])

        if not calc_coord_gradients:
            return gradients


        if triples.empty:
            return gradients

        firsts, seconds, thirds = cls.do_anglegradient_math(triples, cutoff,
                                                            width, nbins,
                                                            aweight, gamma)

        for p in range(len(triples.indices)):
            i, j_, k_ = triples.indices[p]
            j = j_ % natoms
            k = k_ % natoms

            # derivative w.r.t. atom i:
            gradients[i] += np.einsum('ijk,lm->ijklm',
                                      factors[p],
                                      firsts[p],
                                      optimize=False)

            # derivative w.r.t. atom j:
            gradients[j] += np.einsum('ijk,lm->ijklm',
                                      factors[p],
                                      seconds[p],
                                      optimize=False)

            # derivative w.r.t. atom k:
            gradients[k] += np.einsum('ijk,lm->ijklm',
                                      factors[p],
                                      thirds[p],
                                      optimize=False)

        return gradients



class PartialRadAngFPFractionGradientCalculator(RadialAngularFPCalculator):

    @classmethod
    def calculate(cls, natoms, triples, fractions,
                  cutoff, width, nbins, aweight, gamma, calc_frac_gradients):


        f_gradients = np.zeros([natoms,
                                triples.elem.nelem,
                                triples.elem.nelem,
                                triples.elem.nelem,
                                nbins])

        if not calc_frac_gradients:  
            return f_gradients 

        if triples.empty:
            return f_gradients

        gs = cls.get_cutoff_ags(triples, cutoff, width, nbins, aweight, gamma)
        
        #CL This is not the same factors as above for calculating coord-gradients and fingerprint. 
        # this is specific to the angular fraction gradient solvers
        factors = cls.get_factor_product_gradients_for_triples(natoms,  
                                                               triples,
                                                               fractions)

        D, E = fractions.ei
        result = np.einsum('pijk,pl->pijkl',
                           factors,
                           gs,
                           optimize=True)


        for p, (i, j_, k_) in enumerate(triples.indices):
            j = j_ % natoms
            k = k_ % natoms
            r0, r1, r2 = result[p]

            f_gradients[i, D, :, :] += r0
            f_gradients[i, E, :, :] += -r0

            f_gradients[j, :, D, :] += r1
            f_gradients[j, :, E, :] += -r1

            f_gradients[k, :, :, D] += r2
            f_gradients[k, :, :, E] += -r2

        return f_gradients



    @staticmethod
    def get_factor_product_gradients_for_triples(natoms, triples, fractions):

        factors = np.zeros((len(triples.indices), 3,
                            triples.elem.nelem, triples.elem.nelem))

        fmask = fractions.fmask
        q = fractions.fractions
        D, E = fractions.ei

        for p in range(len(triples.indices)):

            i, j_, k_ = triples.indices[p]
            j = j_ % natoms
            k = k_ % natoms
            A, B, C = triples.elem.indices[p]

            if (fmask[i] and
                not fmask[j] and
                not fmask[k]):
                factors[p, 0, B, C] += 1.0

            elif (not fmask[i] and
                  fmask[j] and
                  not fmask[k]):
                factors[p, 1, A, C] += 1.0

            elif (not fmask[i] and
                  not fmask[j] and
                  fmask[k]):
                factors[p, 2, A, B] += 1.0

            elif (not fmask[i] and
                  fmask[j] and
                  fmask[k]):
                factors[p, 1, A, D] += q[k]
                factors[p, 1, A, E] += 1 - q[k]

                factors[p, 2, A, D] += q[j]
                factors[p, 2, A, E] += 1 - q[j]

            elif (fmask[i] and
                  fmask[j] and
                  not fmask[k]):
                factors[p, 0, D, C] += q[j]
                factors[p, 0, E, C] += 1 - q[j]

                factors[p, 1, D, C] += q[i]
                factors[p, 1, E, C] += 1 - q[i]

            elif (fmask[i] and
                  not fmask[j] and
                  fmask[k]):

                factors[p, 0, B, D] += q[k]
                factors[p, 0, B, E] += 1 - q[k]

                factors[p, 2, D, B] += q[i]
                factors[p, 2, E, B] += 1 - q[i]

            elif (fmask[i] and
                  fmask[j] and
                  fmask[k]):

                factors[p, 0, D, D] += q[j] * q[k]
                factors[p, 0, D, E] += q[j] * (1 - q[k])
                factors[p, 0, E, D] += (1 - q[j]) * q[k]
                factors[p, 0, E, E] += (1 - q[j]) * (1 - q[k])

                factors[p, 1, D, D] += q[i] * q[k]
                factors[p, 1, D, E] += q[i] * (1 - q[k])
                factors[p, 1, E, D] += (1 - q[i]) * q[k]
                factors[p, 1, E, E] += (1 - q[i]) * (1 - q[k])

                factors[p, 2, D, D] += q[i] * q[j]
                factors[p, 2, D, E] += q[i] * (1 - q[j])
                factors[p, 2, E, D] += (1 - q[i]) * q[j]
                factors[p, 2, E, E] += (1 - q[i]) * (1 - q[j])

        return factors


    @classmethod
    def calculate_ghost(cls, natoms, triples, fractions,
                  cutoff, width, nbins, aweight, gamma, calc_frac_gradients):
        
        f_gradients = np.zeros([natoms,
                                triples.elem.nelem,
                                triples.elem.nelem,
                                triples.elem.nelem,
                                nbins])


        if not calc_frac_gradients:
            return f_gradients


        if triples.empty:
            return f_gradients

        gs = cls.get_cutoff_ags(triples, cutoff, width, nbins, aweight, gamma)


        fmask = fractions.fmask
        q = fractions.fractions

        for p, (i, j_, k_) in enumerate(triples.indices):
            j=j_ % natoms
            k=k_ % natoms
            A, B, C = triples.elem.indices[p]
            
            if A in fractions.ei:
                D=A
            if B in fractions.ei:
                E=B
            if C in fractions.ei:
                F=C


            if (fmask[i] and
                not fmask[j] and
                not fmask[k]):
                f_gradients[i, D, B, C, :] += gs[p]             
                
            elif (not fmask[i] and
                  fmask[j] and
                  not fmask[k]):
                f_gradients[j, A, E, C, :] += gs[p] 

            elif (not fmask[i] and
                  not fmask[j] and
                  fmask[k]):
                f_gradients[k, A, B, F, :] += gs[p] 

            elif (not fmask[i] and
                  fmask[j] and
                  fmask[k]):
                f_gradients[j, A, E, F, :] += q[k] * gs[p]
                f_gradients[k, A, E, F, :] += q[j] * gs[p] 

            elif (fmask[i] and
                  fmask[j] and
                  not fmask[k]):
                f_gradients[i, D, E, C, :] += q[j] * gs[p]
                f_gradients[j, D, E, C, :] += q[i] * gs[p]

            elif (fmask[i] and
                  not fmask[j] and
                  fmask[k]):
                f_gradients[i, D, B, F, :] += q[k] * gs[p]
                f_gradients[k, D, B, F, :] += q[i] * gs[p] 

            elif (fmask[i] and
                  fmask[j] and
                  fmask[k]):
                f_gradients[i, D, E, F, :] += q[j] * q[k] * gs[p]
                f_gradients[j, D, E, F, :] += q[i] * q[k] * gs[p]
                f_gradients[k, D, E, F, :] += q[i] * q[j] * gs[p] 
                
        return f_gradients


class PartialRadialAngularFPStrainCalculator(RadialAngularFPStrainCalculator):

    @classmethod
    def calculate(cls, triples, factors, cutoff, width, nbins, aweight, gamma):
        '''
        Derivative of the fingerprint vector w.r.t. the cell
        parameters.

        This method has not been optimized in speed.
        '''
        result = np.zeros([triples.elem.nelem, triples.elem.nelem,
                           triples.elem.nelem, nbins, 3, 3])

        if triples.empty:
            return result

        results=cls.get_results(triples, cutoff, width, nbins, aweight, gamma)
        
        result = np.einsum('hijk,hlgs->ijklgs', factors, results, optimize=True)
        
        return result


class FPFractions:

    def __init__(self, atoms, fractions, fmask, felements):
        self.atoms = atoms
        self.fractions = self.set_fractions_for_not_fmask(fractions, fmask)
        self.fmask = fmask
        self.ei = [list(FPElements.get_elementset(atoms)).index(e)
                   for e in felements]

    @staticmethod
    def set_fractions_for_not_fmask(fractions, fmask):
        return [(fractions[i] if fmask[i] else 1.0)
                for i in range(len(fractions))]

    @staticmethod
    def get_fraction_products_for_pairs(pairs, fractions, calc_frac_gradients):

        factors = np.zeros((len(pairs.indices), pairs.elem.nelem,
                            pairs.elem.nelem))

        fmask = fractions.fmask  # alias
        q = fractions.fractions  # alias
        natoms = len(q)
        C, D = fractions.ei  # element indices for fractionized elements

        for p in range(len(pairs.indices)):
            i, j_ = pairs.indices[p]
            j = j_ % natoms

            if not fmask[i] or not fmask[j]:
                A, B = pairs.elem.indices[p]

                if fmask[i]:
                    factors[p, C, B] += q[i]
                    factors[p, D, B] += 1 - q[i]

                if fmask[j]:
                    factors[p, A, C] += q[j]
                    factors[p, A, D] += 1 - q[j]

                if (not fmask[i]) and (not fmask[j]):
                    factors[p, A, B] += 1

            else:
                factors[p, C, C] += q[i] * q[j]
                factors[p, C, D] += q[i] * (1 - q[j])
                factors[p, D, C] += (1 - q[i]) * q[j]
                factors[p, D, D] += ((1 - q[i]) *
                                     (1 - q[j]))
                
                  
        return factors

    @staticmethod
    def get_ghost_products_for_pairs(pairs, fractions, calc_frac_gradients): 

        #if not calc_frac_gradients:
        #factors = np.ones((len(pairs.indices), pairs.elem.nelem, pairs.elem.nelem))
         #   return factors
                              
        factors = np.zeros((len(pairs.indices), pairs.elem.nelem, pairs.elem.nelem))


        fmask = fractions.fmask  # alias        
        q = fractions.fractions 
        natoms = len(q)
  

        for p in range(len(pairs.indices)):
            i, j_ = pairs.indices[p]
            j = j_ % natoms    # get the atom indices

            A, B = pairs.elem.indices[p]   # get the element indices

            if A in fractions.ei:   # assert if the pickes elements are fractional
                C=A    
            if B in fractions.ei:   # then remove C=fractions.ei above
                D=B


            if not fmask[i] or not fmask[j]:
                
                if fmask[i]:     # if this happens C=A and D is unset
                    factors[p, C, B] += q[i]

                if fmask[j]:     #  if this happens B=D and C is unset
                    factors[p, A, D] += q[j]

                if (not fmask[i]) and (not fmask[j]):
                    factors[p, A, B] += 1

            else:
                factors[p, C, D] += q[i] * q[j]   # if this happens both C and D are set       
        
        return factors 



    @staticmethod
    def get_fraction_products_for_triples(triples, fractions, calc_frac_gradients):
        '''
        Precompute all products between fractions and
        extended fractions for atoms in triples.
        '''
        #if not calc_frac_gradients:
        factors = np.zeros((len(triples.indices), triples.elem.nelem,
                            triples.elem.nelem, triples.elem.nelem))

        fmask = fractions.fmask  # alias
        q = fractions.fractions  # alias
        natoms = len(q)
        D, E = fractions.ei  # element indices for fractionized elements.   # this is getting overwritten

        for p in range(len(triples.indices)):
            i, j_, k_ = triples.indices[p]
            j = j_ % natoms
            k = k_ % natoms
            A, B, C = triples.elem.indices[p]
            
            if (not fmask[i] and
                not fmask[j] and
                not fmask[k]):
                factors[p, A, B, C] += 1

            elif (fmask[i] and
                  not fmask[j] and
                  not fmask[k]):
                factors[p, D, B, C] += q[i]
                factors[p, E, B, C] += 1 - q[i]

            elif (not fmask[i] and
                  fmask[j] and
                  not fmask[k]):
                factors[p, A, D, C] += q[j]
                factors[p, A, E, C] += 1 - q[j]

            elif (not fmask[i] and
                  not fmask[j] and
                  fmask[k]):
                factors[p, A, B, D] += q[k]
                factors[p, A, B, E] += 1 - q[k]

            elif (fmask[i] and
                  fmask[j] and
                  not fmask[k]):

                factors[p, D, D, C] += q[i] * q[j]
                factors[p, D, E, C] += q[i] * (1 - q[j])
                factors[p, E, D, C] += (1 - q[i]) * q[j]
                factors[p, E, E, C] += (1 - q[i]) * (1 - q[j])

            elif (fmask[i] and
                  not fmask[j] and
                  fmask[k]):

                factors[p, D, B, D] += q[i] * q[k]
                factors[p, D, B, E] += q[i] * (1 - q[k])
                factors[p, E, B, D] += (1 - q[i]) * q[k]
                factors[p, E, B, E] += (1 - q[i]) * (1 - q[k])

            elif (not fmask[i] and
                  fmask[j] and
                  fmask[k]):
                factors[p, A, D, D] += q[j] * q[k]
                factors[p, A, D, E] += q[j] * (1 - q[k])
                factors[p, A, E, D] += (1 - q[j]) * q[k]
                factors[p, A, E, E] += (1 - q[j]) * (1 - q[k])

            elif (fmask[i] and
                  fmask[j] and
                  fmask[k]):
                factors[p, D, D, D] += q[i] * q[j] * q[k]
                factors[p, D, D, E] += q[i] * q[j] * (1 - q[k])
                factors[p, D, E, D] += q[i] * (1 - q[j]) * q[k]
                factors[p, D, E, E] += q[i] * (1 - q[j]) * (1 - q[k])
                factors[p, E, D, D] += (1 - q[i]) * q[j] * q[k]
                factors[p, E, D, E] += (1 - q[i]) * q[j] * (1 - q[k])
                factors[p, E, E, D] += (1 - q[i]) * (1 - q[j]) * q[k]
                factors[p, E, E, E] += (1 - q[i]) * (1 - q[j]) * (1 - q[k])

        return factors
    

    @staticmethod
    def get_ghost_products_for_triples(triples, fractions, calc_frac_gradients):
        '''
        Precompute all products between fractions and
        extended fractions for atoms in triples.
        '''
        #if not calc_frac_gradients:
        factors = np.zeros((len(triples.indices), triples.elem.nelem,
                            triples.elem.nelem, triples.elem.nelem))

        fmask = fractions.fmask  # alias
        q = fractions.fractions  # alias
        natoms = len(q)

        for p in range(len(triples.indices)):
            i, j_, k_ = triples.indices[p]
            j = j_ % natoms
            k = k_ % natoms
            A, B, C = triples.elem.indices[p]
            
            
            if A in fractions.ei:
                D=A
            if B in fractions.ei:
                E=B        
            if C in fractions.ei:
                F=C


            if (not fmask[i] and
                not fmask[j] and
                not fmask[k]):
                factors[p, A, B, C] += 1

            elif (fmask[i] and
                  not fmask[j] and
                  not fmask[k]):
                factors[p, D, B, C] += q[i]

            elif (not fmask[i] and
                  fmask[j] and
                  not fmask[k]):
                factors[p, A, E, C] += q[j]

            elif (not fmask[i] and
                  not fmask[j] and
                  fmask[k]):
                factors[p, A, B, F] += q[k]

            elif (fmask[i] and
                  fmask[j] and
                  not fmask[k]):
                factors[p, D, E, C] += q[i] * q[j]

            elif (fmask[i] and
                  not fmask[j] and
                  fmask[k]):
                factors[p, D, B, F] += q[i] * q[k]

            elif (not fmask[i] and
                  fmask[j] and
                  fmask[k]):
                factors[p, A, E, F] += q[j] * q[k]

            elif (fmask[i] and
                  fmask[j] and
                  fmask[k]):
                factors[p, D, E, F] += q[i] * q[j] * q[k]

        return factors



    @staticmethod
    def get_full_fractions(atoms, elements, Ghost_mode):

        '''
        Return a valid fraction array for full atoms.

        atoms: Atoms object
        elements: pair of elements as a sorted list, e.g. ['Ag', 'Cu']
        '''
        if Ghost_mode:
            fractions=np.ones(len(atoms))
        else:
            fractions = [(1.0 if atom.symbol == elements[0] else 0.0)
                         for atom in atoms]

        return fractions

        
        
        