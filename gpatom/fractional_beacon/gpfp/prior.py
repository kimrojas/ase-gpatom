#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  1 13:46:29 2023

@author: casper
"""

import numpy as np
from gpatom.gpfp.prior import CalculatorPrior, RepulsivePotential, RepulsivePotentialWithStress



def weight_potential(atom_idx, potential, derivative, weights, neighbors):
    ws=weights[atom_idx]*np.array( [ weights[n] for n in neighbors ] )                
                
    potential = potential * ws
    potential[ws<1e-5]=0
                
    derivative = derivative * np.tile(ws,(3,1)).T
    derivative[ws<1e-5]=0
    
    return potential, derivative
    


class WeightedCalculatorPrior(CalculatorPrior):    

    def __init__(self, calculator, constant, **kwargs):
                
        CalculatorPrior.__init__(self, calculator, constant=constant, **kwargs)

    def get_atoms(self,x):
        fractions=x.atoms.fractions
        atoms=x.atoms.copy()
        atoms.fractions=fractions
        return atoms
    
    


class WeightedRepulsivePotential(RepulsivePotential):
    ''' Repulsive potential of the form
    sum_ij q_iq_j(0.7 * (Ri + Rj) / rij)**12

    where Ri and Rj are the covalent radii of atoms
    '''

    implemented_properties = ['energy', 'forces']

    default_parameters = {'prefactor': 0.7,'rc': None, 'potential_type': 'LJ'}    
    
    nolabel = True
    
    def get_energy_and_forces(self, atoms, rc, prefactor, neighbor_list):
        
        energy, forces = self.setup_energy_forces(atoms)   
        
        # attempt getting weights
        weights=atoms.fractions
        
        for a1 in range(len(atoms)):
            
            neighbors, d = self.get_distance_info(atoms, a1, neighbor_list)
            
            if len(neighbors)>0:     
                                
                potential, derivative = self.get_potential(a1, rc, prefactor, d, neighbors)   
          
                
                potential, derivative = weight_potential(a1, potential, 
                                                         derivative, weights, 
                                                         neighbors)
                
                energy = self.update_energy(energy, potential)
                
                forces = self.update_forces(forces, derivative, a1, neighbors) 

        return energy, forces




class WeightedRepulsivePotentialWithStress(RepulsivePotentialWithStress):

    implemented_properties = ['energy', 'forces', 'stress']

    default_parameters = {'prefactor': 0.7,'rc': None, 
                          'potential_type': 'LJ', 'extrapotential': None}    
    
    nolabel = True    
    
    def get_energy_and_derivatives(self, atoms, rc, prefactor, neighbor_list):

        energy, forces = self.setup_energy_forces(atoms)   
        
        stress = np.zeros((3, 3))

        weights=atoms.fractions

        for a1 in range(len(atoms)):
            
            neighbors, d = self.get_distance_info(atoms, a1, neighbor_list)
            
            if len(neighbors)>0:     
                
                potential, derivative = self.get_potential(a1, rc, prefactor, d, neighbors)   
                
                potential, derivative = weight_potential(a1, potential, 
                                                         derivative, weights, 
                                                         neighbors)
                
                energy = self.update_energy(energy, potential)
                
                forces = self.update_forces(forces, derivative, a1, neighbors) 
                
                stress = self.update_stress(stress, derivative, d)
                
        return energy, forces, stress