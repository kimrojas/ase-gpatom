from gpatom.gpfp.gp import GaussianProcess
from gpatom.fractional_beacon.gpfp.kernel import ICEKernel, ICEKernelNoforces
import numpy as np


class ICEGaussianProcess(GaussianProcess):
    '''
    Gaussian process that is specifically used in ICEBEACON.
    The essential difference is that we need to predict
    the derivatives of the surrogate PES w.r.t. the fractions.
    '''

    def __init__(self, **kwargs):
        GaussianProcess.__init__(self, **kwargs)

        kernelparams = {'weight': self.hp['weight'],
                        'scale': self.hp['scale']}


        if self.use_forces:
            self.kernel = ICEKernel(kerneltype='sqexp', params=kernelparams)
        else:
            self.kernel = ICEKernelNoforces(kerneltype='sqexp', params=kernelparams)



    def set_sizes(self):
        self.natoms=len(self.X[0].atoms)
        self.ntrain=len(self.X)
        self.nforces=3 * (self.natoms)
        self.singlesize=1 + self.nforces
        self.alphasize=self.ntrain * self.singlesize


    def predict(self, x, get_variance=False, calc_frac_gradients=True):
        ''' If get_variance=False, then variance
        is returned as None '''
        
        
        self.set_sizes()
      
        
        if self.use_forces:

            kv = self.kernel.kernel_vector(x, self.X)  


            prior_array = self.calculate_prior_array([x])
            
            f = prior_array + np.dot(kv, self.model_vector) 
            
            coord_gradients = f[1:].reshape(self.natoms, 3)    
        
            if calc_frac_gradients:
                frac_gradients = self.get_frac_gradients(x)
            else:
                frac_gradients = np.zeros([self.natoms,1])
            assert frac_gradients.shape == (self.natoms, 1)  
            
            
            all_gradients = np.concatenate((coord_gradients, frac_gradients),
                                           axis=1).flatten()
            
            
            f = [f[0]] + list(all_gradients)

            f = np.array(f)

            V = self.calculate_variance(get_variance, kv, x)
            

        else:
            
            k = self.kernel.kernel_vector(x, self.X)
            
            f = np.dot(k, self.model_vector)
            
            dk_dxi = np.array([self.kernel.kerneltype.kernel_gradient(x, x2)
                               for x2 in self.X])
            dk_dq = np.array([self.kernel.kerneltype.kernel_gradient_frac(x, x2)
                               for x2 in self.X])

            dk = np.concatenate((dk_dxi, dk_dq), axis=2)

            forces = np.einsum('ijk,i->jk', dk,
                               self.model_vector).flatten()
            
            prior_array = self.calculate_prior_array([x], get_forces=True)
            prior_energy=prior_array[0]
            prior_forces=np.array(prior_array[1:]).reshape(-1,3)
            f+=prior_energy
            forces.reshape(-1,4)[:,0:3]+=prior_forces
            
            f = list([f]) + list(forces)
            
            f=np.array(f)
            
            V = self.calculate_variance(get_variance, k, x)
            
        return f, V

    def get_frac_gradients(self, x):

        dk_dq = np.array([self.kernel.kerneltype.kernel_gradient_frac(x, x2)
                          for x2 in self.X])
    
        # Fractions derivatives of kernel gradients:
        d2k_drm_dq = np.array([self.kernel.kerneltype.dkernelgradient_dq(x, x2)
                               for x2 in self.X])


        d2k_drm_dq = d2k_drm_dq.reshape(self.ntrain, self.natoms, self.nforces)   

        # Kernel vector:
        K_x_X = np.concatenate((dk_dq, d2k_drm_dq), axis=2)     
        K_x_X = K_x_X.swapaxes(0, 1)
        K_x_X = K_x_X.reshape((self.natoms, self.alphasize, 1))   

        
        frac_gradients = (np.einsum('ijk,j->ik', K_x_X, self.model_vector,
                                    optimize=True))
        
        return frac_gradients



    def get_properties(self, x, return_frac_grads=False, return_uncertainty=True):
        f, V = self.predict(x, get_variance=True)
        energy, forces, frac_grads = self.translate_predictions(f)
        
        if self.use_forces:
            uncertainty_squared = V[0, 0]
        else:
            uncertainty_squared = V[0]
        
        if return_frac_grads:
            return energy, forces, uncertainty_squared, frac_grads
        else: 
            return energy, forces, uncertainty_squared
        
    def translate_predictions(self, predict_array):
        
        energy=predict_array[0]
        grads=predict_array[1:].reshape(-1, 4)
        forces=grads[:, :3]
        
        frac_grads=grads[:, 3]
        
        return energy, forces, frac_grads
        
        




class GhostGaussianProcess(ICEGaussianProcess):
    def __init__(self, n_ghost=[0], **kwargs):       
        super().__init__(**kwargs)
        
        self.n_ghost=np.array(n_ghost)
        
    def set_sizes(self):

        extra_atoms=int(sum(self.n_ghost))

        self.natoms=len(self.X[0].atoms)+extra_atoms
        self.ntrain=len(self.X)
        self.nforces=3 * (self.natoms-extra_atoms)
        self.singlesize=1 + self.nforces
        self.alphasize=self.ntrain * self.singlesize
        
    def set_n_ghost(self, n_ghost):
        self.n_ghost=n_ghost
        
