
from gpatom.beacon.inout import FileWriter

from ase.io import write

from ase.calculators.singlepoint import SinglePointCalculator

from scipy.optimize import minimize
from scipy.optimize import LinearConstraint, OptimizeWarning
import numpy as np

import warnings

from ase.constraints import FixAtoms

from gpatom.fractional_beacon.drs import drs

from ase.stress import (full_3x3_to_voigt_6_stress,
                        voigt_6_to_full_3x3_stress)


'''
 The reference for this method is found here:

        S. Kaappa, C. Larsen, K. W. Jacobsen
        https://arxiv.org/abs/2107.01055 (2021)
        Atomic Structure Optimization with Machine-Learning Enabled
        Interpolation between Chemical Elements
        S. Kaappa, C. Larsen, and K. W. Jacobsen
        Physical Review Letters, vol. 127, 166001 (2021)
        https://doi.org/10.1103/PhysRevLett.127.166001
'''


class ICERandomFractionGenerator:
    '''
    Generates random fractions with given generating type and constraints.
    '''
        
    def __init__(self, frac_cindex=[], randomtype='drs',
                 fractional_elements=None, rng=np.random):        
        self.frac_cindex=frac_cindex
        self.randomtype = randomtype
        self.rng = rng
        self.fractional_elements=fractional_elements
        self.frac_lims=[0,1]

    
    def get_fractions(self, atoms):     
        
        fractions = self.get(atoms)
        frac_cindex = self.frac_cindex
            
        return fractions, frac_cindex



    def get(self, atoms):
        
        if self.randomtype == 'uniform':
            f = self.get_uniform(atoms)
    
        elif self.randomtype=='drs':
            f=self.get_dirichlet_rescale(atoms)
            
        elif self.randomtype=='whole_atoms':
            f=self.get_whole_atoms(atoms)     
                    
        else:
            raise RuntimeError('randomtype={:s} not known.'
                               .format(self.randomtype))
        return f
 
    
 
    def get_uniform(self, atoms):
        
        fmask, n_0 = FractionConverter.set_fractional_atoms(atoms, self.fractional_elements)
        natoms = len(atoms) 
         
        f=np.zeros(natoms)
        ci=self.frac_cindex     
        nci=[i for i in np.arange(natoms) if ( (i not in ci) and fmask[i])]   
        
        if len(ci)>0:          
            constrained_fractions=FractionConverter.atoms2fractions(atoms,self.fractional_elements)[ci]
            sum_constrained=sum(constrained_fractions)           
            n_0_remain=n_0- sum_constrained  
            f[ci]=constrained_fractions
        else:
            n_0_remain=n_0   
        
        
        reverse_fmask=[not b for b in fmask]
        val=n_0_remain/(natoms - len(self.frac_cindex) -  sum(np.array(reverse_fmask, dtype=int)))
        f[nci]=val       
        
        return f


    def get_dirichlet_rescale(self, atoms):
        
        
        
        fmask, n_0 = FractionConverter.set_fractional_atoms(atoms, self.fractional_elements)
        
        return self.get_drs_fractions(atoms, n_0, fmask)
        
    
    def get_drs_fractions(self, atoms, f_sum, fmask):        
        
        natoms=len(atoms)
        
        lower_limits=np.ones(natoms)*self.frac_lims[0]        
        upper_limits=np.ones(natoms)*self.frac_lims[1]    
        
        reverse_fmask=[not b for b in fmask]
        nfidx=[i for i in np.arange(natoms) if reverse_fmask[i]] 
        upper_limits[nfidx]=0.0
        lower_limits[nfidx]=0.0
    
        if len(self.frac_cindex)>0:
            ci=self.frac_cindex
            constrained_fractions=FractionConverter.atoms2fractions(atoms,self.fractional_elements)[ci]
            lower_limits[ci]=constrained_fractions
            upper_limits[ci]=constrained_fractions
        
        f=drs(natoms, f_sum, upper_limits, lower_limits, seed=self.rng.randint(1000000))
        f=np.array(f)
        
        return f
    
    
    def get_whole_atoms(self, atoms):
        f=FractionConverter.atoms2fractions(atoms,self.fractional_elements)
        
        return f




class FractionConverter:  

    @staticmethod
    def set_fractional_atoms(atoms, fractional_elements):  
        '''
        Takes atoms object and list of fractionalized elements
        to define a vector of which atoms are fractionalized
        and how many of the first fractionalised species exists
        '''
        symbol_list = list(atoms.symbols[:])
        fmask = [(symbol in fractional_elements) for symbol in symbol_list]
        n_0 = symbol_list.count(fractional_elements[0])  # count of first element
                
        return fmask, n_0   
  
    @staticmethod
    def atoms2fractions(atoms, fractional_elements):  # used multiple placess
        '''
        Convert ase.Atoms object to fractions (or integers because
        everything is full atoms)
        '''
        fractions = [(1.0 if atom.symbol == fractional_elements[0] else 0.0)
                     for atom in atoms]
        
        return np.array(fractions)


    @staticmethod
    def fractions2atoms(fractions, atoms, fractional_elements, constrained_fractions):
        # element 0 have fraction 1.             element 1 have fraction 0. 
        atoms=atoms.copy()
        
        fmask, n_0 = FractionConverter.set_fractional_atoms(atoms, fractional_elements)        
        transformable_atoms=np.array(fmask)
        transformable_atoms[constrained_fractions]=False
                
        count=0
        if len(constrained_fractions)>0:
            for i in constrained_fractions:
                if atoms.symbols[i]==fractional_elements[0]:
                    count+=1 
        
        argsort = np.argsort(fractions)[::-1]   # argsorting from highest to lowest. 

        for idx in argsort:
            if transformable_atoms[idx]:
                if count <n_0:
                    atoms.symbols[idx]=fractional_elements[0]
                    count+=1
                else:
                    atoms.symbols[idx]=fractional_elements[1]
  
        
        assert list(atoms.symbols).count(fractional_elements[0]) == n_0  
        
        return atoms
 


    
class ICESurrogateOptimizer:    
    '''
    Small optimizer class to handle local optimizations in ICEBEACON.
    '''

    def __init__(self, fmax=0.05, 
                 relax_steps=100,  
                 write_surropt_trajs=False,
                 write_surropt_fracs=False,
                 randomtype='drs',
                 with_unit_cell=False,
                 fixed_cell_params = None,
                 frac_cindex=[],
                 fractional_elements=None,
                 pos_lims=np.array([-1,-1,-1,1,1,1])*np.inf,
                 post_rounding_steps=0,
                 derivative_modulation=1.0,  
                 fractions_rng=np.random):
        
        
        self.fmax = fmax*derivative_modulation
        self.relax_steps = relax_steps
        self.write_surropt_trajs = write_surropt_trajs
        self.write_surropt_fracs = write_surropt_fracs

        self.frac_cindex=frac_cindex
        self.pos_lims=pos_lims
        self.frac_lims=[0,1]

        self.post_rounding_steps=post_rounding_steps
        self.derivative_modulation=derivative_modulation

        
        self.fractional_elements=fractional_elements

        self.rfg = ICERandomFractionGenerator(frac_cindex=frac_cindex,
                                              randomtype=randomtype,
                                              fractional_elements=fractional_elements,
                                              rng=fractions_rng)


        self.with_unit_cell=with_unit_cell
                
        

        if self.with_unit_cell:
            self.relax_method=self.constrain_and_minimize_unitcell
        else:
            self.relax_method=self.constrain_and_minimize
            
        if self.with_unit_cell:
            if fixed_cell_params is None:
                fixed_cell_params = [False]*6    
            self.opt_cell_mask = np.array([not elem for elem in fixed_cell_params], dtype=int)

    def get_constrained_atoms(self, atoms):
        pos_cindex = []
        for C in atoms.constraints:
            if isinstance(C, FixAtoms):
                pos_cindex=C.index
                
        return pos_cindex
    
    
    
    def _calculate_properties(self, params, *args):    
        
        atoms = args[1]
        natoms = len(atoms)
        
        positions, fractions=ICEParamsHandler.unpack_atomic_params(natoms, params)
        
        atoms.positions=positions
        # calculate fp and predict energy, force and stress with gp
        model=args[2]
        
        fp=model.fp.get(atoms=atoms, fractions=fractions) 
        
        fit_fractions=args[3]        
        
        predictions, variance = model.gp.predict(fp, get_variance=False,  
                                                 calc_frac_gradients=fit_fractions)
        
        energy, atoms_forces, atoms_frac_grads = model.gp.translate_predictions(predictions)
        
        # write the current state of the system
        writer = args[0]
        
        writer.set_atoms(atoms=atoms, fractions=fractions, energy=energy, gradients=atoms_forces)
        
        derivatives=ICEParamsHandler.pack_atomic_params(natoms, atoms_forces,  atoms_frac_grads)
        # rescale all steps and output of the as the SLSQP routine has a tendency to take too large steps
        energy_rescale, derivatives_rescale=self.rescale_output(energy, derivatives)

        return energy_rescale  , np.array(derivatives_rescale)
 
    
    
    


    def _calculate_properties_unitcell(self, params, *args):
        '''
        Function to be minimized. Returns the predicted energy and
        its gradients w.r.t. both coordinates and fractions.
        '''        
        
        #  collect deformed parameters from params
        atoms = args[1]
        natoms = len(atoms)
        
        deformed_positions, fractions, deformation_tensor=ICEParamsHandler.unpack_params(natoms, params)
        
        original_cell=args[4]
        cell_factor=args[5] 
        # transform deformed paramameters into real parameters and attach to atoms copy
        atoms=UnitCellHandler.atoms_deformed_to_real(atoms, deformation_tensor, deformed_positions, original_cell, cell_factor)

        # calculate fp and predict energy, force and stress with gp in real parameters
        model=args[2]
        
        fp=model.fp.get(atoms=atoms, fractions=fractions) 
    
        fit_fractions=args[3]        
        
        predictions, variance = model.gp.predict(fp, get_variance=False,  
                                                 calc_frac_gradients=fit_fractions)    
        
        energy, atoms_forces, atoms_frac_grads = model.gp.translate_predictions(predictions)
        
        stress = model.gp.predict_stress(fp)

        # write the current state of the system to writer in real parameters
        
        writer = args[0]

        writer.set_atoms(atoms=atoms, fractions=fractions, energy=energy, gradients=atoms_forces)
        
        # transform forces to deformed parameters
        
        deformed_forces, deformed_virial = UnitCellHandler.forces_real_to_deformed(atoms, atoms_forces, stress, original_cell, cell_factor)
 
        deformed_virial = UnitCellHandler.apply_cell_mask(deformed_virial, self.opt_cell_mask)   

        derivatives=ICEParamsHandler.pack_params(natoms, deformed_forces,  atoms_frac_grads, deformed_virial)

        energy_rescale, derivatives_rescale=self.rescale_output(energy, derivatives)

        return energy_rescale  , np.array(derivatives_rescale)
    

    def rescale_output(self, energy, derivatives):
        energy = energy  *   self.derivative_modulation
        derivatives = derivatives  * self.derivative_modulation
        return energy, derivatives


    def initiate_writer(self, atoms, fractions, model, file_identifier):
        
        writer=self.get_writer(atoms, file_identifier)

        fp=model.new_fingerprint(atoms=atoms, fractions=fractions)  
        predictions, variance = model.gp.predict(fp, get_variance=False)
   
        energy, atoms_forces, atoms_frac_grads = model.gp.translate_predictions(predictions)

        writer.set_atoms(atoms=atoms, fractions=fractions, energy=energy , gradients=atoms_forces)
   
        params=ICEParamsHandler.pack_params(len(atoms), atoms.positions, fractions, atoms.cell )
        
        writer.write_atoms(params)
        return writer

    def get_writer(self, atoms, file_identifier):
        writer = OptimizationWriter(atoms=atoms,
                                    fractional_elements=self.fractional_elements,
                                    file_identifier=file_identifier,
                                    write_surropt_fracs=self.write_surropt_fracs,
                                    write_surropt_trajs=self.write_surropt_trajs,
                                    convert_atoms=True)
        return writer


    def relax(self, atoms, model, file_identifier=''):  
        
        '''
        Relax atoms in the surrogate potential.
        '''
    
        fractions, frac_cindex = self.rfg.get_fractions(atoms)
    
        writer=self.initiate_writer(atoms, fractions, model, file_identifier)

        success, opt_atoms, opt_fractions = self.relax_method(atoms, model, fractions, self.frac_lims, self.relax_steps, frac_cindex, writer)
        
        opt_atoms=self.defractionalize_atoms(opt_atoms, opt_fractions, frac_cindex)
        
        if self.post_rounding_steps>0:
            success, opt_atoms = self.round_relax(opt_atoms, model, self.post_rounding_steps, writer, frac_cindex)
   
        return opt_atoms, success


    def round_relax(self, atoms, model, steps, writer, frac_cindex):

        round_fractions=FractionConverter.atoms2fractions(atoms, self.fractional_elements)
        
        frac_cindex=np.arange(len(atoms))
        
        success, opt_atoms, opt_fractions = self.relax_method(atoms, model, round_fractions, [0,1], steps, frac_cindex, writer)
        
        opt_atoms = FractionConverter.fractions2atoms(opt_fractions,       
                                                      atoms=opt_atoms,
                                                      fractional_elements=self.fractional_elements,
                                                      constrained_fractions=frac_cindex)
        
        return success, opt_atoms


    def defractionalize_atoms(self, opt_atoms, opt_fractions, frac_cindex):


        opt_atoms = FractionConverter.fractions2atoms(opt_fractions,       
                                                      atoms=opt_atoms,
                                                      fractional_elements=self.fractional_elements,
                                                      constrained_fractions=frac_cindex)       
        
        return opt_atoms


    def constrain_and_minimize(self, atoms, model, fractions, frac_lims, steps, frac_cindex, writer):
        
        natoms=len(atoms)
        
        params=ICEParamsHandler.pack_atomic_params(natoms, atoms.positions, fractions)
        
        linear_constraints=self.get_constraints(atoms, fractions, frac_cindex, frac_lims)
        
        with warnings.catch_warnings():
            
            fit_fractions=(len(frac_cindex)<natoms)
            
            warnings.filterwarnings('ignore', category=OptimizeWarning)
            result = minimize(self._calculate_properties,   
                              params,
                              args=(writer, atoms, model, fit_fractions), 
                              method='SLSQP',
                              constraints=linear_constraints,
                              jac=True,
                              options={'ftol':self.fmax, 'maxiter': steps},
                              callback=writer.write_atoms)

        success = result['success']
        opt_array = result['x']  
      
        positions, fractions = ICEParamsHandler.unpack_atomic_params(natoms,opt_array)
        atoms.positions=positions
           
        return success, atoms, fractions        
   
    def constrain_and_minimize_unitcell(self, atoms, model, fractions, frac_lims, steps, frac_cindex, writer):
        
        natoms=len(atoms)
        original_cell=atoms.get_cell()
        cell_factor=float(natoms)   
        
        deformation_tensor, deformed_positions = UnitCellHandler.atoms_real_to_deformed(atoms, original_cell, cell_factor)

        params=ICEParamsHandler.pack_params(natoms, deformed_positions, fractions, deformation_tensor) 

        linear_constraints=self.get_constraints(atoms, fractions, frac_cindex, frac_lims)
        
        with warnings.catch_warnings():
            
            fit_fractions=(len(frac_cindex)<natoms)
            
            warnings.filterwarnings('ignore', category=OptimizeWarning)
            result = minimize(self._calculate_properties_unitcell,   
                              params,
                              args=(writer, atoms, model, fit_fractions, original_cell, cell_factor), 
                              method='SLSQP',
                              constraints=linear_constraints,
                              jac=True,
                              options={'ftol':self.fmax, 'maxiter': steps , 'eps':0.1 },
                              callback=writer.write_atoms)

        success = result['success']
        opt_array = result['x']  
      
        deformed_positions, fractions, deformation_tensor = ICEParamsHandler.unpack_params(natoms,opt_array)

        atoms=UnitCellHandler.atoms_deformed_to_real(atoms, deformation_tensor, deformed_positions, original_cell, cell_factor)
           
        return success, atoms, fractions   









   
    def get_constraints(self, atoms, fractions, frac_cindex, frac_lims):


        fmask, n_0 = FractionConverter.set_fractional_atoms(atoms, self.fractional_elements)
        
        pos_cindex=self.get_constrained_atoms(atoms)
        
        constraints = SurrOptConstr.get_constraints(atoms,
                                                    fractions,
                                                    fmask, 
                                                    n_0,
                                                    pos_cindex,
                                                    frac_cindex,      
                                                    self.pos_lims,
                                                    frac_lims,
                                                    self.with_unit_cell)

        return constraints
    


class ICEParamsHandler:

    @staticmethod     
    def pack_params(natoms, position_params, fraction_params, cell_params):
        assert np.shape(cell_params)==(3,3)
        atomic_params = ICEParamsHandler.pack_atomic_params(natoms, position_params, fraction_params)
        params= np.concatenate((atomic_params, cell_params.flatten()), axis=0)
        return params

    @staticmethod
    def pack_atomic_params(natoms, position_params, fraction_params):
        if np.shape(fraction_params)!=(natoms,1):
            fraction_params=fraction_params.reshape(natoms,1)
        assert np.shape(position_params)==(natoms,3)
        assert np.shape(fraction_params)==(natoms,1)
        atomic_params = np.concatenate((position_params, fraction_params), axis=1).flatten()
        return atomic_params
    
    @staticmethod 
    def unpack_params(natoms, params):
        atomic_params=params[0:-9]
        cell_params=params[-9::].reshape(3,3)
        position_params, fraction_params = ICEParamsHandler.unpack_atomic_params(natoms, atomic_params) 
        return position_params, fraction_params, cell_params

    @staticmethod 
    def unpack_atomic_params(natoms, params):
        atomic_params=params.reshape(natoms, 4)
        position_params = atomic_params[:, :3]            
        fraction_params = atomic_params[:, 3].flatten() 
        return position_params, fraction_params
    


    
class SurrOptConstr:  
    '''
    Collection of static methods to build linear constraints for optimization
    with SLSQP minimizer within Scipy.
    '''

    @staticmethod
    def get_constraints(atoms, fractions, fmask, n_0,
                        cindex, frac_cindex, pos_lims, frac_lims, with_unit_cell):
                
        '''
        Get appropriate linear constraints for atoms based on the index of
        constrained atoms ('cindex') and the fraction mask (fmask')
        '''
        
        c = [SurrOptConstr.get_fixed_number_of_atoms(len(atoms), n_0, fmask, with_unit_cell)]    
        
        
        linear_constraints=SurrOptConstr.get_linear_constraints(atoms, fractions, fmask, cindex, frac_cindex, pos_lims, frac_lims, with_unit_cell)
        
        c.append(linear_constraints)
        return tuple(c)


    @staticmethod
    def get_linear_constraints(atoms, fractions, fmask, cindex, frac_cindex, pos_lims, frac_lims, with_unit_cell):
        A, lb, ub = SurrOptConstr.init_arrays(len(atoms), with_unit_cell)
        
        lb, ub = SurrOptConstr.constrain_positions(atoms, cindex, pos_lims, lb, ub)
        lb, ub = SurrOptConstr.constrain_fractions(fractions, fmask, frac_cindex, frac_lims, lb, ub)
        
        if with_unit_cell:
            lb, ub = SurrOptConstr.constrain_unit_cell(lb, ub)
        
        return LinearConstraint(A=A,lb=lb,ub=ub)
        
    @staticmethod    
    def constrain_positions(atoms, cindex, pos_lims, lb, ub):
        for i in range(len(atoms)):
            if i in cindex:
                lb, ub = SurrOptConstr.set_single_constrained(lb, ub, atoms, i)
            else:
                 lb, ub = SurrOptConstr.set_single_unconstrained(lb, ub, atoms, i, pos_lims)
        return lb, ub
        
        
    @staticmethod    
    def constrain_fractions(fractions, fmask, frac_cindex, frac_lims, lb, ub):
        for i in range(len(fmask)):
            if not fmask[i]:                    # i.e. if fmask False. i.e. atoms not fractional
               # set fractions to zero for non-fractional atoms:
               lb[4 * i + 3] = 0         
               ub[4 * i + 3] = 0
               # constrain fractions
            elif i in frac_cindex:
               lb[4 * i + 3] = fractions[i]
               ub[4 * i + 3] = fractions[i]
            else:
               # unconstrain fractions:
               lb[4 * i + 3] = frac_lims[0]
               ub[4 * i + 3] = frac_lims[1]
        return lb, ub
        

    @staticmethod
    def constrain_unit_cell(lb, ub):
        lb[-9::]=-np.inf
        ub[-9::]=np.inf
        
        return lb, ub
        
            
    @staticmethod
    def get_fixed_number_of_atoms(natoms, n_0, fmask, with_unit_cell):
        '''
        Get equality constraints to fix the number of atoms
        of different elements.
        '''
        A = np.zeros(4 * natoms)           
        A[3::4][fmask] = 1
        if with_unit_cell:
            A=np.concatenate( (A, np.zeros(9)), axis=0 )   
        
        return LinearConstraint(A=A,
                                lb=n_0,
                                ub=n_0)


    @staticmethod
    def set_single_constrained(lb, ub, atoms, index):
        '''
        Set single coordinate constraint when atom with index
        'index' is constrained with FixAtoms.
        '''
        i = index
        lb[4 * i: 4 * i + 3] = atoms.positions[i]
        ub[4 * i: 4 * i + 3] = atoms.positions[i]

        return lb, ub  


    @staticmethod
    def set_single_unconstrained(lb, ub, atoms, index, pos_lims):
        '''
        Set single coordinate constraint if atom with index
        'index' is not constrained.

        XXX Debug me. Right now I assume a rectangular unit cell.
        '''
        i = index
        lb[4 * i: 4 * i + 3] = pos_lims[:3]
        ub[4 * i: 4 * i + 3] = pos_lims[3:]
        return lb, ub   
     
        
    @staticmethod
    def init_arrays(natoms, with_unit_cell):
        
        if with_unit_cell:
            A = np.eye(4 * natoms + 9)
            lb, ub = (np.zeros(4 * natoms +9),
                      np.zeros(4 * natoms +9))
            
        else:
            A = np.eye(4 * natoms)
            lb, ub = (np.zeros(4 * natoms),
                      np.zeros(4 * natoms))
            
            
        return A, lb, ub       
    
    
    
class UnitCellHandler:
    
    @staticmethod
    def deform_grad(original_cell, new_cell):
        return np.linalg.solve(original_cell, new_cell).T
    
    @staticmethod
    def atoms_deformed_to_real(atoms_deformed, deformation_tensor, deformed_positions, original_cell, cell_factor):
        atoms_real = atoms_deformed.copy()

        new_deform_grad = deformation_tensor / cell_factor
        
        new_cell=original_cell @ new_deform_grad.T
        
        atoms_real.set_cell(new_cell, scale_atoms=True)
        
        real_positions=deformed_positions @ new_deform_grad.T
        
        atoms_real.set_positions(real_positions)
        
        return atoms_real
    
    @staticmethod
    def atoms_real_to_deformed(atoms_real, original_cell, cell_factor):
        
        deformation_gradient = UnitCellHandler.deform_grad(original_cell, atoms_real.cell)
        
        deformation_tensor = cell_factor * deformation_gradient
        
        deformed_positions = np.linalg.solve(deformation_gradient,
                                             atoms_real.positions.T).T 
        
        return deformation_tensor, deformed_positions
    
    @staticmethod 
    def forces_real_to_deformed(atoms_real, atoms_forces, stress, original_cell, cell_factor):
        cur_deform_grad=UnitCellHandler.deform_grad(original_cell, atoms_real.cell) 
        
        deformed_forces = atoms_forces @ cur_deform_grad

        volume = atoms_real.get_volume()

        negative_virial = volume * (voigt_6_to_full_3x3_stress(stress) )  
        
        deformed_virial = np.linalg.solve(cur_deform_grad, negative_virial.T).T
        
        deformed_virial=deformed_virial / cell_factor
        
        return deformed_forces, deformed_virial
    
    
    @staticmethod 
    def apply_cell_mask(deformed_virial, opt_cell_mask):
                
        mask = voigt_6_to_full_3x3_stress(opt_cell_mask)
            
        deformed_virial *= mask
        
        return deformed_virial
     
        

class OptimizationWriter:   
    '''
    Handles output of trajectories and atom fractions.
    '''

    def __init__(self, atoms, fractional_elements, file_identifier='',
                 write_surropt_fracs=False, write_surropt_trajs=False, convert_atoms=True):

        self.atoms = atoms
        self.natoms = len(atoms)
        self.fractional_elements = fractional_elements
        self.write_surropt_fracs = write_surropt_fracs
        self.write_surropt_trajs = write_surropt_trajs
        self.convert_atoms=convert_atoms


        if self.write_surropt_fracs:
            self.relaxfile = FileWriter('opt_'+file_identifier+'.txt',
                                        printout=False,
                                        write_time=False)

        if self.write_surropt_trajs:
            self.optfilename = 'opt_'+file_identifier+'.xyz'

            # format:
            f = open(self.optfilename, 'w')
            f.close()

    def set_atoms(self, atoms, fractions, energy, gradients):
        self.atoms = atoms
        self.natoms = len(atoms)
        self.fractions=fractions
        self.energy = energy
        self.gradients = gradients

    def write_atoms(self, params):
       
        if self.write_surropt_fracs:
            frac_string = ''.join(['{:6.02f}'.format(f) for f in self.fractions])
            
            
            self.relaxfile.write('{} {:12.04f}'.format(frac_string,
                                                       self.energy))

        if self.write_surropt_trajs:
            atoms = self.atoms.copy()

            # converting fractions to atoms.  only for writing.
            # not technically necessary.   only relevant to ICE-BEACON
            if self.convert_atoms:
                atoms = FractionConverter.fractions2atoms(fractions=self.fractions.copy(),
                                                          atoms=atoms,
                                                          fractional_elements=self.fractional_elements,
                                                          constrained_fractions=[])

            
            results = dict(energy=self.energy,
                           forces=-self.gradients)
            atoms.calc = SinglePointCalculator(atoms, **results)

            with warnings.catch_warnings():

                # with EMT, a warning is triggered while writing the
                # results in ase/io/extxyz.py. Lets filter that out:
                warnings.filterwarnings('ignore', category=UserWarning)


###################################################################
                atoms.set_initial_charges(charges=self.fractions)
###################################################################
                write(self.optfilename,
                      atoms,
                      append=True,
                      parallel=False)
