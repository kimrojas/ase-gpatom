#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 21:55:07 2023

@author: casper
"""
from scipy.optimize import LinearConstraint
import numpy as np

from gpatom.fractional_beacon.icebeacon import (ICERandomFractionGenerator, OptimizationWriter, 
                                                ICESurrogateOptimizer, SurrOptConstr)
from gpatom.fractional_beacon.drs import drs

    
class GhostRandomFractionGenerator(ICERandomFractionGenerator):
    '''
    Generates random fractions with given generating type and constraints.
    '''
        
    def __init__(self, n_real, frac_cindex=[], randomtype='drs',
                 fractional_elements=None, lower_lim=0, rng=np.random):        
        self.n_real=n_real
        self.frac_cindex=frac_cindex
        self.randomtype = randomtype
        self.rng = rng
        self.fractional_elements=fractional_elements
        self.frac_lims=[lower_lim, 1]


    def get_fractions(self, atoms):
            
        n_ghost=self.get_n_ghost(atoms)
        
        if np.any(n_ghost<0):
            raise RuntimeError('negative number of ghosts in structure') 
        
        if np.all(n_ghost==0):
            fractions=GhostHandler.atoms2fractions(atoms, self.fractional_elements)
            frac_cindex=np.arange(len(atoms))
        else: 
            fractions=self.get(atoms)
            frac_cindex=self.frac_cindex
         
        return fractions, frac_cindex


    def get(self, atoms):
        
        if self.randomtype == 'uniform':
            f = self.get_uniform(atoms)
    
        elif self.randomtype=='drs':
            f=self.get_dirichlet_rescale(atoms)
            
        elif self.randomtype=='whole_atoms':
            f=self.get_whole_atoms(atoms)     
                    
        else:
            raise RuntimeError('randomtype={:s} not known.'
                               .format(self.randomtype))
        return f
 


    def get_n_ghost(self, atoms):   
        fmask, n_0 = GhostHandler.set_fractional_atoms(atoms, self.fractional_elements) 
        n_ghost = n_0 - self.n_real
        return n_ghost
    
    
    def get_uniform(self, atoms):
        
        fmask, n_0 = GhostHandler.set_fractional_atoms(atoms, self.fractional_elements)

        natoms = len(atoms) 

        n_ghost=self.get_n_ghost(atoms) 
        
        reverse_fmask=[not b for b in fmask]
        nfidx=[i for i in np.arange(natoms) if reverse_fmask[i]] 


        
        
        f=np.zeros(natoms)        
        f[nfidx]=1

        
        symbol_list = list(atoms.symbols[:])
        for k in range(len(self.fractional_elements)):
            
            ci=[i for i, symbol in enumerate(symbol_list) 
                           if (symbol == self.fractional_elements[k]) and (i in self.frac_cindex)]
            
            nci = [i for i, symbol in enumerate(symbol_list) 
                           if (symbol == self.fractional_elements[k]) and (i not in self.frac_cindex)]     

            if len(ci)>0:          
                constrained_fractions=GhostHandler.atoms2fractions(atoms,self.fractional_elements)[ci]
                sum_constrained=sum(constrained_fractions)           
                n_0_remain=n_0[k]- sum_constrained - n_ghost[k]       +  n_ghost[k]*self.frac_lims[0]
                f[ci]=constrained_fractions
            else:
                n_0_remain=n_0[k] - n_ghost[k]  +  n_ghost[k]*self.frac_lims[0]

            val=n_0_remain/(n_0[k] - len(ci) )
            f[nci]=val   
   
        return f


    def get_dirichlet_rescale(self, atoms):
    
        natoms=len(atoms)
    
        fmask, n_0 = GhostHandler.set_fractional_atoms(atoms, self.fractional_elements)

        n_ghost=self.get_n_ghost(atoms)
        
        f_sum=n_0-n_ghost + n_ghost*self.frac_lims[0]

        reverse_fmask=[not b for b in fmask]
        nfidx=[i for i in np.arange(natoms) if reverse_fmask[i]] 

        f=np.zeros(natoms) 
        f[nfidx]=1
        
        symbol_list = list(atoms.symbols[:])
        for k in range(len(self.fractional_elements)):
            
            ci=[i for i, symbol in enumerate(symbol_list) 
                           if (symbol == self.fractional_elements[k]) and (i in self.frac_cindex)]
            
            nci = [i for i, symbol in enumerate(symbol_list) 
                           if (symbol == self.fractional_elements[k]) and (i not in self.frac_cindex)] 
            
            lower_limits=np.zeros(natoms)        
            upper_limits=np.zeros(natoms)
            lower_limits[nci]=self.frac_lims[0]
            upper_limits[nci]=self.frac_lims[1]
            lower_limits[ci]=1
            upper_limits[ci]=1
            
            f_elem=drs(natoms, f_sum[k], upper_limits, lower_limits, seed=self.rng.randint(1000000))
            
            f+=f_elem
            
        return f

        
    
    
    def get_whole_atoms(self, atoms):
        f=GhostHandler.atoms2fractions(atoms,self.fractional_elements)
        return f




class GhostHandler:
        
    @staticmethod
    def construct_processed_atoms_object(atoms, selected_atoms):
        
        processed_atoms=atoms.copy() 
        processed_atoms=atoms[selected_atoms]
        
        return processed_atoms

    
    @staticmethod   
    def exclude_nonconserved_ghosts(atoms, exclusion_list, n_ghost, constrained_atoms, fmask, fractional_elements):

        
        removeable_atoms=np.array(fmask)           # false means that the atom is conserved. cant be erased
        removeable_atoms[constrained_atoms]=False
        
        ghost_mask=np.ones(len(atoms),dtype=bool)  # remove atom if false
        
        symbol_list = list(atoms.symbols[:])
        
        for k in range(len(fractional_elements)):
        
            count=0   
            for i in exclusion_list:
                if  removeable_atoms[i] and symbol_list[i]==fractional_elements[k]:
                    ghost_mask[i]=False
                    count+=1

                                
                if count==n_ghost[k]:
                    break

               
        return ghost_mask


    @staticmethod
    def generate_ghosts_constrained(atoms, fractions, n_ghost, constrained_atoms, fmask, fractional_elements):

        
        if np.all(n_ghost==0):

            return np.ones(len(atoms),dtype=bool)
        
        argsort = np.argsort(fractions)

        ghost_mask=GhostHandler.exclude_nonconserved_ghosts(atoms, argsort, n_ghost, constrained_atoms, fmask, fractional_elements)


        return ghost_mask  


    @staticmethod
    def set_fractional_atoms(atoms, fractional_elements):
        '''
        Takes atoms object and list of fractionalized elements
        to define a vector of which atoms are fractionalized
        and how many of the first fractionalised species exists
        '''
        symbol_list = list(atoms.symbols[:])
        fmask = [(symbol in fractional_elements) for symbol in symbol_list]
        
        n_0=[]

        for i in range(len(fractional_elements)): 
            n_0.append(symbol_list.count(fractional_elements[i]))  # count of first element
                
        return fmask, n_0   
  
    @staticmethod
    def atoms2fractions(atoms, fractional_elements):  # used multiple placess
        '''
        Convert ase.Atoms object to fractions (or integers because
        everything is full atoms)
        '''
        return np.ones(len(atoms)) 



class GhostSurrogateOptimizer(ICESurrogateOptimizer):
    def __init__(self, fmax=0.05, 
                 relax_steps=100,   
                 write_surropt_trajs=False,
                 write_surropt_fracs=False,
                 randomtype='drs',
                 frac_cindex=[],
                 fractional_elements=None,
                 with_unit_cell=False,
                 fixed_cell_params=None,
                 n_real=None,
                 pos_lims=np.array([-1,-1,-1,1,1,1])*np.inf,
                 lower_lim=0,
                 defractioning_steps=0,
                 defractioning_sub_steps=0,
                 post_rounding_steps=0,
                 derivative_modulation=1.0,
                 fractions_rng=np.random):
        

        self.n_real=n_real
    
        self.fmax = fmax*derivative_modulation
        self.relax_steps = relax_steps
        self.write_surropt_trajs = write_surropt_trajs
        self.write_surropt_fracs = write_surropt_fracs

        self.frac_cindex=frac_cindex
        self.pos_lims=pos_lims
        self.frac_lims=[lower_lim, 1]
        
        self.defractioning_steps=defractioning_steps
        self.defractioning_sub_steps=defractioning_sub_steps
        self.post_rounding_steps=post_rounding_steps
        self.derivative_modulation=derivative_modulation

        self.fractional_elements=fractional_elements

        self.rfg = GhostRandomFractionGenerator(frac_cindex=frac_cindex,
                                                randomtype=randomtype,
                                                fractional_elements=fractional_elements,
                                                n_real=n_real,
                                                lower_lim=lower_lim,
                                                rng=fractions_rng)
        
        self.with_unit_cell=with_unit_cell
        
        if self.with_unit_cell:
            self.relax_method=self.constrain_and_minimize_unitcell
        else:
            self.relax_method=self.constrain_and_minimize
            
        if self.with_unit_cell:
            if fixed_cell_params is None:
                fixed_cell_params = [False]*6    
            self.opt_cell_mask = np.array([not elem for elem in fixed_cell_params], dtype=int)

    def relax(self, atoms, model, file_identifier):  
        
        '''
        Relax atoms in the surrogate potential.
        '''
        
        fractions, frac_cindex = self.rfg.get_fractions(atoms)
        
        n_ghost= self.rfg.get_n_ghost(atoms)     
           
        writer=self.initiate_writer(atoms, fractions, model, file_identifier)

        success, opt_atoms, opt_fractions = self.relax_method(atoms, model, fractions, self.frac_lims, self.relax_steps, frac_cindex, writer)
        
        if self.defractioning_steps>0:
            success, opt_atoms, opt_fractions= self.defractionate_relax(opt_atoms, model, opt_fractions, writer, n_ghost, frac_cindex)
            
        opt_atoms=self.defractionalize_atoms(opt_atoms, opt_fractions, n_ghost, frac_cindex)
        
        if self.post_rounding_steps>0:
            model.gp.set_n_ghost(np.zeros(len(self.fractional_elements)))
            success, opt_atoms = self.round_relax(opt_atoms, model, self.post_rounding_steps, writer, frac_cindex)
   
        return opt_atoms, success  



    def get_writer(self, atoms, file_identifier):
        writer = OptimizationWriter(atoms=atoms,
                                    fractional_elements=self.fractional_elements,
                                    file_identifier=file_identifier,
                                    write_surropt_fracs=self.write_surropt_fracs,
                                    write_surropt_trajs=self.write_surropt_trajs,
                                    convert_atoms=False)
        return writer



    def defractionalize_atoms(self, opt_atoms, opt_fractions, n_ghost, frac_cindex):

         fmask, n_0 = GhostHandler.set_fractional_atoms(opt_atoms, self.fractional_elements)
         ghost_mask=GhostHandler.generate_ghosts_constrained(opt_atoms, opt_fractions, n_ghost, frac_cindex, fmask, self.fractional_elements)

         opt_atoms=GhostHandler.construct_processed_atoms_object(opt_atoms, ghost_mask)
            
         return opt_atoms




    def defractionate_relax(self, atoms, model, fractions, writer, n_ghost, frac_cindex):
        f_change=self.frac_lims[0]*n_ghost/self.defractioning_steps
        for i in range(self.defractioning_steps):
            lower_lim= self.frac_lims[0] * (self.defractioning_steps-(i+1))/self.defractioning_steps
            
            symbol_list = list(atoms.symbols[:])
        
            for k in range(len(self.fractional_elements)):
                indices = [i for i, symbol in enumerate(symbol_list) 
                           if (symbol == self.fractional_elements[k]) and (i not in frac_cindex)]
                          
                fractions[indices]=fractions[indices]-np.ones(len(indices))*(f_change[k]/len(indices))

            
            success, opt_atoms, opt_fractions = self.relax_method(atoms, model, fractions, [lower_lim,1], self.defractioning_sub_steps, frac_cindex, writer)
                    
        return success, opt_atoms, opt_fractions 



    def round_relax(self, atoms, model, steps, writer, frac_cindex):

        round_fractions=GhostHandler.atoms2fractions(atoms, self.fractional_elements)
        
        frac_cindex=np.arange(len(atoms))
        
        success, opt_atoms, opt_fractions = self.relax_method(atoms, model, round_fractions, [0,1], steps, frac_cindex, writer)
    
        return success, opt_atoms



    def get_constraints(self, atoms, fractions, frac_cindex, frac_lims):


        fmask, n_0 = GhostHandler.set_fractional_atoms(atoms, self.fractional_elements)

        
        pos_cindex=self.get_constrained_atoms(atoms)
        
        n_ghost= self.rfg.get_n_ghost(atoms)
        
        constraints = GhostSurrOptConstr.get_constraints(atoms,
                                                         fractions,
                                                         fmask, 
                                                         n_0,
                                                         n_ghost,
                                                         pos_cindex,
                                                         frac_cindex,      
                                                         self.pos_lims,
                                                         frac_lims, 
                                                         self.fractional_elements,
                                                         self.with_unit_cell)
        
        return constraints


class GhostSurrOptConstr:
    
    
    @staticmethod
    def get_constraints(atoms, fractions, fmask, n_0, n_ghost,
                        cindex, frac_cindex, pos_lims, frac_lims, fractional_elements, with_unit_cell):
                
        '''
        Get appropriate linear constraints for atoms based on the index of
        constrained atoms ('cindex') and the fraction mask (fmask')
        '''

        c = [GhostSurrOptConstr.get_fixed_number_of_atoms(atoms, n_0, fmask, n_ghost, frac_lims, fractional_elements, with_unit_cell)]    
        
        
        linear_constraints=GhostSurrOptConstr.get_linear_constraints(atoms, fractions, fmask, cindex, frac_cindex, pos_lims, frac_lims, with_unit_cell)
        
        c.append(linear_constraints)
        return tuple(c)
    
    
    @staticmethod
    def get_linear_constraints(atoms, fractions, fmask, cindex, frac_cindex, pos_lims, frac_lims, with_unit_cell):
        A, lb, ub = SurrOptConstr.init_arrays(len(atoms), with_unit_cell)
        
        lb, ub = SurrOptConstr.constrain_positions(atoms, cindex, pos_lims, lb, ub)
        lb, ub = GhostSurrOptConstr.constrain_fractions(fractions, fmask, frac_cindex, frac_lims, lb, ub)
        
        if with_unit_cell:
            lb, ub = SurrOptConstr.constrain_unit_cell(lb, ub)
        
        return LinearConstraint(A=A,lb=lb,ub=ub)
    
    
    @staticmethod    
    def constrain_fractions(fractions, fmask, frac_cindex, frac_lims, lb, ub):
        for i in range(len(fmask)):
            if not fmask[i]:                    # i.e. if fmask False. i.e. atoms not fractional
               # set fractions to zero for non-fractional atoms:
               lb[4 * i + 3] = 1         
               ub[4 * i + 3] = 1
               # constrain fractions
            elif i in frac_cindex:
               lb[4 * i + 3] = fractions[i]
               ub[4 * i + 3] = fractions[i]
            else:
               # unconstrain fractions:
               lb[4 * i + 3] = frac_lims[0]
               ub[4 * i + 3] = frac_lims[1]
        return lb, ub
    
    
    @staticmethod
    def get_fixed_number_of_atoms(atoms, n_0, fmask, n_ghost, frac_lims, fractional_elements, with_unit_cell):
        '''
        Get equality constraints to fix the number of atoms
        of different elements.
        '''
        A_template = np.zeros(4 * len(atoms))
         
        A_template[3::4][fmask] = 1
        
        symbol_list = list(atoms.symbols[:])
 
        A=[]
        for element in fractional_elements: 
            not_element=[True if not symbol==element else False for symbol in symbol_list]
            A_copy=A_template.copy()
            A_copy[3::4][not_element]=0
            
            if with_unit_cell:
                A_copy=np.concatenate( (A_copy, np.zeros(9)), axis=0 )
            
            A.append(A_copy)

        
        return LinearConstraint(A=A,
                                lb=n_0-n_ghost + n_ghost*frac_lims[0],
                                ub=n_0-n_ghost + n_ghost*frac_lims[0])
    
    
    