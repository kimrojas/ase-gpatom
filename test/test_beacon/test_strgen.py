from gpatom.beacon.str_gen import (ConstraintHandler,
                                   Remake,
                                   Rattler,
                                   RandomBox,
                                   BoxConstructor,
                                   RandomBranch,
                                   RandomCell,
                                   Random2D,
                                   Random2DRanges,
                                   AtomsRelaxer,
                                   AtomsAndStressRelaxer,
                                   AtomsInsideBoxRelaxer)

from gpatom.gpfp.prior import RepulsivePotential, RepulsivePotentialWithStress

import numpy as np
from ase import Atoms
from ase.constraints import FixAtoms
from ase.data import covalent_radii

def get_constrained_atoms():
    atoms=Atoms(['Cu']*5, positions=[[0.,0.,0.]]*5)
    atoms.cell=[6,6,6]
    atoms.pbc=False
    
    constrained_atoms=[0,1]
    atoms.positions[0]=[2, 2, 2]    
    atoms.positions[1]=[4, 4, 4] 
    atoms.set_constraint(FixAtoms(constrained_atoms))

    return atoms, constrained_atoms


def get_constrained_periodic_atoms():
    atoms=Atoms(['Cu']*5, positions=[[0.,0.,0.]]*5)
    atoms.cell=[4,4,4]
    atoms.pbc=True
    
    constrained_atoms=[0,1]
    atoms.positions[0]=[1, 1, 1]    
    atoms.positions[1]=[3, 3, 3] 
    atoms.set_constraint(FixAtoms(constrained_atoms))

    return atoms, constrained_atoms



def assert_functioning_fixed_constraints(newatoms, template_atoms, 
                                         constrained_atoms):
    for idx in range(len(newatoms)):
        if idx in constrained_atoms:
            assert np.allclose(newatoms.positions[idx] , template_atoms.positions[idx])

    assert not np.allclose(newatoms.positions.flatten(), template_atoms.positions.flatten())            
    
def test_remake():  
    '''
    get a copy of the original atoms object
    '''
    
    atoms, constrained_atoms=get_constrained_atoms()
    sgen = Remake(atoms)
    new_atoms=sgen.get()
    
    assert np.allclose(new_atoms.positions.flatten(), atoms.positions.flatten())



def test_ConstraintHandler():
    '''
    test that ConstrainstHandler makes a correct list of constrained atoms
    '''
    
    atoms, constrained_atoms=get_constrained_atoms()
    ch=ConstraintHandler(atoms)
    assert all(ch.cindex==constrained_atoms)
    


def test_Rattler():  
    '''
    get a copy of the original atoms object
    '''
    rng = np.random.RandomState(30061)
    atoms, constrained_atoms=get_constrained_atoms()
    sgen = Rattler(atoms, intensity=0.1, rng=rng)
    new_atoms=sgen.get()
    
    assert_functioning_fixed_constraints(new_atoms, atoms, 
                                         constrained_atoms)
    

def test_randombox():
    '''
    place atoms inside of a box and minimize repulsive energy
    '''    

    atoms, constrained_atoms=get_constrained_atoms()

    rng = np.random.RandomState(30061)
    sgen = RandomBox(atoms, box=[(1., 4.),(1., 4.),(1., 4.)], rng=rng)
    new_atoms = sgen.get()

    assert_functioning_fixed_constraints(new_atoms, atoms, 
                                         constrained_atoms)



def test_BoxConstructor():
    
    '''
    test that BoxConstructor creates a box with proper volume
    and a fitting unit cell
    '''
    
    atoms, constrained_atoms=get_constrained_atoms()
    
    
    volume_fraction=0.1
    
    box, cell = BoxConstructor.get_box(atoms, volume_fraction=volume_fraction,  
                                       free_space=2.5)
    
    crads=[covalent_radii[atom.number] for atom in atoms]
    
    box_volume=1
    for i in range(len(box)):
        box_volume *= (box[i][1]-box[i][0])

    V_atoms=sum(  (4/3)*np.pi*np.array(crads)**3  )
    assert np.isclose(volume_fraction*box_volume , V_atoms)
  
    V_cell=cell[0]*cell[1]*cell[2]  
    assert(V_cell>box_volume)  
  
    for i in range(3):
        assert (cell[i] != atoms.cell[i][i])
    


def test_randombranch():
    '''
    Generate new clusters by adding atoms one by one next to
    each other.
    '''
    
    atoms, constrained_atoms=get_constrained_atoms()

    rng = np.random.RandomState(30061)
    sgen = RandomBranch(atoms, llim=1.8, ulim=2.2, rng=rng)
    
    new_atoms = sgen.get()

    assert_functioning_fixed_constraints(new_atoms, atoms, 
                                         constrained_atoms)

    for i in range(len(new_atoms)):
        for j in range(len(new_atoms)):
            
            if i == j:
                continue

            distance = np.linalg.norm(new_atoms.positions[i] -
                                      new_atoms.positions[j])
            assert 1.8 < distance



def test_randomcell():
    '''
    Test that RandomCell changes the unit cell.
    '''  
    atoms, constrained_atoms=get_constrained_atoms()

    rng = np.random.RandomState(30061)

    sgen = RandomCell(atoms, rng=rng, 
                      fixed_cell_params=[True, True, False, False, False, False])

    new_atoms = sgen.get()

    assert_functioning_fixed_constraints(new_atoms, atoms, 
                                         constrained_atoms)

    # test that xx and yy is constrained while everything else is not constrained
    for i in range(3):
        for j in range(3):
            
            if i==0 and j==0:
                assert np.isclose(new_atoms.cell[i][j] , atoms.cell[i][j])
            elif i==1 and j==1:
                assert np.isclose(new_atoms.cell[i][j] , atoms.cell[i][j])
            else:
                assert (new_atoms.cell[i][j] != atoms.cell[i][j])
    
   

def test_Random2D():
    '''
    check that z components doesnt change but that x and y do
    '''
    atoms, constrained_atoms=get_constrained_atoms()

    rng = np.random.RandomState(30061)

    sgen = Random2D(atoms, minz=4, maxz=5, rng=rng)
                     
    new_atoms = sgen.get()

    assert_functioning_fixed_constraints(new_atoms, atoms, 
                                         constrained_atoms)
    
    #check that all z-components havent changed
    for i in range(3):
        for j in range(3):
            if i==2 or j==2:
                assert np.isclose(new_atoms.cell[i][j] , atoms.cell[i][j])
            else:
                assert (new_atoms.cell[i][j] != atoms.cell[i][j])    



def test_Random2DRanges():
    '''
    check that z components doesnt change but that x, and y do
    '''
    atoms, constrained_atoms=get_constrained_atoms()

    rng = np.random.RandomState(30061)

    sgen = Random2DRanges(atoms, minz=4, maxz=5, area_range=[20, 30],
                          x_y_ratio_range=[1,2], angle_range=[90, 135], rng=rng)
                     
    new_atoms = sgen.get()

    cell=new_atoms.get_cell()
    xy_angle=cell.angles()[2]
    x_len=new_atoms.cell[0][0]    
    y_len=new_atoms.cell[1][1]
    xy_area=x_len*y_len  
    xy_ratio=y_len/x_len
    assert_functioning_fixed_constraints(new_atoms, atoms, 
                                         constrained_atoms)
    
    assert(xy_area>=20)  
    assert(xy_area<=30)  

    assert(xy_ratio>=1)  
    assert(xy_ratio<=2)

    assert(xy_angle>=90)  
    assert(xy_angle<=135)

    # all z-components unchanged
    for i in range(3):
        for j in range(3):
            if i==2 or j==2:
                assert np.isclose(new_atoms.cell[i][j] , atoms.cell[i][j])



def test_repulsion():
    
    atoms, constrained_atoms=get_constrained_atoms()
    fixed_cell_params=[False]*6
    rng = np.random.RandomState(30061)
    sgen = RandomCell(atoms, rng=rng, 
                      fixed_cell_params=fixed_cell_params)
    
    potential=RepulsivePotential(potential_type='parabola', prefactor=10)

    relaxer=AtomsRelaxer(calculator=potential)

    new_atoms = sgen.get()

    repelled_new_atoms=relaxer.run(new_atoms.copy())

    assert_functioning_fixed_constraints(repelled_new_atoms, atoms, 
                                         constrained_atoms)
    

def test_repulsion_with_stress():
    
    atoms, constrained_atoms=get_constrained_periodic_atoms()
    
    fixed_cell_params=[False]*6
    rng = np.random.RandomState(30061)
    sgen = RandomCell(atoms, rng=rng, 
                      fixed_cell_params=fixed_cell_params)
    
    potential=RepulsivePotentialWithStress(potential_type='parabola', prefactor=10)

    relaxer=AtomsAndStressRelaxer(calculator=potential,
                                  fixed_cell_params=fixed_cell_params)

    new_atoms = sgen.get()

    repelled_new_atoms=relaxer.run(new_atoms.copy())
    
    assert not np.allclose(repelled_new_atoms.cell.flatten(), new_atoms.cell.flatten())

    # add the new cell to the old atoms to recsale all fixed_atoms
    # to their corresponding place in the new cell before testing for propper
    opt_cell=repelled_new_atoms.cell.copy()
    new_atoms.set_cell(opt_cell, scale_atoms=True)

    assert_functioning_fixed_constraints(repelled_new_atoms, new_atoms, 
                                         constrained_atoms)
     

    

def test_repulsive_inside_of_box():
    atoms, constrained_atoms=get_constrained_atoms()

    rng = np.random.RandomState(23432)

    box=[(0.5, 5.5), (0.5, 5.5), (0.5, 5.5)]  

    crads=[covalent_radii[atom.number] for atom in atoms]

    covrad_inside=[True]*len(box)

    sgen=RandomBox(atoms, box=box, covrad_inside=covrad_inside,rng=rng)
    
    new_atoms=sgen.get()

    potential=RepulsivePotential(potential_type='parabola', prefactor=10)

    relaxer=AtomsInsideBoxRelaxer(calculator=potential, box=box,
                                  covrad_inside=covrad_inside, constr=sgen.constr)

    new_atoms = sgen.get()

    repelled_new_atoms=relaxer.run(new_atoms.copy())

    assert_functioning_fixed_constraints(repelled_new_atoms, new_atoms, 
                                         constrained_atoms)    


    for i in range(3):
        assert all(repelled_new_atoms.positions[i,:] - crads[0]  >= box[i][0])
        assert all(repelled_new_atoms.positions[i,:] + crads[0]  <= box[i][1])
