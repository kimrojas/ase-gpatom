""" Tests for pytest

Usage
-----
>>> python3 -m pytest
"""

import pytest


from gpatom.beacon.str_gen import (Rattler, Remake, RandomBranch)
from ase.build.surface import fcc100
from ase.calculators.emt import EMT
from ase.io import read, write
from ase.constraints import FixAtoms

import numpy as np

from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.gpfp.gp import GaussianProcess
from gpatom.gpfp.fingerprint import FingerPrint
from gpatom.gpfp.prior import ConstantPrior
from gpatom.beacon.beacon import (BEACON, SurrogateOptimizer,  
                                  InitatomsGenerator, CalculatorInterface, 
                                  Checker, LowerBound, Logger)

from gpatom.gpfp.hpfitter import HpFitInterface


import os


def get_model():

    gp_args = dict(prior=ConstantPrior(constant=0), 
                   hp={'scale': 1000, 'weight': 100, 
                       'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1},
                   use_forces=True) 

    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1.0}               

    gp=GaussianProcess(**gp_args)
    fp=FingerPrint(fp_args) 
    model=Model(gp=gp, fp=fp)  
                           
    return model


def get_logger():
    logger=Logger(output='info.txt', logoutput='log.txt')
    return logger


def get_test_atoms():
    # it is importtant that all atoms are centere with atoms.center()
    # or the tests wont succeed as BEACON aosto centers
    
    # Lowest energy structure
    atoms1 = fcc100('Au', (2, 2, 2))
    atoms1.info = {}
    atoms1.center(vacuum=6.0, axis=2)
    atoms1.center()

    rng=np.random.RandomState(451)


    # second lowest energy structure
    atoms2 = atoms1.copy()
    atoms2.rattle(0.5,seed=rng.randint(100))
    atoms2.center()

    # highest energy structure
    atoms3=atoms1.copy()
    atoms3.rattle(0.5, seed=rng.randint(200))
    atoms3.center()
    
    
    return atoms1, atoms2, atoms3







def read_last_prediction():
    with open('info.txt', 'r') as f:
        e = float(f.readlines()[-1].split()[1])
    return e


def read_last_calculation():
    with open('info.txt', 'r') as f:
        e = float(f.readlines()[-1].split()[2])
    return e



def read_info():
    with open('info.txt') as fd:
        headers = next(fd).split()
        values = next(fd).split()
        
        values=[float(i) for i in values]
        return dict(zip(headers, values))


def read_hyperparams():
    file=np.loadtxt('info.txt')
        
    prior=file[:,10]
    scale=file[:,11]
    weight=file[:,12]
    noise=file[:,13]
    noisefactor=file[:,14]
    ratio=file[:,15]

    return prior, scale, weight, noise, noisefactor, ratio    



def test_beacon_cycle_outputs():             
    atoms1, atoms2, atoms3 = get_test_atoms()

    init_atoms=[atoms1, atoms2]

    sgen = Remake(atoms3)  
    
    InitatomsGenerator(sgen)

    model=get_model()    

    logger=get_logger()

    go=BEACON(calculator=CalculatorInterface(calc=EMT),
              model=model,
              logger=logger,
              initatomsgen=InitatomsGenerator(sgen, nbest=0, nrattle=0),
              init_atoms=init_atoms,
              surropt=None,
              hp_optimizer=None,
              ndft=len(init_atoms)+1,
              nsur=1,
              write_surropt=True)

    go.run()
    
    #Check that all relevant files are being made    
    assert( os.path.isfile('info.txt') )
#    assert( os.path.isfile('forces.txt') )
    assert( os.path.isfile('log.txt') )
    assert( os.path.isfile('structures_dft.xyz') )
    assert( os.path.isfile('init_structures.xyz') )
    assert( os.path.isfile('surropt.xyz') )

    # check that the output folders have the expected lengths
    assert len(read('init_structures.xyz',':'))==2
    assert len(read('structures_dft.xyz',':'))==1
    assert len(read('surropt.xyz',':'))==1

    # check that the expected structures go into the right folders:
    assert np.allclose(read('init_structures.xyz','0').positions, atoms1.positions)
    assert np.allclose(read('init_structures.xyz','1').positions, atoms2.positions)
    assert np.allclose(read('structures_dft.xyz','0').positions, atoms3.positions)    
    assert np.allclose(read('surropt.xyz','0').positions, atoms3.positions)
    
        
    # check that the desired structure was attatched a calculator 
    #and calculated correctly
    atoms3.calc = EMT()
    eng_input=atoms3.get_potential_energy()
    forces_input=atoms3.get_forces()


    output_structure=read('structures_dft.xyz','0')
    eng_output=output_structure.get_potential_energy()
    forces_output=output_structure.get_forces()
    
    
    assert np.isclose(eng_input, eng_output)
    
    assert np.allclose(forces_input.flatten(), forces_output.flatten())  

    eng_read = read_last_calculation()
 #   forces_read = read_last_calculated_forces()
    
    assert np.isclose(eng_read, eng_output)
  #  assert np.allclose(forces_read.flatten(), forces_output.flatten(), rtol=0, atol=1e-4)  
    
    
    # make another model so test is not dependent on if model got
    # updated in the BEACON cycle
    for atoms in init_atoms:
        atoms.calc=EMT()
    model2=get_model()
    model2.add_data_points(init_atoms)
    
    
    eng_p, forces_p, unc= model2.get_predicted_properties(output_structure)
    eng_p_read=read_last_prediction()
    
 #   forces_p_read=read_last_predicted_forces()
    
    assert np.isclose(eng_p_read, eng_p)
 #   assert np.allclose(forces_p_read.flatten(), forces_p.flatten(), rtol=0, atol=1e-4)
    
    
    

def test_beacon_cycle_with_bad_structure():

    atoms1, atoms2, atoms3 = get_test_atoms()
    
    init_atoms=[atoms1, atoms2]
    
    sgen = Remake(atoms2)  
    
    rgen = Remake(atoms3)

    model=get_model()
    
    logger=get_logger()
     
    go=BEACON(calculator=CalculatorInterface(calc=EMT),
              model=model,
              logger=logger,
              initatomsgen=InitatomsGenerator(sgen, rgen=rgen),
              init_atoms=init_atoms,
              checker=Checker(dist_limit=0.1), 
              ndft=len(init_atoms)+1,
              nsur=1,
              write_surropt=True)

    go.run()
    
    assert( os.path.isfile('extras.xyz') )
    assert len(read('init_structures.xyz',':'))==2
    assert len(read('surropt.xyz',':'))==1
    assert len(read('extras.xyz',':'))==1   

    
    assert np.allclose(read('surropt.xyz','0').positions, atoms2.positions)
    assert np.allclose(read('extras.xyz','0').positions, atoms3.positions)
    





def test_beacon_cycle_with_relaxations():
    atoms1, atoms2, atoms3 = get_test_atoms()

    init_atoms=[atoms1, atoms2]
    
    atoms3.set_constraint(FixAtoms(indices=[1, 3]))
    sgen = RandomBranch(atoms3, rng=np.random.RandomState(987))

    model=get_model()    

    logger=get_logger()

    optimizer=SurrogateOptimizer(fmax=0.05, relax_steps=10, 
                                 write_surropt_trajs=True)
    

    acq=LowerBound(kappa=2)

    go=BEACON(calculator=CalculatorInterface(calc=EMT),
              model=model,
              logger=logger,
              initatomsgen=InitatomsGenerator(sgen),
              init_atoms=init_atoms,
              surropt=optimizer,
              acq=acq,
              ndft=len(init_atoms)+1,
              nsur=3,
              write_surropt=True)
    
    go.run()

    assert len(read('structures_dft.xyz',':'))==1
    assert len(read('surropt.xyz',':'))==3
    
    origpos=atoms3.positions
    output_atoms=read('structures_dft.xyz','-1')
    newpos=output_atoms.positions
    
    assert( os.path.isfile('opt_002_000.traj') )
    
    # assert same distance between constrained atoms
    assert np.allclose(newpos[3] - newpos[1],
                       origpos[3] - origpos[1])

    # assert different distance between unconstrained atoms
    assert not np.allclose(newpos[3] - newpos[0],
                           origpos[3] - origpos[0])
    
    
    #assert that the structure going into structures_dft.xyz is the one with 
    #the lowest acquisition function
    surropts=read('surropt.xyz',':')

    surropts_acq=np.zeros(3)
    for i in range(len(surropts)):  
        gp_eng_i=surropts[i].info['gp_energy']
        gp_unc_i=surropts[i].info['gp_uncertainty']    
        surropts_acq[i]=acq.get(gp_eng_i,gp_unc_i)
        
    lowst_acq_idx=np.argmin(surropts_acq)
    best_structure=surropts[lowst_acq_idx]    
    best_structure.center()
    
    saved_best_structure=read('structures_dft.xyz','-1')
    saved_best_structure.center()
    
    assert np.allclose(best_structure.positions, saved_best_structure.positions)
    


def test_structre_list_update():

    atoms1, atoms2, atoms3 = get_test_atoms()
    
    #energies of the 3 sets of atoms
    #E(atoms1)<E(atoms_2)<E(atoms3)
        
    sgen = Remake(atoms1)

    model=get_model() 
    
    logger=get_logger()
    
    init_atoms=[atoms2, atoms3]  
    
    
    initatomsgen=InitatomsGenerator(sgen, nbest=1, nrattle=0, lob=None)
    
    # assert that initatomsgen has no lob (list of best) before running
    assert initatomsgen.lob is None

    go=BEACON(calculator=CalculatorInterface(calc=EMT),
              model=model,
              logger=logger,
              initatomsgen=InitatomsGenerator(sgen, nbest=1, nrattle=0),
              init_atoms=init_atoms,
              surropt=None,
              acq=None,
              ndft=len(init_atoms)+1,        
              nsur=2,   # one for the nbest annd one for the sgen.get()   
              write_surropt=True)
    
    #assert that BEACON.initatomsgen has lob after inialization with
    # atoms2 as the best
    assert go.initatomsgen.lob is not None
    assert len(go.initatomsgen.lob)==1
    
    lob_atoms=go.initatomsgen.lob.structurelist[0]
    
    assert np.allclose(lob_atoms.positions, atoms2.positions)
    
    
    go.run()
    assert go.initatomsgen.lob is not None
    assert len(go.initatomsgen.lob)==1
    
    lob_atoms=go.initatomsgen.lob.structurelist[0]
    
    assert np.allclose(lob_atoms.positions, atoms1.positions)






def test_hpfit_intervals():                    
    
    atoms1, atoms2, atoms3 = get_test_atoms()
    
    init_atoms=[atoms1, atoms2]
    
    sgen = RandomBranch(atoms3, rng=np.random.RandomState(987))

    hp_args=dict(scale_prior=None, set_large_scale=True, large_scale_factor=5)

    hp_optimizer=HpFitInterface(**hp_args)

    model=get_model()

    logger=get_logger()

    go=BEACON(calculator=CalculatorInterface(calc=EMT),
              model=model,
              logger=logger,
              initatomsgen=InitatomsGenerator(sgen, nbest=1, nrattle=0),
              init_atoms=init_atoms,
              hp_optimizer=hp_optimizer,
              fit_interval=[5,5,5],
              surropt=None,
              acq=None,
              ndft=len(init_atoms)+4,        
              nsur=1,   
              write_surropt=False)
    
    go.run()
    

    
    
    prior, scale, weight, noise, noisefactor, ratio = read_hyperparams()
    #dct = read_info()
    #prior=dct['prior']
    #noise=dct['noise']
    #scale=dct['scale']
    #weight=dct['weight']
    #factor=dct['noisefactor']
    #ratio=dct['ratio']
    
    
    identical_scales= np.array([scale[0], scale[1], scale[2]]  )
    identical_weights= np.array([weight[0], weight[1], weight[2]] )
    identical_priors = np.array([prior[0], prior[1], prior[2]] )
    

    # check that hyper parameters are updated as expected
    # we need some tolerance because numbers are reported to
    # info.txt with rounding
    assert  all(identical_scales==scale[0])
    assert not scale[0]==scale[3]
       
    assert  all(identical_weights==weight[0])
    assert not weight[0]==weight[3]
    
    assert  all(identical_priors==prior[0])
    assert not prior[0]==prior[3]
    
    assert  np.allclose(noise , ratio*weight, rtol=0, atol=1e-4)
   
    assert all(noisefactor==noisefactor[0]) 
    
    assert all(ratio==ratio[0]) 
   
    # chech that hyperparameters output agree with the model
    
    model_scale=model.gp.hp['scale']
    model_weight=model.gp.hp['weight']
    model_prior=model.gp.prior.constant
    model_noise=model.gp.hp['noise']
    model_ratio=model.gp.hp['ratio']
    model_factor=model.gp.hp['noisefactor']
    
    
    assert (model_scale - scale[-1])<1e-4
    assert (model_weight - weight[-1])<1e-4
    assert (model_prior - prior[-1])<1e-4
    assert (model_noise - noise[-1])<1e-4
    assert (model_ratio - ratio[-1])<1e-6
    assert (model_factor - noisefactor[-1])<1e-6
    
    
    #asser that the length scale is inially set by initialze parameters
    hp_optimizer.initialize_parameters(model.gp)
    set_scale=model.gp.hp['scale']

    assert  (scale[0] - set_scale)<1e-4



def produce_structures_from_BEACON():
    
    atoms1, atoms2, atoms3 = get_test_atoms()
    
    sgen = Rattler(atoms1.copy(), rng=np.random.RandomState(123))
   
    init_atoms=[atoms1, atoms2]

    optimizer=SurrogateOptimizer(fmax=0.05, relax_steps=10, 
                                 write_surropt_trajs=False)
    
    
    model=get_model()
    logger=get_logger()    
        
    go=BEACON(calculator=CalculatorInterface(calc=EMT),
              model=model,
              logger=logger,
              initatomsgen=InitatomsGenerator(sgen, nbest=0, nrattle=0),
              init_atoms=init_atoms,
              surropt=optimizer,
              acq=LowerBound(kappa=2),
              ndft=len(init_atoms)+3,        
              nsur=2,    
              write_surropt=False)

    go.run()
     
    structures=read('structures_dft.xyz',':')
    
    return structures
    
def test_BEACON_consistency():
    
    strs1=produce_structures_from_BEACON()
    
    strs2=produce_structures_from_BEACON()
    
    for i in range(len(strs1)):
        assert np.allclose(strs1[i].positions, strs2[i].positions)    
    

