#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  9 10:23:15 2023

@author: casper
"""

import numpy as np
from ase.build import fcc111
from ase.calculators.emt import EMT

from gpatom.gpfp.gp import GaussianProcess
#from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.gpfp.fingerprint import RadialAngularFP
from gpatom.gpfp.hpfitter import HyperparameterFitter, GPPrefactorFitter, PriorFitter, HpFitInterface
from gpatom.gpfp.prior import ConstantPrior
from gpatom.gpfp.hpfitter import PriorDistributionLogNormal

from gpatom.gpfp.hp_interface import GSSHpFitInterfaceSmall

def setup_gaussian_process():
    fp_class = RadialAngularFP

    gp_args = dict(prior=ConstantPrior(constant=0), 
                   hp={'scale': 2, 'weight': 1, 
                       'noise': 1e-5, 'ratio': 0.001, 'noisefactor': 1},
                   use_forces=True) 

    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1.0}


    # Create slab:
    slab = fcc111('Ag', size=(2, 2, 2))
    slab.center(vacuum=4.0, axis=2)
    slab.rattle(0.05)
    slab.calc = EMT()

    def new_atoms(rattle_seed=None):
        atoms = slab.copy()
        atoms.rattle(0.05, seed=rattle_seed)
        atoms.calc = EMT()
        return atoms

    frames = [new_atoms(rattle_seed=i) for i in range(3)]


    X = [fp_class(atoms=atoms, **fp_args) for atoms in frames]

    energyforces = [[atoms.get_potential_energy()] +
                    list(atoms.get_forces().flatten())
                    for atoms in frames]
    Y = np.array(energyforces).reshape(-1)
    
    
    gp = GaussianProcess(**gp_args)
    gp.train(X, Y)
    
    return gp, X, Y


    

def check_vicinity_prior(gp, X, Y):
    
    optimum_prior_const=gp.prior.constant
    
    logP_afterfit = HyperparameterFitter.logP(gp) 
    
    gp.set_hyperparams({'prior': optimum_prior_const * 0.9})
    gp.train(X,Y)    
    logP_toosmallprior = HyperparameterFitter.logP(gp)  
    
    gp.set_hyperparams({'prior': optimum_prior_const * 1.1})
    gp.train(X,Y)    
    logP_toobigprior = HyperparameterFitter.logP(gp)  

    # set back to optimum
    gp.prior.constant=optimum_prior_const
    gp.train(X,Y)

    return (logP_afterfit > logP_toobigprior) and  (logP_afterfit > logP_toosmallprior)





def check_vicinity_weight(gp, X, Y):
    
    optimum_weight=gp.hp['weight']
    
    logP_afterfit= HyperparameterFitter.logP(gp) 
    
    gp.set_hyperparams({'weight': optimum_weight*0.9})
    gp.set_hyperparams({'noise': gp.hp['ratio']*optimum_weight*0.9})

    gp.train(X,Y)    
    logP_toosmallweight = HyperparameterFitter.logP(gp) 

    gp.set_hyperparams({'weight': optimum_weight*1.1})
    gp.set_hyperparams({'noise': gp.hp['ratio']*optimum_weight*1.1})
    
    gp.train(X,Y) 
    logP_toobigweight = HyperparameterFitter.logP(gp) 
    
    # set back to optimal
    gp.set_hyperparams({'weight': optimum_weight})
    gp.set_hyperparams({'noise': gp.hp['ratio']*optimum_weight})
    gp.train(X,Y)
    
    return (logP_afterfit > logP_toobigweight) and  (logP_afterfit > logP_toosmallweight)




def check_vicinity_scale(gp, X, Y):
    
    optimum_scale=gp.hp['scale']
    
    logP_afterfit = HyperparameterFitter.logP(gp) 

    gp.set_hyperparams({'scale': optimum_scale * 0.9})
    gp.train(X,Y)    
    logP_toosmallscale = HyperparameterFitter.logP(gp)  

    gp.set_hyperparams({'scale': optimum_scale * 1.1})
    gp.train(X,Y)   
    logP_toobigscale = HyperparameterFitter.logP(gp)  
    
    # set back to optimum
    gp.set_hyperparams({'scale': optimum_scale})
    gp.train(X,Y)
    
    return (logP_afterfit > logP_toobigscale) and  (logP_afterfit > logP_toosmallscale)






def test_fit_prior():
    # test that the optimized prior consatnt is a maximum on log-likelihood space       
    gp, X, Y = setup_gaussian_process()
    
    gp=PriorFitter.fit(gp)
    
    is_optimum_prior = check_vicinity_prior(gp, X, Y)
    
    assert is_optimum_prior




def test_fit_weight():
    # test that the optimized prefactor is a maximum on log-likelihood space
    
    gp, X, Y = setup_gaussian_process()

    gp=GPPrefactorFitter.fit(gp)
    
    is_optimum_weight = check_vicinity_weight(gp, X, Y)
    
    assert is_optimum_weight


def test_fit_scale():
    # test that the optimized length scale is a maximum on log-likelihood space    

    gp, X, Y = setup_gaussian_process()


    gp, results=HyperparameterFitter.fit(gp, params_to_fit=['scale'], 
                                         fit_weight=False, fit_prior=False, 
                                         pd=None)
    
    is_optimum_scale = check_vicinity_scale(gp, X, Y)
    
    assert is_optimum_scale




def test_HpFitInterFace():
    
    gp, X, Y = setup_gaussian_process()
    
    hp_optimizer=HpFitInterface(scale_prior=None, set_large_scale=False, 
                                large_scale_factor=None)

    hp_optimizer.fit(gp, fit_scale=True, fit_weight=True, fit_prior=True)




    is_optimum_scale = check_vicinity_scale(gp, X, Y)    
    assert is_optimum_scale

    is_optimum_prior = check_vicinity_prior(gp, X, Y)
    assert is_optimum_prior
    

    is_optimum_weight=check_vicinity_weight(gp, X, Y)   
    assert is_optimum_weight


    

    








        
def test_pd_log_normal():
    # test that the lognormal prior has its optimum at exp(3)
    
    pd_log_normal = PriorDistributionLogNormal(loc=3, width=0.1)
    
    log_scales=[0. , 0.5, 1. , 1.5, 2. , 2.5, 3. , 3.5, 4. ]
    
    results=[pd_log_normal.get( np.exp(scale) ) for scale in log_scales]
 
    max_idx=np.argmax(results)
    
    assert log_scales[max_idx]==3
    

    


def test_scale_fit_with_prior():    
    # check that the optimized length scale is the same as that of the
    # lognormal prior for small lognormal widths
    
    gp, X, Y = setup_gaussian_process()

    pd_log_normal = PriorDistributionLogNormal(loc=3, width=0.0001)
    

    gp, results=HyperparameterFitter.fit(gp, params_to_fit=['scale'], 
                                         fit_weight=False, fit_prior=False, 
                                         pd=pd_log_normal)
 
    assert np.isclose(gp.hp['scale'] , np.exp(3)) 
    



def test_scale_fit_with_prior_in_HpFitInterface():    
    # check that the optimized length scale is the same as that of the
    # lognormal prior for small lognormal widths
    
    gp1, X, Y = setup_gaussian_process()
    hp_optimizer1=HpFitInterface(scale_prior=None,
                                set_large_scale=False, large_scale_factor=None)
    
    hp_optimizer1.fit(gp1, fit_scale=True, fit_weight=False, fit_prior=False)

    scale1=gp1.hp['scale']
    
    pd_log_normal = PriorDistributionLogNormal(loc=np.log(scale1*3), width=0.0001)

    gp2, X, Y = setup_gaussian_process()
    hp_optimizer2=HpFitInterface(scale_prior=pd_log_normal,
                                set_large_scale=False, large_scale_factor=None)

    hp_optimizer2.fit(gp2, fit_scale=True, fit_weight=False, fit_prior=False)
    
    scale2=gp2.hp['scale']
    
    assert scale2 > scale1
    
    
    
    

def test_GSSHpFitInterFaceSmall():
    
    gp, X, Y = setup_gaussian_process()
    
    
    hp_optimizer=GSSHpFitInterfaceSmall(E_train=False, bounds=None, 
                    scale_prior=None, noise_prior=None, prior_constant_correction=False,
                    set_large_scale=False, large_scale_factor=None)


    hp_optimizer.fit(gp, fit_scale=True, fit_weight=True, fit_prior=True)
 

    is_optimum_scale = check_vicinity_scale(gp, X, Y)    
    assert is_optimum_scale

    is_optimum_prior = check_vicinity_prior(gp, X, Y)
    assert is_optimum_prior
    
    is_optimum_weight=check_vicinity_weight(gp, X, Y)   
    assert is_optimum_weight


    
    

    
    
    