import pytest
from ase import Atoms
from gpatom.beacon.str_gen import RandomBranch
from gpatom.fractional_beacon.gpfp.gp import ICEGaussianProcess, GhostGaussianProcess
from gpatom.fractional_beacon.gpfp.fingerprint import PartialFingerPrint
from gpatom.gpfp.prior import ConstantPrior, RepulsivePotential, RepulsivePotentialWithStress, CalculatorPrior
from ase.calculators.emt import EMT
import numpy as np

def get_trained_gp(sgen, fp, gp, use_forces):

    trainingfeatures = []
    trainingtargets = []
    ninit = 2
    for i in range(ninit):
        atoms = sgen.get()
        atoms.calc = EMT()
        atoms.get_potential_energy()
        trainingfeatures.append(fp.get(atoms))
        
        if use_forces:
            trainingtargets.append([atoms.get_potential_energy()] +
                                   list(atoms.get_forces().flatten()))
        else: 
            trainingtargets.append([atoms.get_potential_energy()]) 
        
    trainingtargets = np.array(trainingtargets).flatten()
    
    gp.train(trainingfeatures, trainingtargets)

    return gp


def get_forces(gp, fp, testatoms, testfractions):
    
    index = 0
    dofindex = 0
    dx = 0.0001
    
    testfp = fp.get(atoms=testatoms, fractions=testfractions)
    ef0, V = gp.predict(testfp)

    testatoms.positions[index, dofindex] += dx
    testfp = fp.get(atoms=testatoms, fractions=testfractions)
    ef1, V = gp.predict(testfp)

    numerical_force = (ef1[0] - ef0[0]) / dx
    analytical_force = ef0[1 + 4 * (index) + dofindex]
    
    return numerical_force,  analytical_force
    
    
def get_fraction_gradients(gp, fp, testatoms, testfractions):
    
    index = 0
    dofindex = 3
    dx = 0.0001

    testfp = fp.get(atoms=testatoms, fractions=testfractions)
    ef0, V = gp.predict(testfp)

    testfractions[index] += dx
    testfp = fp.get(atoms=testatoms, fractions=testfractions)
    ef1, V = gp.predict(testfp)

    numerical_gradient = (ef1[0] - ef0[0]) / dx
    analytical_gradient = ef0[1 + 4 * (index) + dofindex]
    
    return numerical_gradient, analytical_gradient


def get_numeric_stress(atoms, fractions, gp, fp, d=1e-6, voigt=False):
    stress = np.zeros((3, 3), dtype=float)

    cell = atoms.cell.copy()
    V = atoms.get_volume()
    for i in range(3):
        x = np.eye(3)
        x[i, i] += d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, fractions=fractions)
        predict, unc = gp.predict(atoms_fp)
        eplus=predict[0]

        x[i, i] -= 2 * d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, fractions=fractions)
        predict, unc = gp.predict(atoms_fp)
        eminus=predict[0]

        stress[i, i] = (eplus - eminus) / (2 * d * V)
        x[i, i] += d

        j = i - 2
        x[i, j] = d
        x[j, i] = d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, fractions=fractions)
        predict, unc = gp.predict(atoms_fp)
        eplus=predict[0]


        x[i, j] = -d
        x[j, i] = -d
        atoms.set_cell(np.dot(cell, x), scale_atoms=True)
        atoms_fp=fp.get(atoms, fractions=fractions)
        predict, unc = gp.predict(atoms_fp)
        eminus=predict[0]

        stress[i, j] = (eplus - eminus) / (4 * d * V)
        stress[j, i] = stress[i, j]
    atoms.set_cell(cell, scale_atoms=True)

    if voigt:
        return stress.flat[[0, 4, 8, 5, 2, 1]]
    else:
        return stress    


def get_stresses(gp, fp, testatoms, testfractions):
    dx=0.0001
    
    testfp = fp.get(atoms=testatoms, fractions=testfractions)
    analytical_stress = gp.predict_stress(testfp)
    
    numerical_stress=get_numeric_stress(testatoms, testfractions, gp, fp, d=dx, voigt=True)
    
    return numerical_stress, analytical_stress





@pytest.mark.parametrize('prior', [ConstantPrior(constant=0),
                                   CalculatorPrior(RepulsivePotential(potential_type='parabola', prefactor=10), constant=0),
                                   CalculatorPrior(RepulsivePotentialWithStress(potential_type='parabola', prefactor=10), constant=0)])
@pytest.mark.parametrize('use_forces', [True, False] )  
def test_predict_forces_and_gradients_ICE(use_forces, prior):
    '''
    Test that the predicted forces and fraction gradients are
    correct by comparing to finite-difference energies.
    '''
    atoms = Atoms(['Au'] * 2 + ['Cu'] * 2 + ['Ag'], positions=[[0., 0., 0.]] * 5)
    atoms.pbc = True
    atoms.center(vacuum=6.0)
    rng = np.random.RandomState(9)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)

    fp=PartialFingerPrint(fractional_elements=['Au', 'Cu'], Ghost_mode=False,
                          calc_coord_gradients=True, calc_frac_gradients=True, 
                          calc_strain=True)

    gp_args=dict(prior=prior,
                 hp={'scale': 1000, 'weight': 100, 
                     'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                 use_forces=use_forces)
        
    gp = ICEGaussianProcess(**gp_args)  

    gp=get_trained_gp(sgen, fp, gp, use_forces)

    testatoms = sgen.get()
    testfractions = rng.random(len(atoms))
    numerical_force, analytical_force = get_forces(gp, fp, testatoms, testfractions)
    assert abs(numerical_force - analytical_force)/abs(numerical_force) < 0.001

    # Test that predicted gradient of a fraction is correct:
    testatoms = sgen.get()
    testfractions = rng.random(len(atoms))
    numerical_gradient, analytical_gradient = get_fraction_gradients(gp, fp, testatoms, testfractions)
    assert abs(numerical_gradient - analytical_gradient)/abs(numerical_gradient) < 0.001
    
    # test that the predicted stresses are correct unless prior is oftype RepulsivePotential
    if isinstance(prior, CalculatorPrior) and isinstance(prior.calculator, RepulsivePotential):
        return 
    
    testatoms = sgen.get()
    testfractions = rng.random(len(atoms))
    numerical_stress, analytical_stress = get_stresses(gp, fp, testatoms, testfractions)
    assert all( abs(numerical_stress - analytical_stress)/abs(numerical_stress) < 0.001 )

    

@pytest.mark.parametrize('prior', [ConstantPrior(constant=0),
                                   CalculatorPrior(RepulsivePotential(potential_type='parabola', prefactor=10), constant=0),
                                   CalculatorPrior(RepulsivePotentialWithStress(potential_type='parabola', prefactor=10), constant=0)])
@pytest.mark.parametrize('use_forces', [True, False] )  
def test_predict_forces_and_gradients_Ghost(use_forces, prior):

    atoms = Atoms(['Au'] * 2 + ['Cu'] * 2, positions=[[0., 0., 0.]] * 4)
    atoms.pbc = True
    atoms.center(vacuum=6.0)
    rng = np.random.RandomState(9)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)


    atoms2 = Atoms(['Au'] * 4 + ['Cu'] * 2, positions=[[0., 0., 0.]] * 6)
    atoms2.pbc = True
    atoms2.center(vacuum=6.0)    
    rng2 = np.random.RandomState(8)
    sgen2 = RandomBranch(atoms2, llim=2.0, ulim=2.2, rng=rng2)


    fp=PartialFingerPrint(fractional_elements=['Au'], Ghost_mode=True,
                          calc_coord_gradients=True, calc_frac_gradients=True,
                          calc_strain=True)

    gp_args=dict(prior=ConstantPrior(constant=0), 
                 hp={'scale': 1000, 'weight': 100, 
                     'noise': 1e-3, 'ratio': 0.001, 'noisefactor': 1}, 
                 use_forces=use_forces)
        
    gp = GhostGaussianProcess(n_ghost=np.array([2]), **gp_args)  

    gp=get_trained_gp(sgen, fp, gp, use_forces)


    testatoms = sgen2.get()
    testfractions = rng.random(len(atoms2))
    numerical_force, analytical_force = get_forces(gp, fp, testatoms, testfractions)
    assert abs(numerical_force - analytical_force)/abs(numerical_force) < 0.001

    # Test that predicted gradient of a fraction is correct:
    testatoms = sgen2.get()
    testfractions = rng.random(len(atoms2))
    numerical_gradient, analytical_gradient = get_fraction_gradients(gp, fp, testatoms, testfractions)
    assert abs(numerical_gradient - analytical_gradient)/abs(numerical_force) < 0.001

    # test that the predicted stresses are correct unless prior is oftype RepulsivePotential
    if isinstance(prior, CalculatorPrior) and isinstance(prior.calculator, RepulsivePotential):
        return 
    testatoms = sgen2.get()
    testfractions = rng.random(len(atoms2))
    numerical_stress, analytical_stress = get_stresses(gp, fp, testatoms, testfractions)
    assert all( abs(numerical_stress - analytical_stress)/abs(numerical_stress) < 0.001   )


