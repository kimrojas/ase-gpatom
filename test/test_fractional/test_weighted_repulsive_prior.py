#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 14:47:39 2023

@author: casper
"""


import numpy as np
from ase.build import fcc111
from ase.calculators.emt import EMT


from gpatom.gpfp.calculator import PriorCalculator
from gpatom.fractional_beacon.gpfp.prior import WeightedRepulsivePotential, WeightedRepulsivePotentialWithStress
from ase.data import covalent_radii, atomic_numbers

def new_atoms(rattle_seed=None):     
    atoms = fcc111('Ag', size=(2, 2, 2))
    atoms.center(vacuum=4.0, axis=2)
    atoms.rattle(0.05, seed=rattle_seed)
    atoms.calc = EMT()
    return atoms


def test_weighted_repulsive_prior():
    
    r_atom= covalent_radii[atomic_numbers['Ag']]
    
    potential=WeightedRepulsivePotential(potential_type='parabola',
                                         prefactor=10, rc=r_atom*1)
    
    calc=PriorCalculator(potential, calculate_stress=False)
    
    atoms=new_atoms(rattle_seed=10)
   
    atoms.calc=calc 
   
    rng=np.random.RandomState(243) 
   
    fractions=rng.random(len(atoms))
    
    atoms.fractions=fractions
    
    atoms.get_potential_energy() 
    
    analytical_forces=calc.results['forces']
    
    numerical_forces = atoms.calc.calculate_numerical_forces(atoms)    

    print("analytical forces:", analytical_forces)
    print("numerical forces:", numerical_forces)     

    assert np.allclose(analytical_forces, numerical_forces, atol=5e-3, rtol=1e-3)
    
    
def test_weighted_repulsive_prior_with_stress():
        
    r_atom= covalent_radii[atomic_numbers['Ag']]
        
    potential=WeightedRepulsivePotentialWithStress(potential_type='parabola',
                                                   prefactor=10, rc=r_atom*1)
        
    calc=PriorCalculator(potential, calculate_stress=True)
        
    atoms=new_atoms(rattle_seed=10)
       
    atoms.calc=calc 
       
    rng=np.random.RandomState(243) 
       
    fractions=rng.random(len(atoms))
        
    atoms.fractions=fractions
        
    atoms.get_potential_energy() 
    
    analytical_forces=calc.results['forces']
        
    numerical_forces = atoms.calc.calculate_numerical_forces(atoms)    

    print("analytical forces:", analytical_forces)
    print("numerical forces:", numerical_forces)     

    assert np.allclose(analytical_forces, numerical_forces, atol=5e-3, rtol=1e-3)