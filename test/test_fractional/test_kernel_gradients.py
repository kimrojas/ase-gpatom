import pytest
from gpatom.fractional_beacon.gpfp.fingerprint import PartialFP, PartialRadAngFP
from gpatom.fractional_beacon.gpfp.kerneltypes import ICESquaredExp
from ase import Atoms
import numpy as np
from gpatom.beacon.str_gen import RandomBranch



@pytest.mark.parametrize('fp_class', [PartialFP, PartialRadAngFP])
def test_dk_drm(fp_class):
    atoms = Atoms(['Cu'] * 4 + ['Au'] * 2, positions=[[0., 0., 0.]] * 6)
    natoms = len(atoms)
    atoms.center(vacuum=6.0)

    kerneltype = ICESquaredExp(scale=100, weight=100)

    fpparams = dict()

    rng = np.random.RandomState(33)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)

    atoms1 = sgen.get()
    atoms2 = sgen.get()

    fractions1 = rng.random(natoms)
    fractions2 = rng.random(natoms)

    fp1 = fp_class(atoms=atoms1, fractions=fractions1, **fpparams)
    fp2 = fp_class(atoms=atoms2, fractions=fractions2, **fpparams)

    dx = 0.0001
    atomindex = 4
    fractions3 = fractions1.copy()
    fractions3[atomindex] += dx
    fp3 = fp_class(atoms=atoms1.copy(), fractions=fractions3, **fpparams)

    kernelA = kerneltype.kernel(fp1, fp2)
    kernelB = kerneltype.kernel(fp3, fp2)

    numerical = ((kernelB - kernelA) / dx).flatten()

    analytical = kerneltype.kernel_gradient_frac(fp1, fp2)[atomindex]

    print(numerical)
    print()
    print(analytical)

    assert np.allclose(numerical, analytical, rtol=1e-3, atol=0.0)

    # Does exactly the same as test_kernel_gradient

    dx = 0.0001
    atomindex = 4
    dofindex = 1
    atoms3 = atoms1.copy()
    atoms3.positions[atomindex, dofindex] += dx
    fp3 = fp_class(atoms=atoms3, fractions=fractions1.copy(), **fpparams)

    kernelA = kerneltype.kernel(fp1, fp2)
    kernelB = kerneltype.kernel(fp3, fp2)

    numerical = ((kernelB - kernelA) / dx).flatten()

    analytical = kerneltype.kernel_gradient(fp1, fp2)[atomindex, dofindex]

    print(numerical)
    print()
    print(analytical)

    assert np.allclose(numerical, analytical, atol=0.0, rtol=0.01)





@pytest.mark.parametrize('fp_class', [PartialFP, PartialRadAngFP])
def test_dk_drm_dq(fp_class):
    atoms = Atoms(['Cu'] * 4 + ['Au'] * 12, positions=[[0., 0., 0.]] * 16)
    natoms = len(atoms)
    atoms.center(vacuum=6.0)

    kerneltype = ICESquaredExp(scale=1000)

    fpparams = dict()

    rng = np.random.RandomState(33)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng)

    atoms1 = sgen.get()
    atoms2 = sgen.get()

    fractions1 = rng.random(natoms)
    fractions2 = rng.random(natoms)

    fp1 = fp_class(atoms=atoms1, fractions=fractions1, **fpparams)
    fp2 = fp_class(atoms=atoms2, fractions=fractions2, **fpparams)

    dx = 0.0001
    atomindex = 4
    fractions3 = fractions1.copy()
    fractions3[atomindex] += dx
    fp3 = fp_class(atoms=atoms1.copy(), fractions=fractions3, **fpparams)

    gradientA = kerneltype.kernel_gradient(fp2, fp1)
    gradientB = kerneltype.kernel_gradient(fp2, fp3)

    numerical = ((gradientB - gradientA) / dx).flatten()

    analytical = kerneltype.dkernelgradient_dq(fp1, fp2)[atomindex].flatten()

    print(numerical)
    print()
    print(analytical)

    assert np.allclose(numerical, analytical, atol=0.0, rtol=0.01)


