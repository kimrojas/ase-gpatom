from gpatom.beacon.str_gen import RandomBranch

from ase.calculators.emt import EMT
from ase.constraints import FixAtoms

from ase.io import read
import numpy as np

from ase import Atoms
from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.fractional_beacon.gpfp.gp import GhostGaussianProcess
from gpatom.gpfp.prior import ConstantPrior
from gpatom.fractional_beacon.ghostbeacon import GhostSurrogateOptimizer
from gpatom.fractional_beacon.gpfp.fingerprint import PartialFingerPrint

import os


def test_ghost_surrogate_optimization():
    '''
    Test that the GhostSurrogateOptimizer relaxes atoms as intended.
    Notice that the SLSQP optimizer from python scipy used in Ghost
    has a habit of taking long steps which may lead to momentary
    wallks uphill and momentary breaking of constraints. 
    hence these things will not be tested.
    '''
        
    atoms = Atoms(['Cu'] * 4 + ['Ni'] * 2 + ['Au'] * 2, positions=[[0., 0., 0.]] * 8)
    atoms.cell=[12,12,12]
    atoms.pbc = False
    atoms.center()

    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, 
                        rng=np.random.RandomState(99)) 

    atoms_g = Atoms(['Cu'] * 6 + ['Ni'] * 3 + ['Au'] * 2, positions=[[0., 0., 0.]] * 11)
    atoms_g.cell=[12,12,12]
    atoms_g.pbc = False
    atoms_g.center()

    sgen_g = RandomBranch(atoms_g, llim=2.0, ulim=2.2, 
                          rng=np.random.RandomState(43)) 

    atoms_g.positions[0,:]=[7.5,6,6]
    atoms_g.positions[7,:]=[4.5,6,6]
    
    position_constrained_atoms=[0,7]
    fraction_constrained_atoms=[0,1]
    
    atoms_g.set_constraint(FixAtoms(position_constrained_atoms))

    training_atoms=[]
    for i in range(5):
        atoms_i=sgen.get()
        atoms_i.calc=EMT()
        training_atoms.append(atoms_i)
        
    fractional_elements=['Cu','Ni']
    n_real=np.array([4,2])
    n_ghost=np.array([2,1])
    lower_lim=0.2
    
    surropt=GhostSurrogateOptimizer(fmax=0.0001,
                                    relax_steps=20,  
                                    post_rounding_steps=10,
                                    lower_lim=lower_lim,
                                    n_real=n_real,
                                    write_surropt_trajs=True,
                                    write_surropt_fracs=True,
                                    randomtype='uniform',
                                    frac_cindex=fraction_constrained_atoms,
                                    fractional_elements=fractional_elements,
                                    fractions_rng=np.random.RandomState(284))
    
    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    fp=PartialFingerPrint(fp_args=fp_args, Ghost_mode=True, fractional_elements=fractional_elements)
    
    gp_args=dict(prior=ConstantPrior(constant=0), 
                 hp={'scale': 1000}, use_forces=True)

    gp=GhostGaussianProcess(n_ghost=n_ghost, **gp_args)

    model=Model(gp=gp, fp=fp)
    
    model.add_data_points(training_atoms)

    test_atoms=sgen_g.get()

    opt_atoms, success = surropt.relax(test_atoms, model, file_identifier='ghost')
    
    # check that files are written
    assert( os.path.isfile('opt_ghost.txt') )
    assert( os.path.isfile('opt_ghost.xyz') )
    
    trajectory=read('opt_ghost.xyz', ':')
    
    len_preround=0
    for image in trajectory:
        if len(image)==len(atoms_g):
            len_preround+=1
    
    len_postround=len(trajectory)-len_preround
    
    # fractions are stored in opt_ice.xyz initial charges
    fractions_preround=np.zeros( (len_preround,len(atoms_g)) )    
    for i in range(len_preround):
        fractions_preround[i,:]=trajectory[i].get_initial_charges()

    fractions_postround=np.zeros(  (len_postround,len(atoms)) )
    for i in range(len_postround):
        fractions_postround[i,:]=trajectory[len_preround+i].get_initial_charges()
    
    
    # check that position constrained atoms havent moved
    first_frame=trajectory[0]
    last_frame=trajectory[-1]
    
    
    assert np.allclose(first_frame.get_positions()[0], last_frame.get_positions()[0])
    assert np.allclose(first_frame.get_positions()[7], last_frame.get_positions()[4])    
        
    # check that fraction constrained atoms didnt change fraction
    assert np.allclose(fractions_preround[:,0], np.ones(len_preround))
    assert np.allclose(fractions_preround[:,1], np.ones(len_preround))
    assert np.allclose(fractions_postround[:,0], np.ones(len_postround))
    assert np.allclose(fractions_postround[:,1], np.ones(len_postround))

    #check that non-fractional atoms didnt change fractions
    assert np.allclose(fractions_preround[:,9], np.ones(len_preround))
    assert np.allclose(fractions_preround[:,10], np.ones(len_preround))
    assert np.allclose(fractions_postround[:,6], np.ones(len_postround))
    assert np.allclose(fractions_postround[:,7], np.ones(len_postround))


    # check that inequality constraints were satisfies at start
    
    expected_sums_preround=n_real+n_ghost*lower_lim
    expected_sums_preround = np.append(expected_sums_preround, 2)
    sums_preround=np.array([sum(fractions_preround[0,0:6]) , sum(fractions_preround[0,6:9]), sum(fractions_preround[0,9::]) ])
    
    assert np.allclose( sums_preround, expected_sums_preround )
    assert np.all( fractions_preround[0,:] <=1 )
    assert np.all( fractions_preround[0,0:9] >=lower_lim )
    assert np.allclose( fractions_preround[0,9::], np.ones(2))
    
    # check that inequality constraints were satisfies at end
    
    expected_sums_postround=np.array([4,2,2])
    sums_postround=np.array([sum(fractions_postround[0,0:4]) , sum(fractions_postround[0,4:6]), sum(fractions_postround[0,6::]) ])
    
    assert np.allclose( sums_postround, expected_sums_postround)   
    assert np.all( fractions_postround[-1,:] <=1 )
    assert np.all( fractions_postround[-1,:] >=0 )

    # check that the end energy is lower than the start energy    
    gp.set_n_ghost([2,1])
    fp_start=model.fp.get(atoms=trajectory[0], fractions=fractions_preround[0,:])    
    prediction_start, unc_start=model.gp.predict(fp_start)

    gp.set_n_ghost([0,0])    
    fp_end=model.fp.get(atoms=trajectory[-1], fractions=fractions_postround[-1,:])    
    prediction_end, unc_end=model.gp.predict(fp_end)

    assert(prediction_end[0] < prediction_start[0])

