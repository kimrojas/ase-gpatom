#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 23:49:53 2023

@author: casper
"""
import numpy as np
from ase import Atoms
from gpatom.fractional_beacon.ghostbeacon import GhostHandler
from gpatom.beacon.str_gen import RandomBranch

def test_GhostHandler():
    
    atoms = Atoms(['Au'] * 5 + ['Cu'] * 3+ ['Ni']*2, positions=[[0., 0., 0.]] * 10)
    atoms.pbc = False
    atoms.center(vacuum=6.0)
    
    fractional_elements=['Au', 'Cu']
    
    sgen=RandomBranch(atoms, rng=np.random.RandomState(7680))
    
    atoms0=sgen.get()
    
    fmask=np.array([1,1,1,1,1,1,1,1,0,0],dtype=bool)
    
    constrained_atoms=[0,1]
    
    n_ghost=np.array([2,1])
    
    fractions=np.array([1, 1, 0.1, 0.5, 0.4, 1, 0.4, 0.6])
    
    selected_atoms=GhostHandler.generate_ghosts_constrained(atoms0, fractions, n_ghost, constrained_atoms, fmask, fractional_elements)    

    assert all( selected_atoms == np.array( [1, 1, 0, 1, 0, 1, 0, 1, 1, 1], dtype=bool) )

    processed_atoms=GhostHandler.construct_processed_atoms_object(atoms0, selected_atoms)
    
    pos0=atoms0.get_positions()
    
    pos_pros=processed_atoms.get_positions()
    
    assert len(processed_atoms)==7
    assert np.allclose(pos_pros[0,:], pos0[0,:])
    assert np.allclose(pos_pros[1,:], pos0[1,:])  
    assert np.allclose(pos_pros[2,:], pos0[3,:])
    assert np.allclose(pos_pros[3,:], pos0[5,:])  
    assert np.allclose(pos_pros[4,:], pos0[7,:])
    assert np.allclose(pos_pros[5,:], pos0[8,:])  
    assert np.allclose(pos_pros[6,:], pos0[9,:])
    
    symbols=processed_atoms.get_chemical_symbols()
    assert symbols==['Au','Au','Au','Cu','Cu','Ni','Ni']
