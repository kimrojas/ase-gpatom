import pytest
from ase.build import bulk
from gpatom.fractional_beacon.gpfp.fingerprint import (PartialFP,
                                                       PartialRadAngFP)
import numpy as np


def fractional_derivatives(atoms, fractions, fp_class, kwargs):
    index = 3
    dx = 0.000001
    
    atoms1 = atoms.copy()
    v1 = fp_class(atoms=atoms1, fractions=fractions, **kwargs).vector

    atoms2 = atoms.copy()
    fractions2 = fractions.copy()
    fractions2[index] += dx
    v2 = fp_class(atoms=atoms2, fractions=fractions2, **kwargs).vector

    drho_numerical = (v2 - v1) / dx


    fp1 = fp_class(atoms=atoms, fractions=fractions, **kwargs)
    drho_analytical = fp1.reduce_frac_gradients()[index].flatten()

    assert np.allclose(drho_numerical, drho_analytical, rtol=1e-2, atol=1e-2)    
    


def coordinate_derivatives(atoms, fractions, fp_class, kwargs):
    index = 3
    dx = 0.000001
    
    atoms1 = atoms.copy()
    v1 = fp_class(atoms=atoms1, fractions=fractions, **kwargs).vector
    
    # Coordinate gradients:
    atoms2 = atoms.copy()
    atoms2[index].position += [dx, 0., 0.]
    v2 = fp_class(atoms=atoms2, fractions=fractions, **kwargs).vector

    drho_numerical = (v2 - v1) / dx

    
    fp1 = fp_class(atoms=atoms, fractions=fractions, **kwargs)
    drho_analytical = fp1.reduce_coord_gradients()[index, :, 0]

    assert np.allclose(drho_numerical, drho_analytical, rtol=1e-2, atol=1e-2)


from ase.visualize import view
def strain_derivatives(atoms, fractions, fp_class, kwargs):
    i1, i2 = 0, 0
    dx = 0.000001
    
    atoms1 = atoms.copy()
    v1 = fp_class(atoms=atoms1, fractions=fractions, calc_strain=True,
                  **kwargs).vector

    atoms2 = atoms.copy()
    x = np.eye(3)
    x[i1, i2] += dx
    x[i2, i1] += dx
    atoms2.set_cell(np.dot(atoms2.cell, x), scale_atoms=True)
    v2 = fp_class(atoms=atoms2, fractions=fractions, calc_strain=True,
                  **kwargs).vector
    
    drho_numerical = (v2 - v1) / (2 * dx)
    
    fp1 = fp_class(atoms=atoms, fractions=fractions, calc_strain=True, **kwargs)
    drho_analytical = fp1.reduce_strain()[:, i1, i2]
    
     
    assert np.allclose(drho_numerical, drho_analytical, rtol=1e-2, atol=1e-2)



@pytest.mark.parametrize('fp_class', [PartialFP, PartialRadAngFP])
def test_fp_derivative_ICE(fp_class):
    
    '''
    tests that gradient of fingerprint is correct with respect to
    fractions and coordinates
    '''

    atoms = bulk('NaCl', 'rocksalt', a=4.6)
    atoms = atoms.repeat((2, 2, 3))
    atoms.rattle(0.001, seed=13)
    atoms.pbc = True
    fmask = np.ones(len(atoms), dtype=bool)
    kwargs = dict(fmask=fmask, elements=['Cl', 'Na'], Ghost_mode=False)

    rng = np.random.RandomState(400)

    fractions = [rng.random() for i in range(len(atoms))]

    fractional_derivatives(atoms, fractions, fp_class, kwargs)

    coordinate_derivatives(atoms, fractions, fp_class, kwargs)

    strain_derivatives(atoms, fractions, fp_class, kwargs)



@pytest.mark.parametrize('fp_class', [PartialFP, PartialRadAngFP])
def test_fp_derivative_Ghost(fp_class):
    
    '''
    tests that gradient of fingerprint is correct with respect to
    fractions and coordinates
    '''

    atoms = bulk('Cu', 'fcc', a=3.6)
    atoms = atoms.repeat((2, 2, 3))
    atoms.rattle(0.001, seed=13)
    atoms.pbc = True
    fmask = np.ones(len(atoms), dtype=bool)
    kwargs = dict(fmask=fmask, elements=['Cu'], Ghost_mode=True)

    rng = np.random.RandomState(400)

    fractions = [rng.random() for i in range(len(atoms))]

    fractional_derivatives(atoms, fractions, fp_class, kwargs)

    coordinate_derivatives(atoms, fractions, fp_class, kwargs)
    
    strain_derivatives(atoms, fractions, fp_class, kwargs)

    
    