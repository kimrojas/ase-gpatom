#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 00:42:58 2023

@author: casper
"""

from gpatom.fractional_beacon.icebeacon import FractionConverter
from ase import Atoms
import numpy as np


def test_FractionConverter():
    
    atoms = Atoms(['Au'] * 3 + ['Cu'] * 3 + 2 * ['Ag'], positions=[[0., 0., 0.]] * 8)
    atoms.pbc = False
    atoms.center(vacuum=6.0)
    
    constrained_fractions=[0,5]
    
    fractional_elements=['Au', 'Cu']
    
    
    # assert that fmask, and n_0 production is correct
    fmask, n_0 = FractionConverter.set_fractional_atoms(atoms, fractional_elements)
    
    assert  all(fmask==np.array([1,1,1,1,1,1,0,0],dtype=bool))
    assert  n_0==3
    
    
    
    # assert that atoms2fraction conversion is correct
    fractions = FractionConverter.atoms2fractions(atoms, fractional_elements)
    
    assert all(fractions==[1,1,1,0,0,0,0,0])
    
    
    # assert that fractions2atoms conversion is correct
    fractions=np.array([1, 0.8, 0.3, 0.7, 0.2, 0, 0, 0])
    
    atoms = FractionConverter.fractions2atoms(fractions, atoms, fractional_elements, constrained_fractions)
        
    correct_symbols=['Au','Au','Cu','Au','Cu','Cu','Ag','Ag']
    
    found_symbols=[]
    for atom in atoms:
        found_symbols.append(atom.symbol)
        
    assert (found_symbols==correct_symbols)

