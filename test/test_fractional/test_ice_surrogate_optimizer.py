from gpatom.beacon.str_gen import RandomBranch

from ase.calculators.emt import EMT
from ase.constraints import FixAtoms

from ase.io import read
import numpy as np

from ase import Atoms
from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.fractional_beacon.gpfp.gp import ICEGaussianProcess
from gpatom.gpfp.prior import ConstantPrior
from gpatom.fractional_beacon.icebeacon import ICESurrogateOptimizer
from gpatom.fractional_beacon.gpfp.fingerprint import PartialFingerPrint

import os


def test_ice_surrogate_optimization():
    '''
    Test that the ICESurrogateOptimizer relaxes atoms as intended.
    Notice that the SLSQP optimizer from python scipy used in ICE
    has a habit of taking long steps which may lead to momentary
    wallks uphill and momentary breaking of constraints. 
    hence these things will not be tested.
    '''
        
    atoms = Atoms(['Au'] * 3 + ['Cu'] * 3 + 2*['Ag'], positions=[[0., 0., 0.]] * 8)
    atoms.cell=[12,12,12]
    atoms.pbc = False
    atoms.center()

    atoms.positions[0,:]=[7.5,6,6]
    atoms.positions[7,:]=[4.5,6,6]
    
    position_constrained_atoms=[0,7]
    fraction_constrained_atoms=[0,5]
    
    atoms.set_constraint(FixAtoms(position_constrained_atoms))

    rng = np.random.RandomState(9)
    sgen = RandomBranch(atoms, llim=2.0, ulim=2.2, rng=rng) 

    training_atoms=[]
    for i in range(5):
        atoms_i=sgen.get()
        atoms_i.calc=EMT()
        training_atoms.append(atoms_i)
        
    
    surropt=ICESurrogateOptimizer(fmax=0.05, 
                                  relax_steps=50,  
                                  post_rounding_steps=10,
                                  write_surropt_trajs=True,
                                  write_surropt_fracs=True,
                                  randomtype='uniform',
                                  frac_cindex=fraction_constrained_atoms,
                                  fractional_elements=['Au','Cu'],
                                  fractions_rng=np.random.RandomState(284))
    
    
    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    fp=PartialFingerPrint(fp_args, Ghost_mode=False, fractional_elements=['Au', 'Cu'])
    
    gp_args=dict(prior=ConstantPrior(constant=0), 
                 hp={'scale': 1000}, use_forces=True)

    gp=ICEGaussianProcess(**gp_args)

    model=Model(gp=gp, fp=fp)
    
    model.add_data_points(training_atoms)


    test_atoms=sgen.get()

    opt_atoms, success = surropt.relax(test_atoms, model, file_identifier='ice')

    
    # check that files are written
    assert( os.path.isfile('opt_ice.txt') )
    assert( os.path.isfile('opt_ice.xyz') )
    
    trajectory=read('opt_ice.xyz', ':')
    
    # fractions are stored in opt_ice.xyz initial charges
    fractions=np.zeros((len(trajectory),len(atoms)))
    for i in range(len(trajectory)):
        fractions[i,:]=trajectory[i].get_initial_charges()
    
    
    # check that position constrained atoms havent moved
    first_frame=trajectory[0]
    last_frame=trajectory[-1]
    for atom_index in position_constrained_atoms:
        coord_start=first_frame.get_positions()[atom_index]
        coord_end=last_frame.get_positions()[atom_index]
        
        assert np.allclose(coord_start, coord_end)
        
    # check that fraction constrained atoms didnt change fraction
    assert np.allclose(fractions[:,0], np.ones(len(trajectory)))
    assert np.allclose(fractions[:,5], np.zeros(len(trajectory)))

    #check that non-fractional atoms didnt change fractions
    assert np.allclose(fractions[:,6], np.zeros(len(trajectory))) 
    assert np.allclose(fractions[:,7], np.zeros(len(trajectory)))

    # check that inequality constraints were satisfies at start
    assert np.isclose( sum(fractions[0,:]), 3 ) 
    assert np.all( fractions[0,:] <=1 )
    assert np.all( fractions[0,:] >=0 )
    
    # check that inequality constraints were satisfies at end
    assert np.isclose( sum(fractions[-1,:]), 3 ) 
    assert np.all( fractions[-1,:] <=1 )
    assert np.all( fractions[-1,:] >=0 )


    # check that the end energy is lower than the start energy    
    fp_start=model.fp.get(atoms=trajectory[0], fractions=fractions[0,:])
    prediction_start, unc_start=model.gp.predict(fp_start)
    
    fp_end=model.fp.get(atoms=trajectory[-1], fractions=fractions[-1,:])
    prediction_end, unc_end=model.gp.predict(fp_end)

    assert(prediction_end[0] < prediction_start[0])

