from ase.build import bulk
from gpatom.fractional_beacon.gpfp.fingerprint import PartialRadAngFP
from gpatom.gpfp.fingerprint import RadialAngularFP
import numpy as np



def basetest_fingerprint(atoms=None, Ghost_mode=None, elements=None):
    elements = np.sort(list(set(atoms.symbols)))
    fractions = [(1.0 if atoms.symbols[i] == elements[0] else 0.0)
                  for i in range(len(atoms))]

    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    
    fp = PartialRadAngFP(atoms=atoms, fractions=fractions, 
                         Ghost_mode=Ghost_mode, **fp_args)
    v1 = fp.vector

    fp = RadialAngularFP(atoms=atoms, **fp_args)
    v2 = fp.vector

    assert np.allclose(v1, v2, atol=1e-6)
    

def test_fingerprint_nonperiodic_ICE():
    atoms = bulk('NaCl', 'rocksalt', a=4.6)
    atoms = atoms.repeat((2, 2, 3))
    atoms.pbc = False
    basetest_fingerprint(atoms=atoms, Ghost_mode=False, elements=['Na','Cl'])
    
def test_fingerprint_periodic_ICE():
    atoms = bulk('NaCl', 'rocksalt', a=4.6)
    atoms.pbc = True
    basetest_fingerprint(atoms=atoms, Ghost_mode=False, elements=['Na','Cl'])


def test_fingerprint_nonperiodic_Ghost():
    atoms = bulk('Cu', 'fcc', a=3.6)
    atoms = atoms.repeat((2, 2, 3))
    atoms.pbc = False
    basetest_fingerprint(atoms=atoms, Ghost_mode=True, elements=['Cu'])
    
def test_fingerprint_periodic_Ghost():
    atoms = bulk('Cu', 'fcc', a=3.6)
    atoms.pbc = True
    basetest_fingerprint(atoms=atoms, Ghost_mode=True, elements=['Cu'])

    
    

