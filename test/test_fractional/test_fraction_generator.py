import pytest
from gpatom.fractional_beacon.icebeacon import ICERandomFractionGenerator
from gpatom.fractional_beacon.ghostbeacon import GhostRandomFractionGenerator
from ase import Atoms
import numpy as np



@pytest.mark.parametrize('randomtype', ['drs', 'uniform', 'whole_atoms'])
def test_fraction_generator_ICE(randomtype):
    
    atoms = Atoms(['Au'] * 3 + ['Cu'] * 3 + 2 * ['Ag'], positions=[[0., 0., 0.]] * 8)
    atoms.pbc = False
    atoms.center(vacuum=6.0)

    rfg=ICERandomFractionGenerator(frac_cindex=[0,5],
                                   randomtype=randomtype,
                                   fractional_elements=['Au', 'Cu'],
                                   rng=np.random.RandomState(34388))
    
    fractions=rfg.get(atoms)
    
    assert(np.isclose(sum(fractions),3))
    
    assert all(fractions<=1)
    assert all(fractions>=0)
    
    assert(np.isclose(fractions[0],1))
    assert(np.isclose(fractions[5],0))
    
    assert(np.isclose(fractions[6],0))
    assert(np.isclose(fractions[7],0))
    
    if randomtype=='uniform':
        assert np.allclose( fractions[1:5], 2/4*np.ones(4))
        
    if randomtype=='whole_atoms':
        assert np.allclose(fractions[0:3],np.ones(3))
        assert np.allclose(fractions[3:6],np.zeros(3))
        
    
    
    
@pytest.mark.parametrize('randomtype', ['drs', 'uniform', 'whole_atoms'])    
def test_fraction_generator_Ghost(randomtype):
    atoms = Atoms(['Au'] * 5 + ['Cu'] * 3 + 4 * ['Ag'], positions=[[0., 0., 0.]] * 12)

    atoms.pbc = False
    atoms.center(vacuum=6.0)

    n_real=np.array([4,2])
    n_ghost=np.array([5,4])-n_real

    
    lower_lim=0.2

    constrained_frqactions=[0,1]

    rfg=GhostRandomFractionGenerator(frac_cindex=constrained_frqactions,
                                     n_real=n_real,
                                     lower_lim=lower_lim,
                                     randomtype=randomtype,
                                     fractional_elements=['Au', 'Ag'],
                                     rng=np.random.RandomState(34388))
    
    fractions=rfg.get(atoms)
    
    assert all(fractions<=1)
    assert all(fractions>=0)

    assert(np.isclose(fractions[0],1))
    assert(np.isclose(fractions[1],1))
    
    assert(np.isclose(fractions[5],1))
    assert(np.isclose(fractions[6],1))
    assert(np.isclose(fractions[7],1))


    sum_f=n_real+n_ghost*lower_lim
    
    if randomtype=='drs':
        assert(np.isclose(sum(fractions[0:5]), sum_f[0] ))
        assert(np.isclose(sum(fractions[8::]), sum_f[1] ))

        assert all(fractions[2:5]<=1)
        assert all(fractions[2:5]>=lower_lim)
        
        assert all(fractions[8::]<=1)
        assert all(fractions[8::]>=lower_lim)

    
    if randomtype=='uniform':
        left_over=[sum_f[0]-len(constrained_frqactions), sum_f[1]]
        print(left_over)
        assert(np.isclose(sum(fractions[0:5]), sum_f[0] ))
        assert(np.isclose(sum(fractions[8::]), sum_f[1] ))
        assert np.allclose( fractions[2:5], left_over[0]/3*np.ones(3))
        assert np.allclose( fractions[8::], left_over[1]/4*np.ones(4))

        assert all(fractions[2:5]<=1)
        assert all(fractions[2:5]>=lower_lim)
        
    if randomtype=='whole_atoms':
        assert(np.isclose(sum(fractions), len(atoms)) )
 