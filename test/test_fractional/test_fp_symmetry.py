from gpatom.fractional_beacon.gpfp.fingerprint import PartialRadAngFP
from gpatom.fractional_beacon.gpfp.kerneltypes import ICEDistance
from ase.cluster import Icosahedron
import numpy as np


def asses_symmetry_operations(slab=None, fractions=None, elements=None, Ghost_mode=None):
    
    rng = np.random.RandomState(53453)
    
    fp = PartialRadAngFP

    fp0 = fp(atoms=slab.copy(), fractions=fractions, 
             elements=elements, Ghost_mode=Ghost_mode)
    vec0 = fp0.vector

    # TRANSLATION
    slab.positions += rng.random(3)
    fp_trans = fp(atoms=slab.copy(), fractions=fractions, 
                  elements=elements, Ghost_mode=Ghost_mode)

    # ROTATION
    slab.rotate(a=180. * rng.random(), v=rng.random(3), center='COM')
    fp_rot = fp(atoms=slab.copy(), fractions=fractions, 
                elements=elements, Ghost_mode=Ghost_mode)

    # PERMUTATION (swap both positions and fractions)
    tmp = fractions[3].copy()
    fractions[3] = fractions[4]
    fractions[4] = tmp.copy()
    tmp = slab[3].position.copy()
    slab[3].position = slab[4].position
    slab[4].position = tmp
    fp_perm = fp(atoms=slab.copy(), fractions=fractions, 
                 elements=elements, Ghost_mode=Ghost_mode)

    for fp1 in [fp_trans, fp_rot, fp_perm]:

        vec1 = fp1.vector

        d = ICEDistance.distance(fp0, fp1)

        assert(fp0.atoms.positions != fp1.atoms.positions).all()
        assert(d < 1e-4)
        assert(np.allclose(vec0, vec1, atol=1e-8))




def test_fingerprint_symmetries_ICE():

    """
    Test that fingerprint behaves as wanted with
    symmetry operations:
    - Translation
    - Rotation
    - Permutation
    """
    rng = np.random.RandomState(15072021)

    # Create slab:
    slab = Icosahedron('Cu', noshells=2, latticeconstant=3.6)
    slab[10].symbol = 'Au'
    slab[11].symbol = 'Au'
    slab.center(vacuum=4.0)

    fractions = rng.random(len(slab))

    asses_symmetry_operations(slab=slab, fractions=fractions, 
                        elements=['Cu','Au'], Ghost_mode=False)


    
def test_fingerprint_symmetries_Ghost():

    rng = np.random.RandomState(15072021)

    # Create slab:
    slab = Icosahedron('Cu', noshells=2, latticeconstant=3.6)
    slab.center(vacuum=4.0)

    fractions = rng.random(len(slab))

    asses_symmetry_operations(slab=slab, fractions=fractions, 
                        elements=['Cu'], Ghost_mode=True)