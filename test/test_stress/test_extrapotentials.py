from gpatom.gpfp.prior import RepulsivePotentialWithStress, SmallAreaPunisher, TallStructurePunisher
from gpatom.gpfp.calculator import PriorCalculator

from ase import Atoms
import numpy as np
import pytest

def create_atoms():
    atoms = Atoms(['Au']*4, positions=[[0., 0., 0.]]*4)
    atoms.pbc = True

    atoms.positions[1] = [1.3, 1.6, 2.0]
    atoms.positions[2] = [2.2, 1.0, 0.5]
    atoms.positions[3] = [2.5, 0.1, 2.5]
    
    atoms.cell=[[0.0, 2.04, 2.04], [2.04, 0.0, 2.04], [2.04, 2.04, 0.0]]
    
    atoms.center(vacuum=1.5)
    atoms.center()
    
    atoms.center(vacuum=20.0, axis=2)
    return atoms




@pytest.mark.parametrize('extrapotential', [None,
                                            SmallAreaPunisher(start=30),
                                            TallStructurePunisher(zlow=9.0, zhigh=11.0)])
def test_extrapotential_in_prior(extrapotential):
      
    potential=RepulsivePotentialWithStress(potential_type='parabola', prefactor=10, extrapotential=extrapotential)
    
    calc=PriorCalculator(potential, calculate_stress=True)
    
    atoms = create_atoms()
   
    atoms.calc=calc 
   
    atoms.get_potential_energy() 
    
    analytical_forces=calc.results['forces']
    
    numerical_forces = atoms.calc.calculate_numerical_forces(atoms)    

    analytical_stress=calc.results['stress']
    
    numerical_stress = atoms.calc.calculate_numerical_stress(atoms) 

    print("analytical forces:", analytical_forces)
    print("numerical forces:", numerical_forces)     


    print("analytical stress:", analytical_stress)
    print("numerical stress:", numerical_stress)  


    assert np.allclose(analytical_forces, numerical_forces, atol=5e-3, rtol=1e-3)
    
    assert np.allclose(analytical_stress, numerical_stress, atol=5e-3, rtol=1e-3)



