from ase import Atoms
from ase.visualize import view
from gpatom.gpfp.fingerprint import RadialAngularFP
from matplotlib import pyplot as plt
import numpy as np

def test_drho_dc():
    '''
    Test that the fingerprint strain is correct by comparing
    to numerical finite-difference result.
    '''

    dx = 0.00001

    atoms = Atoms(['Au']*3, positions=[[0., 0., 0.]]*3)
    atoms.pbc = True
    atoms.positions[1] = [1.3, 1.6, 2.0]
    atoms.positions[2] = [2.2, 1.0, 0.5]
    atoms[2].symbol = 'Cu'
    atoms.center(vacuum=0.5)

    # view(atoms)

    i1, i2 = 0, 0

    fp0 = RadialAngularFP(atoms=atoms, calc_strain=True)
    v0 = fp0.vector

    atoms1 = atoms.copy()
    x = np.eye(3)
    x[i1, i2] += dx
    x[i2, i1] += dx
    atoms1.set_cell(np.dot(atoms1.cell, x), scale_atoms=True)
    fp1 = RadialAngularFP(atoms=atoms1)
    v1 = fp1.vector

    numerical = (v1 - v0) / (2 * dx)

    analytical = fp0.reduce_strain()[:, i1, i2]

    # plt.plot(numerical)
    # plt.plot(analytical)
    # # - analytical, label="Difference")
    # plt.legend()
    # plt.show()
    
    assert np.allclose(analytical, numerical, atol=5e-3, rtol=1e-3)
