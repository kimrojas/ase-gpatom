from ase import Atoms
from ase.visualize import view
from gpatom.gpfp.fingerprint import RadialAngularFP
from gpatom.gpfp.kerneltypes import SquaredExp
from matplotlib import pyplot as plt
import numpy as np
import time

from gpatom.beacon.str_gen import RandomCell, RandomBranch


def print_matrix(matrix):
    print()
    for row in matrix:
        for val in row:
            print('{:12.06f}'.format(val), end='')
        print()
    print()


def get_atoms(natoms=4, nelem=1, pbc=False):

    elems = ['Cu', 'Au', 'Ag', 'Pt']  # elements with EMT potential

    atoms = Atoms([elems[0]] * natoms, positions=[[0., 0., 0.]] * natoms)

    for j in range(natoms):
        atoms.symbols[j] = elems[j % nelem]

    if pbc:
        atoms.pbc = True
        density = 20.0  # cubic angstroms per atom
        atoms.center(vacuum=(0.5 * (natoms * density)**(1/3.)))
        new_atoms = RandomCell(atoms, rng=np.random.RandomState(715517)).get()

    else:
        atoms.center(vacuum=6.0)
        atoms.pbc = False
        new_atoms = RandomBranch(atoms, llim=2.0, ulim=2.2,
                                 rng=np.random.RandomState(715517)).get()

    return new_atoms
    

def numerical_gradient(function, dx=0.00001, **params):

    result_init = function(**params, dx=0.0)
    result_final = function(**params, dx=dx)

    return (result_final - result_init) / dx





def get_originatoms(atoms):
    atoms2 = atoms.copy()
    atoms2.rattle(0.2)
    return atoms2


def get_differentiated_kernelgradient(atoms, atom_index, dof_index, cell_indices, kerneltype, fp_params, dx):

    atoms1 = atoms.copy()
    x = np.eye(3)
    i1, i2 = cell_indices
    x[i1, i2] += dx
    x[i2, i1] += dx
    atoms1.set_cell(np.dot(atoms1.cell, x), scale_atoms=True)
    fp_test = RadialAngularFP(atoms=atoms1, **fp_params)

    fp_origin = RadialAngularFP(atoms=get_originatoms(atoms), calc_strain=True, **fp_params)
    
    return kerneltype.kernel_gradient(fp_origin, fp_test)[atom_index, dof_index]


def get_analytical(atoms, atom_index, dof_index, cell_indices, kerneltype, fp_params):
    atoms1 = atoms.copy()
    fp_test = RadialAngularFP(atoms=atoms1, calc_strain=True, **fp_params)

    fp_origin = RadialAngularFP(atoms=get_originatoms(atoms), calc_strain=True, **fp_params)

    return kerneltype.dkernelgradient_dc(fp_origin, fp_test)[atom_index, dof_index]


def test_dk_dc():
    '''
    Test that the analytical kernel gradients with respect to the unit cell parameters
    match with the numerical ones.
    '''

    atoms = get_atoms(natoms=4, nelem=1, pbc=True)

    kernel = SquaredExp(weight=1.0, scale=100.)

    atom_index = 0
    dof_index = 0

    params = dict(atoms=atoms, atom_index=atom_index, dof_index=dof_index,
                  kerneltype=kernel, fp_params={})

    numerical = np.zeros((3, 3))
    t0 = time.time()
    for i1 in range(3):
        for i2 in range(3):

            params.update({'cell_indices': (i1, i2)})

            numerical[i1, i2] = 0.5 * numerical_gradient(get_differentiated_kernelgradient, **params)

    print('Numerical:')
    print_matrix(numerical)
    print('Time consumed:', (time.time() - t0), 'seconds\n')

    t0 = time.time()
    analytical = get_analytical(**params)

    print()
    print('Analytical:')
    print_matrix(analytical)
    print('Time consumed:', (time.time() - t0), 'seconds\n')

    assert np.allclose(numerical, analytical, atol=1e-2, rtol=1e-3)
