#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 22:13:36 2023

@author: casper
"""

import numpy as np
from gpatom.hyperspatial_beacon.gpfp.fingerprint import CoordExtender
from ase import Atoms


def test_get_dimensions():
    
    rng=np.random.RandomState(3242)
        
    dims_none=CoordExtender.get_dimension(None)
    dims_4=CoordExtender.get_dimension(rng.rand(3,1))
    dims_5=CoordExtender.get_dimension(rng.rand(3,2))
    
    assert dims_none==3
    assert dims_4==4
    assert dims_5==5
    
def test_get_extended_atoms():

    rng=np.random.RandomState(121)    

    atoms = Atoms(['Au'] * 3 , positions=[[1., 2., 1.], [1,1,2], [2,1,1]])
    atoms.cell=[10,10,10]
    atoms.pbc = False
    atoms.center()
    
    extended_coords=CoordExtender.get_extended_coords(atoms, extra_coords=None)
    
    # test extended coords 3D
    assert np.shape(extended_coords)==(len(atoms),3)
    assert np.allclose(extended_coords,atoms.positions) 

    ec4=rng.rand(len(atoms),1)
    extended_coords=CoordExtender.get_extended_coords(atoms, extra_coords=ec4)
    
    # test extended coords 4D
    assert np.shape(extended_coords)==(len(atoms),4)
    assert np.allclose(extended_coords[:,0:3],atoms.positions) 
    assert np.allclose(extended_coords[:,3::],ec4)     

    ec5=rng.rand(len(atoms),2)
    extended_coords=CoordExtender.get_extended_coords(atoms, extra_coords=ec5)
    
    # test extended coords 5D
    assert np.shape(extended_coords)==(len(atoms),5)
    assert np.allclose(extended_coords[:,0:3],atoms.positions) 
    assert np.allclose(extended_coords[:,3::],ec5) 