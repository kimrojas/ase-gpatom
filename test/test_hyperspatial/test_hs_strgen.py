#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 19:13:57 2023

@author: casper
"""

import pytest
import numpy as np
from ase import Atoms
from ase.constraints import FixAtoms
from gpatom.hyperspatial_beacon.str_gen import HighDimRandomBox, HighDimRandomCell, HighDimBoxConstructor
from gpatom.hyperspatial_beacon.gpfp.prior import HighDimRepulsivePotential, HighDimPotentialWithStress
from gpatom.hyperspatial_beacon.str_gen import HighDimAtomsRelaxer, HighDimAtomsInsideBoxRelaxer, HighDimAtomsAndStressRelaxer
from ase.data import covalent_radii

def get_constrained_atoms():
    atoms=Atoms(['Cu']*5, positions=[[0.,0.,0.]]*5)
    atoms.cell=[10,10,10]
    atoms.pbc=False
    
    constrained_atoms=[0,1]
    atoms.positions[0]=[4, 5, 5]    
    atoms.positions[1]=[6, 5, 5] 
    atoms.set_constraint(FixAtoms(constrained_atoms))

    return atoms, constrained_atoms



def get_constrained_periodic_atoms():
    atoms=Atoms(['Cu']*5, positions=[[0.,0.,0.]]*5)
    atoms.cell=[5,5,5]
    atoms.pbc=True
    
    constrained_atoms=[0,1]
    atoms.positions[0]=[1.5, 3, 3]    
    atoms.positions[1]=[3.5, 3, 3] 
    atoms.set_constraint(FixAtoms(constrained_atoms))

    return atoms, constrained_atoms

def assert_functioning_fixed_constraints(newatoms, template_atoms, 
                                         constrained_atoms, world_center):

    for idx in range(len(newatoms)):
        if idx in constrained_atoms:
            assert np.allclose(newatoms.positions[idx] , template_atoms.positions[idx])
            assert np.allclose(newatoms.extra_coords[idx,:] , world_center)
    
    assert not np.allclose(newatoms.positions[idx] , template_atoms.positions[idx])
    assert all( abs(newatoms.extra_coords[idx,:] - world_center) > 0 )


def assert_functioning_box_constraints(newatoms, box, crads):

    for i in range(3):
        assert all(newatoms.positions[i,:] - crads[0]  >= box[i][0])
        assert all(newatoms.positions[i,:] + crads[0]  <= box[i][1])
    
    
    for i in range(len(box)-3):
        assert all(newatoms.extra_coords[:,i] - crads[0] >= box[i+3][0] )
        assert all(newatoms.extra_coords[:,i] + crads[0] <= box[i+3][1] )



def assert_functioning_relaxer(newatoms, template_atoms):
    
    assert not np.allclose(newatoms.positions.flatten(), template_atoms.positions.flatten())
    assert not np.allclose(newatoms.extra_coords.flatten(), template_atoms.extra_coords.flatten())


@pytest.mark.parametrize('dimensions', [4,5])
@pytest.mark.parametrize('inside', [False, True])  
def test_HighDimRandomBox(inside, dimensions): 

    if inside:
        covrad_inside=[True]*dimensions
    else:
        covrad_inside=[False]*dimensions
    
    atoms, constrained_atoms = get_constrained_atoms()
    
    rng = np.random.RandomState(2473)
    
    box=[(2., 8.)]*dimensions  
    
    sgen=HighDimRandomBox(atoms.copy(), box=box, covrad_inside=covrad_inside, rng=rng)
    
    world_center=sgen.world_center[3::]
    
    newatoms=sgen.get()
    
    assert hasattr(newatoms, 'extra_coords')
    assert np.shape(newatoms.extra_coords) == (len(newatoms),dimensions-3)  
    
    assert_functioning_fixed_constraints(newatoms, atoms, 
                                         constrained_atoms, world_center)


@pytest.mark.parametrize('dimensions', [1,2,3,4,5])
def test_HighDimBoxConstructor(dimensions): 

    atoms, constrained_atoms = get_constrained_atoms()
    
    
    volume_fraction=0.2
    
    box, cell = HighDimBoxConstructor.get_box(atoms, volume_fraction=volume_fraction, 
                                              dims=dimensions, free_space=2.5)
    
    box_volume=1
    for i in range(dimensions):
        box_volume *= (box[i][1]-box[i][0])

    crads=np.array([covalent_radii[atom.number] for atom in atoms])

    if dimensions==1:
        V_atoms=sum(2*crads)
    elif dimensions==2:
        V_atoms=sum(np.pi*(crads**2) )
    elif dimensions==3:
        V_atoms=sum(  (4/3)*np.pi*(crads**3)  )
    elif dimensions==4:
        V_atoms=sum(  (1/2)*(np.pi**2)*(crads**4)  )
    elif dimensions==5:
        V_atoms=sum(  (8/15)*(np.pi**2)*(crads**5)  )
    
    assert np.isclose(volume_fraction*box_volume , V_atoms)
    
    
    for i in range(3):
        assert (cell[i] != atoms.cell[i][i])

@pytest.mark.parametrize('dimensions', [4,5]) 
def test_HighDimRandomCell(dimensions):
    
    atoms, constrained_atoms = get_constrained_periodic_atoms()
    
    rng = np.random.RandomState(2473)
    
    ce=2.5
    
    extra_size=[ce]*(dimensions-3)
    world_center=[ce/2]*dimensions
    world_center=np.array(world_center)
    
    sgen=HighDimRandomCell(atoms=atoms, extra_size=extra_size,
                           world_center=world_center, rng=rng)
    
    world_center=sgen.world_center[3::]
    
    newatoms=sgen.get()
    
    
    
    assert hasattr(newatoms, 'extra_coords')
    assert np.shape(newatoms.extra_coords) == (len(newatoms),dimensions-3)  
    
    assert not np.allclose(newatoms.cell.flatten(), atoms.cell.flatten())
    
    assert_functioning_fixed_constraints(newatoms, atoms, 
                                         constrained_atoms, world_center)

    
@pytest.mark.parametrize('dimensions', [4,5])
def test_HighDimAtomsRelaxer(dimensions):
    
    atoms, constrained_atoms = get_constrained_atoms()

    covrad_inside=[True]*dimensions

    rng = np.random.RandomState(23432)

    box=[(2, 8)]*dimensions

    sgen=HighDimRandomBox(atoms.copy(), box=box, covrad_inside=covrad_inside, rng=rng)
    
    newatoms=sgen.get()
    
    potential=HighDimRepulsivePotential(prefactor=10, potential_type='parabola')
    
    relaxer=HighDimAtomsRelaxer(potential, dims=dimensions, constr=sgen.constr, 
                                world_center=sgen.world_center)

    newatoms_copy=newatoms.copy()
        
    repelled_newatoms, repelled_extra_coords=relaxer.run(newatoms_copy, newatoms.extra_coords)

    repelled_newatoms.extra_coords=repelled_extra_coords
    
    world_center=sgen.world_center[3::]
    
    assert_functioning_fixed_constraints(repelled_newatoms, newatoms, 
                                         constrained_atoms, world_center)
    
    assert_functioning_relaxer(repelled_newatoms, newatoms)
    
    
@pytest.mark.parametrize('dimensions', [4,5])
def test_HighDimAtomsInsideBoxRelaxer(dimensions):
    atoms, constrained_atoms = get_constrained_atoms()
    
    covrad_inside=[True]*dimensions
    
    rng = np.random.RandomState(97909)
    
    box=[(2, 8)]*dimensions

    sgen=HighDimRandomBox(atoms.copy(), box=box, covrad_inside=covrad_inside, rng=rng)
    
    box=sgen.box
    
    newatoms=sgen.get()
    
    potential=HighDimRepulsivePotential(prefactor=10, potential_type='parabola')
    
    relaxer=HighDimAtomsInsideBoxRelaxer(potential, box=box, dims=dimensions, constr=sgen.constr, 
                                         world_center=sgen.world_center, covrad_inside=sgen.covrad_inside)
    
    newatoms_copy=newatoms.copy()

    repelled_newatoms, repelled_extra_coords=relaxer.run(newatoms_copy, newatoms.extra_coords)
    
    repelled_newatoms.extra_coords=repelled_extra_coords
    
    repelled_newatoms.extra_coords=repelled_extra_coords
    
    world_center=sgen.world_center[3::]
    
    assert_functioning_fixed_constraints(repelled_newatoms, newatoms, 
                                         constrained_atoms, world_center)
    
    assert_functioning_relaxer(repelled_newatoms, newatoms)
    
    crads=[covalent_radii[atom.number] for atom in atoms]
    assert_functioning_box_constraints(newatoms, box, crads)

@pytest.mark.parametrize('dimensions', [4,5])
def test_HighDimAtomsAndStressRelaxer(dimensions):
    
    atoms, constrained_atoms = get_constrained_periodic_atoms()

    rng = np.random.RandomState(23432)

    ce=5    
    extra_size=[ce]*(dimensions-3)
    world_center=[ce/2]*dimensions
    world_center=np.array(world_center)
    
    sgen=HighDimRandomCell(atoms=atoms.copy(), extra_size=extra_size,
                           world_center=world_center, rng=rng)
    
    newatoms=sgen.get()

    potential=HighDimPotentialWithStress(prefactor=10, potential_type='parabola')
    
    relaxer=HighDimAtomsAndStressRelaxer(potential, dims=dimensions, constr=sgen.constr, 
                                         world_center=sgen.world_center)

    newatoms_copy=newatoms.copy()
        
    repelled_newatoms, repelled_extra_coords=relaxer.run(newatoms_copy, newatoms.extra_coords)

    repelled_newatoms.extra_coords=repelled_extra_coords
    
    world_center=sgen.world_center[3::]
    
    assert not np.allclose(repelled_newatoms.cell.flatten(), newatoms.cell.flatten())
    
    # add the new cell to the old atoms to recsale all fixed_atoms
    # to their corresponding place in the new cell before testing for propper
    opt_cell=repelled_newatoms.cell.copy()
    newatoms.set_cell(opt_cell, scale_atoms=True)
    
    assert_functioning_relaxer(repelled_newatoms, newatoms)
    
    assert_functioning_fixed_constraints(repelled_newatoms, newatoms, 
                                         constrained_atoms, world_center)
        
    