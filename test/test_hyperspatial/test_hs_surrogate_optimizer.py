#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 19:12:33 2023

@author: casper
"""

import pytest

from gpatom.beacon.str_gen import RandomBox, AtomsRelaxer
from gpatom.gpfp.prior import RepulsivePotential


from gpatom.hyperspatial_beacon.str_gen import HighDimRandomBox, HighDimAtomsRelaxer

from ase.calculators.emt import EMT
from ase.constraints import FixAtoms

from ase.io import read
import numpy as np

from ase import Atoms
from gpatom.gpfp.atoms_gp_interface import Model
from gpatom.hyperspatial_beacon.gpfp.gp import HighDimGaussianProcess
from gpatom.hyperspatial_beacon.gpfp.prior import HighDimConstantPrior
from gpatom.hyperspatial_beacon.hyperspacebeacon import HighDimSurrogateOptimizer
from gpatom.hyperspatial_beacon.gpfp.fingerprint import HighDimFingerPrint
from gpatom.hyperspatial_beacon.gpfp.prior import HighDimRepulsivePotential

import os

@pytest.mark.parametrize('penalty_type', [0,1])
def test_penalty(penalty_type):

    surropt=HighDimSurrogateOptimizer(fmax=0.05, 
                                      relax_steps=40,  
                                      after_steps=10,
                                      from_dims=5, 
                                      to_dims=3,
                                      rate=0.5,
                                      potential_type=0,
                                      penalty_type=penalty_type,
                                      world_size=10,
                                      write_surropt_trajs=True)
    
    rng=np.random.RandomState(2432)
    
    penalty_coords=rng.rand(1,2)
  
    strength=0.5
    
    HS_eng0_n, HS_eng0_p, HS_deriv0 = surropt.HS_penalty(penalty_coords, strength)

   
    # assert that penalty force only acts on extra dimensions
    assert np.allclose(HS_deriv0[0][0:3], np.zeros(3))
    assert all ( abs(HS_deriv0[0][3::])>0 )
    
 
    # assert that the gradient is correct derivative of the energy
    dx=0.001
    analytical_force=HS_deriv0[0][3::]
    
    numerical_force=np.zeros(  (len(penalty_coords[0,:])) )  
    for i in range(2):
        penalty_coords_copy=penalty_coords.copy()
        penalty_coords_copy[0,i]+=dx
    
        HS_eng1_n, HS_eng1_p, HS_deriv1 = surropt.HS_penalty(penalty_coords_copy, strength)    
    
        numerical_force[i]=  (HS_eng1_n - HS_eng0_n)/dx

    assert  np.allclose(analytical_force , numerical_force, atol=1e-6,  rtol=1e-3) 
    #assert that gradients has the right right direction and is minimized at the world_center
    wc=np.array(surropt.world_center[3]).reshape( (1,1) )
 
    penalty_coords_plus=wc+dx
    HS_eng_plus_n, HS_eng_plus_p, HS_deriv_plus = surropt.HS_penalty(penalty_coords_plus, strength)    
    
    penalty_coords_minus=wc-dx
    HS_eng_minus_n, HS_eng_minus_p, HS_deriv_minus = surropt.HS_penalty(penalty_coords_minus, strength)  

    penalty_coords_wc=wc
    HS_eng_wc_n, HS_eng_wc_p, HS_deriv_wc = surropt.HS_penalty(penalty_coords_wc, strength)    
    
    assert(HS_deriv_plus[0][3]>0)
    assert(HS_deriv_minus[0][3]<0)
    assert np.isclose(HS_deriv_wc[0][3],0)
    assert(HS_eng_wc_n<HS_eng_plus_n)
    assert(HS_eng_wc_n<HS_eng_minus_n)


@pytest.mark.parametrize('potential_type', [0,1])
def test_hyperspace_surrogate_optimization(potential_type):
        
    atoms = Atoms(['Au'] * 5 + ['Cu'] * 3, positions=[[0., 0., 0.]] * 8)
    atoms.cell=[12,12,12]
    atoms.pbc = False
    atoms.center()

    atoms.positions[0,:]=[7.5,6,6]
    atoms.positions[7,:]=[4.5,6,6]
    
    constrained_atoms=[0,7]
    
    atoms.set_constraint(FixAtoms(constrained_atoms))

    r_rng = np.random.RandomState(34)
    
    s_rng = np.random.RandomState(2473)
    
    box=[(0., 10.), (0., 10.), (0., 10.)]
    rgen=RandomBox(atoms=atoms, box=box, 
                   covrad_inside=[False, False, False], rng=r_rng)
    
    potential=RepulsivePotential(prefactor=10, potential_type='parabola')
    relaxer=AtomsRelaxer(potential)
    
    
    hd_box=[(2., 8.), (2., 8.), (2., 8.), (2., 8.)]
    sgen=HighDimRandomBox(atoms=atoms, box=hd_box, 
                          covrad_inside=[False, False, False, False], rng=s_rng)

    potential=HighDimRepulsivePotential(prefactor=10, potential_type='parabola')
    relaxer=HighDimAtomsRelaxer(potential, dims=sgen.dims, constr=sgen.constr, 
                                world_center=sgen.world_center)
    
    sgen.attatch_relaxer(relaxer)

    training_atoms=[]
    for i in range(5):
        atoms_i=rgen.get()
        atoms_i.calc=EMT()
        training_atoms.append(atoms_i)
    
    surropt=HighDimSurrogateOptimizer(fmax=0.05, 
                                      relax_steps=40,  
                                      after_steps=10,
                                      from_dims=4, 
                                      to_dims=3,
                                      rate=0.5,
                                      potential_type=potential_type,
                                      world_size=atoms.cell[0][0],
                                      write_surropt_trajs=True)
    
    fp_args = {'r_cutoff': 8.0,
               'r_delta': 0.4,
               'r_nbins': 200,
               'a_cutoff': 4.0,
               'a_delta': 0.4,
               'a_nbins': 100,
               'gamma': 0.5,
               'aweight': 1}  

    fp=HighDimFingerPrint(fp_args)
    
    gp_args=dict(prior=HighDimConstantPrior(constant=0), 
                 hp={'scale': 1000}, use_forces=True)

    gp=HighDimGaussianProcess(**gp_args)

    model=Model(gp=gp, fp=fp)
    
    model.add_data_points(training_atoms)


    test_atoms=sgen.get()
    assert hasattr(test_atoms, 'extra_coords')

    opt_atoms, success = surropt.relax(test_atoms, model, file_identifier='hs')

    
    # check that files are written
    assert( os.path.isfile('opt_hs.xyz') )
    
    trajectory=read('opt_hs.xyz', ':')
    
    # fractions are stored in opt_hs.xyz initial charges
    extra_coords=np.zeros((len(trajectory),len(atoms)))
    for i in range(len(trajectory)):
        extra_coords[i,:]=trajectory[i].get_initial_charges()
    
    
    # check that position constrained atoms havent moved
    first_frame=trajectory[0]
    last_frame=trajectory[-1]
    for atom_index in constrained_atoms:
        coord_start=first_frame.get_positions()[atom_index]
        coord_end=last_frame.get_positions()[atom_index]
        
        assert np.allclose(coord_start, coord_end)
        
    # check that extra_coords stayed zero at all steps
    assert np.allclose(extra_coords[:,0], np.zeros(len(trajectory)))
    assert np.allclose(extra_coords[:,7], np.zeros(len(trajectory)))

    #check that all atoms have left fourth dimension in the end
    assert np.allclose(extra_coords[-1,:], np.zeros(len(atoms)))
    
    
    
    # energy wont neccessarily be lower in the end state than the start state
    # with this algorithm , hence it wont be tested
###################################    
    # check that the end energy is lower than the start energy   
#    start_atoms=trajectory[0]
#    start_extra_coords=extra_coords[0,:].reshape( (len(atoms),1) )

#    fp_start=model.fp.get(atoms=start_atoms, extra_coords=start_extra_coords)
#    prediction_start, unc_start=model.gp.predict(fp_start)

#    end_atoms=trajectory[-1]
#    end_extra_coords=extra_coords[-1,:].reshape( (len(atoms),1) )
#    fp_end=model.fp.get(atoms=end_atoms, extra_coords=end_extra_coords)
#    prediction_end, unc_end=model.gp.predict(fp_end)

#    assert(prediction_end[0] < prediction_start[0])
##################################
