#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 23 16:32:03 2023

@author: casper
"""
import pytest

import numpy as np
from gpatom.hyperspatial_beacon.gpfp.fingerprint import (HighDimRadialFP,
                                                         HighDimRadialAngularFP)

from ase import Atoms

from gpatom.beacon.str_gen import RandomBox       

@pytest.mark.parametrize('dimensions', [4,5])
@pytest.mark.parametrize('fp_class', [HighDimRadialFP, HighDimRadialAngularFP])
def test_fp_derivative(fp_class, dimensions):
    
    '''
    tests that gradient of fingerprint is correct with respect to
    coordinates and extra_coordinates
    '''
    
    atoms=Atoms(['Cu']*5, positions=[[0,0,0]]*5)
    atoms.cell=[6,6,6]
    atoms.pbc=False
    
    rng=np.random.RandomState(3423)

    sgen=RandomBox(atoms, box=[(1., 5.), (1., 5.), (1., 5.)], rng=rng)

    extra_coords=rng.rand(len(atoms),  dimensions-3 )-0.5

    dx = 0.000001
    
    hd_atoms = sgen.get()

    fp1 = fp_class(atoms=hd_atoms, extra_coords=extra_coords)
    drho_analytical = fp1.reduce_coord_gradients()[0,:, :] # just test atom_0
    v1 = fp_class(atoms=hd_atoms, extra_coords=extra_coords).vector
    
    drho_numerical=np.zeros(np.shape(drho_analytical))
    
    
    for j in range(dimensions):
        extra_coords_copy=extra_coords.copy()
        atoms_copy=hd_atoms.copy()
        
        if j>2:
            extra_coords_copy[0,j-3]+=dx
        else:
            atoms_copy.positions[0,j]+=dx
    
        v2 = fp_class(atoms=atoms_copy, extra_coords=extra_coords_copy).vector
 
        drho_numerical[:,j] = (v2 - v1) / dx

    print(drho_numerical)
    print(drho_analytical)     
    assert np.allclose(drho_numerical, drho_analytical, rtol=1e-5, atol=1e-2)
   