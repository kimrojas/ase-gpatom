import pytest
from gpatom.hyperspatial_beacon.gpfp.fingerprint import HighDimRadialAngularFP
from ase.cluster import Icosahedron
import numpy as np

@pytest.mark.parametrize('dimensions', [4,5])
def test_fingerprint_symmetries(dimensions):

    rng = np.random.RandomState(34534)
    slab = Icosahedron('Cu', noshells=2, latticeconstant=2.5)
    slab.center(vacuum=4.0)

    extra_coords = rng.rand(len(slab),dimensions-3)-0.5
    
    rng = np.random.RandomState(53453)
    
    fp = HighDimRadialAngularFP

    fp0 = fp(atoms=slab.copy(), extra_coords=extra_coords)
    vec0 = fp0.vector

    # TRANSLATION
    slab.positions += rng.random(3)
    extra_coords_trans = extra_coords.copy() + rng.random(dimensions-3)  
    fp_trans = fp(atoms=slab.copy(), extra_coords=extra_coords_trans)

    # ROTATION (just 3D.  4D rotation was too difficult to set up)        
    slab.rotate(a=180. * rng.random(), v=rng.random(3), center='COM')
    fp_rot = fp(atoms=slab.copy(), extra_coords=extra_coords)

    # PERMUTATION (swap both positions and extra_coords)
    extra_coords_perm=extra_coords.copy()
    extra_coords_perm[3,:]=extra_coords[4,:]
    extra_coords_perm[4,:]=extra_coords[3,:]
    
    tmp = slab[3].position.copy()
    slab[3].position = slab[4].position
    slab[4].position = tmp
    fp_perm = fp(atoms=slab.copy(), extra_coords=extra_coords_perm)

    for fp1 in [fp_trans, fp_rot, fp_perm]:
        
        vec1 = fp1.vector

        d = np.linalg.norm(fp0.vector - fp1.vector)

        assert(fp0.atoms.positions != fp1.atoms.positions).all()
        assert(d < 1e-4)
        assert(np.allclose(vec0, vec1, atol=1e-8))
        
        